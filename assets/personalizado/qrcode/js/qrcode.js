let scanner = new Instascan.Scanner({ video: document.getElementById('preview'), mirror: false});
scanner.addListener('scan', function(content) {
    window.open(content);
});

Instascan.Camera.getCameras().then(cameras => {
    if(cameras.length > 0){
        scanner.start(cameras[1]);
    } else {
        console.error("Não existe câmera no dispositivo!");
    }
});