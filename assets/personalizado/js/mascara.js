$(".modalFooterConteudo").on("input", ".telefone", function() {
	$(this).mask("(00) 00000-0000");
});

$(".modalFooterConteudo").on("input", "#cnpj", function() {
	$(this).mask("99.999.999/9999-99");
});

$('.cep').mask("99.999-999");
$('#telefone').mask("(00) 00000-0000");
$(".cartao").mask("9999 9999 9999 9999");
$(".validade").mask("99/99");
$(".cpf").mask("999.999.999-99");
$(".valor").mask("000.000.000.000.000,00", {reverse: true});
