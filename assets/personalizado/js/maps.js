    
function notificacaoMapa(){
  $.notify({
    icon: "add_alert",
    message: "Seja bem vindo ao Pets Meeting, ative a localização para utilizar os nossos serviços."

  },{
    type: 'default',
    timer: 6000,
    placement: {
      from: 'top',
      align: 'right'
    }
  });
}
notificacaoMapa();

var latitudade = -17.2545519;
var longitude = -44.8997827;
getLocation();

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position){
      console.log(position);
      latitudade = position.coords.latitude;
      longitude = position.coords.longitude;
      tamanhozoom = 14;
      initMap();
    });

  }
}
function initMap() {
  var uluru = {lat: latitudade , lng: longitude};
  var map = new google.maps.Map(
    document.getElementById('map'), {zoom: tamanhozoom, center: uluru});
  var marker = new google.maps.Marker({position: uluru, map: map});
}

$("#continuarMapa").on("click", function() {
  window.location.href = base_url+"selecionarperfilpets";
});

$("#aceitelocalizacao").on("click",function() {

  var tipoConta = $(this).attr("data-tipoConta");

  if( latitudade == "-17.2545519" || longitude == "-44.8997827" ) {
    Swal({
      type: 'error',    
      background: '#fff ',
      title: 'Oops...',
      text: '',
    });
    Swal.fire({
      title: 'Ops!',
      text: 'Localização não encontrada!, permita a localização para prosseguir',
      imageUrl: 'assets/personalizado/imagem/error-ativacao-localizacao.gif',
      imageWidth: 500,
      imageHeight: 300,
      imageAlt: 'Custom image',
      animation: false
    })
  } else {
    Swal({
      title: 'Ativando localização!',
      html: 'Aguarde um momento.',
      timer: 2000,
      onBeforeOpen: () => {
        Swal.showLoading()
        timerInterval = setInterval(() => {
          Swal.getContent().querySelector('strong')
          .textContent = Swal.getTimerLeft()
        }, 100)
        $.ajax ({
          url: 'salvarLocalizacao',
          type: 'post',
          dataType : 'Json',
          data: {
            "latitudelocalizacao": latitudade,
            "longitudelocalizacao": longitude
          },
          success:function(data){
            console.log(data);
          }
        });
      },
      onClose: () => {
        clearInterval(timerInterval);
        if (tipoConta == 0) {
          window.location.href = base_url+"pesquisa";
        } else {
          window.location.href = base_url+"financeiro";
        }
        
      }
    }).then((result) => {
      if (
    // Read more about handling dismissals
    result.dismiss === Swal.DismissReason.timer
    ) {
      }
  });
  }

});


