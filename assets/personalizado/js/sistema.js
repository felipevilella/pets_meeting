//ACIONANDO O MODAL DESCRIÇÃO
$("#descricao").on("click",function()  {
	$("#modaldescricao").modal();
});
//CONTANDO CARACTER DOS CAMPOS
$("#textDescricao").on("input",function()  {
	texto = $(this).val();
	total = 200 - texto.length;
	$("#limitecaracter").html(total);
});



$("#alterarfotousuario").on("click",function()  {
  $(".alterarfoto_usuario").modal();
});
$("#alterarperfil").on("click",function()  {
  $(".editarinformacaousuario").modal();
});
$("#inserirfotoPets").on("click",function() {
  $("#perfilfotopets").modal();
});
$("#sobreperfilpets").on("click",function() {
  $("#alterardescricao").modal();
});



$("#editarperfilpets").on("click",function() {
  $("#modalalteracaopets").modal();
});
$("#adicionarfotospets").on("click",function() {
  $("#exampleModalCenter").modal();
});


//MODAL FOTOS PETS

$('.newbtn').bind("click" , function ()  {
  $('#pic').click();
});

function readURL(input)  {
  if (input.files && input.files[0])  {
    var reader = new FileReader();

    reader.onload = function (e)  {
      $('#blah')
      .attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$("#esqueci").on("click",function()  {
  $("#recuperarsenha").modal();
});


$("#Entrar").on("click",function()  {
  var email_usuario = $("#email").val();
  var senha_usuario = $("#senha").val();
  
  $(".erroLogin").val();
  
    $.ajax( {
      url:base_url+"validacaousuario",
      type:'post',
      dataType:'Json',
      data: {
        usuario: email_usuario,
        senha: senha_usuario
      },
      success:function(data) {
       if (data.confirmado == false) {
        $(".erroLogin").html(data.mensagem);
      }
      else {
        if(data.autenticacao == "0") {
           localStorage.setItem("emailUsuario", email_usuario);
           localStorage.setItem("senhaUsuario", senha_usuario);
        }
       
        window.location.href = data.url;
      }
    }
  });
});


$("#Solicitar").on("click",function()  {
 $("#chatbox").toggleClass("hidden");
});



$("#estadoBuscar").on("change",function()  {
  var idEstadoCadastro = $(this).val();
  $("#Cidade").attr("disabled",true);
  $("#Cidade").html("<option> Carregando ... </option>");
  $.ajax( {
   url:base_url+'buscarCidade',
   type:'post',
   dataType:"Json",
   data: {
    idEstado: idEstadoCadastro,
  },
  success:function(data) {
    for (var i = 0; i <data.length; i++)  {
      if (i == 0) {
       $("#Cidade").html("<option value ='"+data[i].idcidade+"'>"+data[i].nome+"</option>");
     }
     else {
       $("#Cidade").append("<option value ='"+data[i].idcidade+"'>"+data[i].nome+"</option>");
     }

   }
 }
});
  $("#Cidade").attr("disabled",false);
});


$("#estadoBuscar").on("change",function()  {
  var idEstadoCadastro = $(this).val();
  $("#Cidade").attr("disabled",true);
  $("#Cidade").html("<option> Carregando ... </option>");
  $.ajax( {
   url:base_url+'buscarCidade',
   type:'post',
   dataType:"Json",
   data: {
    idEstado: idEstadoCadastro,
  },
  success:function(data) {
    for (var i = 0; i <data.length; i++)  {
      if (i == 0) {
       $("#Cidade").html("<option value ='"+data[i].idcidade+"'>"+data[i].nome+"</option>");
     }
     else {
       $("#Cidade").append("<option value ='"+data[i].idcidade+"'>"+data[i].nome+"</option>");
     }

   }
 }
});
  $("#Cidade").attr("disabled",false);
});



$("#password1").on("input", function() {
  var senha1_usuario = $("#password").val();
  var senha2_usuario = $(this).val();
  if (senha1_usuario != senha2_usuario) {
    $("#retornosenha").html("Senhas divergentes");
    $("#Cadastrar").attr("disabled",true);
  }
  else {
    $("#retornosenha").html("");
    $("#Cadastrar").attr("disabled",false);
  }
});



$("#Cadastrar").on("click",function() {

  var nome_usuario = $("#nome").val();
  var estado_usuario = $("#estadoBuscar").val();
  var cidade_usuario = $("#Cidade").val();
  var email_usuario = $("#emailCadastro").val();
  var senha1_usuario = $("#password").val();
  var contaComercial = $("#comercial").val();

  $("#Cadastrar").attr("disabled",false);
  $.ajax({
   url:base_url+'salvarUsuario',
   type:'post',
   dataType:"Json",
   data:{
    nome:nome_usuario,
    estado:estado_usuario,
    cidade:cidade_usuario,
    email:email_usuario,
    senha:senha1_usuario,
    comercial: contaComercial
  },
  success:function(data) {
    if (data.mensagem == "Conta cadastrada com sucesso!") {
      cadastro_success();
    } else {
      $("#erroNome").html(data.nome);
      $("#erroSobrenome").html(data.sobrenome);
      $("#erroEstado").html(data.estado);
      $("#erroCidade").html(data.cidade);
      $("#erroEmailCadastro").html(data.email);
      $("#erroSenha").html(data.senha);
      $("#retornoComercial").html(data.comercial);
    }
  }
});
});

$(document).keypress(function(e) {    
    var urlPagina =  window.location.href;
        if (e.which == 13) {
          if (urlPagina.match("login")) {
             $('#Entrar').trigger('click'); 
          }
        }
});

$("#emailCadastro").on("focusout",function() {
  var email_usuario = $(this).val();

  $.ajax( {
   url:base_url+'validarUsuario',
   type:'post',
   dataType:"Json",
   data: {
     email: email_usuario
   },
   success:function(data) {
    if (data.mensagem == "E-mail indisponivel"){
      $("#erroEmailCadastro").html(data.mensagem);
      $("#Cadastrar").attr("disabled",true);
    }else{
      $("#Cadastrar").attr("disabled",false);
      $("#erroEmailCadastro").html("");
    }
  }
});
});

$("#atualizarDescricacao").on("click",function() {
  var texto = $("#sobretexto").val();
  var idPets = $("#idpets").val();
  $.ajax( {
   url:base_url+'salvarDescricao',
   type:'post',
   dataType:"Json",
   data: {
     descricaoPets:texto,
     idpets: idPets
   },
   success:function(data) {

   }
 });
  window.location.href = base_url+"perfilpets/"+idPets;
});

$("#mensagemusuario").on("click","#verconversa", function() {
  var idusuario = $("#idusuarioconversa").val();
  window.location.href = base_url+"perfilusuario/"+idusuario;
});

$("#solicitarEmail").on("click",function(){
 var emailsolicitacao = $("#emailesquecisenha").val();
  $.ajax({
    url: base_url+"solicitarSenha",
    type: "post",
    dataType: "Json",
    data:{
      email: emailsolicitacao
    },
    success:function(data) {
     if (data.tipo == "success") {
      email_success();
    } else if (data.tipo == "alert") {
      email_error();
    }

  }
  });
});

$("#alterarsenha").on("click",function() {
  var senha = $("#senhaalterar").val();
  var senha1 = $("#senharepitir").val();
  var codigo = $("#codigo").val();
  
  if (senha != senha1) {
    senha_error();
  } else {
    $.ajax({
      url: base_url+"alterarsenha",
      type: "post",
      dataType: "Json",
      data:{
        senha: senha1,
        idUsuario:codigo
      },
      success:function(data) {
        senha_success();
      }
    });
  }
});

function notificacao() {
  $.ajax({
    url:base_url+"notificacao",
    type: "post",
    dataType: "Json",
    data:{

    },
    success:function(data){
      for (var i = 0 ; i < data.notificacao.length; i ++) {

        if (data.notificacao[i].fk_idTipoNotificacao == 2  && data.notificacao[i].ativo == '1') {

          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            type: 'info',
            title: 'Você possui uma nova solicitação de conversa!' 
          });

        } if (data.notificacao[i].ativo == '1' &&  data.notificacao[i].fk_idTipoNotificacao == 1) {
         const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });
         Toast.fire({
          type: 'info',
          title: 'Você possui uma nova mensagem!' 
        });
       } 

     } 
   }
 });
}

$(".expandirFoto").on("click", function() {
  var imagem  = $(this).attr("data-foto");
  $(".imagemPet").html("<img class='d-block w-100' src="+base_url+"assets/personalizado/fotos_pets/"+imagem+">");
  $("#verFoto").modal();
});

$("#petPerdido").on("click", function() {
  $("#petPerdidomodal").modal();

});


$("#btn-ultimaLocalizacao").on("click", function() {
  $("#ultimaLocalizacao").modal();
});


$(".entrarPets").on("click", function() {
  $("#body").removeClass("mobile-nav-active");
  $(".mobile-nav-overly").attr("style", "display: none;");
  $("#acessar").modal();
});

$("#cadastro").on("click",function() {
  $("#tituloFormulario").html("Cadastro de donos de animais");
  $("#comercial").val("0");
  $("#nome").attr("placeholder", "Nome");
  $("#criaConta").modal();
  $("#selecionarPerfil").modal('hide');
});

$("#cadastroPetShop").on("click", function() {
  $("#tituloFormulario").html("Cadastro de estabelecimento");
  $("#nome").attr("placeholder", "Nome fantasia");
  $("#comercial").val("1");
  $("#criaConta").modal();
  $("#selecionarPerfil").modal('hide');
});

$(".cadastroPerfil").on("click", function() {
  $("#body").removeClass("mobile-nav-active");
  $(".mobile-nav-overly").attr("style", "display: none;");
  $("#selecionarPerfil").modal();
  
});



$(".cep").on('input', function() {
  var cep = $(this).val().replace(/\.|\-/g,'');

  if(cep.length == 8) {
    $.get('https://viacep.com.br/ws/'+cep+'/json/',{}, function(data) {
      if (data.erro == true) {
        $("#erroCep").html("O cep está inválido");
      } else {
        $('#bairro').val(data.bairro);
        $('#rua').val(data.logradouro);
        $("#salvarEndereco").attr('data_estado',data.uf);
        $("#salvarEndereco").attr('data_cidade',data.localidade);
        $("#inserirEndereco").addClass('show');
        $("#listaEndereco").removeClass('show');
        $(".div-endereco").removeClass('show');
      }

    }, 'json');
  }
});

$(".btn-alteraEndereco").on("click",function() {
  $('#bairro').val($(this).attr('data_bairro'));
  $('#rua').val($(this).attr('data_rua'));
  $('.cep').val($(this).attr('data_cep'));
  $('#numero').val($(this).attr('data_numero'));

  $('.cep').attr('disabled','true');
  $("#salvarEndereco").attr('data_codigo', $(this).attr('data_codigo'));
  $("#inserirEndereco").addClass('show');
  $(".div-endereco").removeClass('show');
  $("#listaEndereco").removeClass('show');
});

$(".btn-excluirEndereco").on("click", function() {
    Swal.fire({
    title: 'Você deseja excluir o Endereço?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'sim',
    cancelButtonText: 'Não'
  }).then((result) => {
    if (result.value) {
      var codigo = $(this).attr('data_codigo');
      $.post(base_url+"deletarEndereco", {
          codigoEndereco: codigo
      }, function(data){
          const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000
            });

            Toast.fire({
              type: 'success',
              title: "Endereço excluido com sucesso!"
            });

            setTimeout(function() {
              location.reload();
            }, 700);
       }, "JSON");
    }
  })
});

$(".btn-ativarEndereco").on("click", function() {
    var codigo = $(this).attr('data_codigo');
    $.post(base_url+"selecionarEndereco", {
        codigoEndereco: codigo
    }, function(data){
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });

          Toast.fire({
            type: 'success',
            title: "Endereço selecionar com sucesso!"
          });

          setTimeout(function() {
            location.reload();
          }, 700);
     }, "JSON");
});


$("#salvarEndereco").on("click", function() {
    $("#erroCep").html('');
    $("#erroBairro").html('');
    $("#erroRua").html('');
    
    $.post(base_url+"salvarEndereco", {
      bairro: $('#bairro').val(),
      rua: $('#rua').val(),
      cep: $('.cep').val(),
      numero: $("#numero").val(),
      estado: $(this).attr('data_estado'),
      cidade: $(this).attr('data_cidade'),
      codigoEndereco: $(this).attr('data_codigo')
    }, function(data) {
        if (data.tipo == "alert") {
          if(data.limite != "") {
            $("#erroCep").html(data.limite);
          } else {
            $("#erroCep").html(data.campos.cep);
            $("#erroBairro").html(data.campos.bairro);
            $("#erroRua").html(data.campos.rua);
          }
        } else {
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });

          Toast.fire({
            type: 'success',
            title: "Endereço salvo com sucesso!"
          });

          setTimeout(function() {
            location.reload();
          }, 700);
        }
    },'JSON'); 
});

FilePond.registerPlugin(
      FilePondPluginFileValidateSize,
      FilePondPluginImageExifOrientation,
      FilePondPluginImageCrop,
      FilePondPluginImageResize,
      FilePondPluginImagePreview,
      FilePondPluginImageTransform
);    


const inputElement = document.querySelector("input[type='file']");

FilePond.create(inputElement, {
  
  maxFileSize: '50MB',
  imageResizeTargetWidth: 800,
  imageResizeTargeHeight: 800,
  imageResizeMode: 'contain',
  allowImageResize: true,
  
  server: {
    url: base_url+'alterarFotoUsuario',
  },

  maxFileSize: '50MB',
  instantUpload: true,

  revert: './revert',
  restore: './restore/',
  load: './load/',
  fetch: './fetch/',

  onpreparefile:(fileItem, output) => {
      const img = new Image();
      img.src = URL.createObjectURL(output);
  } 

});

$(".btn-sair").on('click', function() {
  localStorage.clear();
});

function loginAutomatico() {
  var email_usuario =  localStorage.getItem("emailUsuario");
  var senha_usuario =  localStorage.getItem("senhaUsuario");
  
  $(".erroLogin").val();
  
    $.ajax( {
      url:base_url+"validacaousuario",
      type:'post',
      dataType:'Json',
      data: {
        usuario: email_usuario,
        senha: senha_usuario
      },
      success:function(data) {
       if (data.confirmado == false) {
      }
      else {
        if(data.autenticacao == "0") {
           localStorage.setItem("emailUsuario", email_usuario);
           localStorage.setItem("senhaUsuario", senha_usuario);
        }
       
        window.location.href = data.url;
      }
    }
  });
}

var url = window.location.href;

if(url.match('login')) {
  loginAutomatico();
}



