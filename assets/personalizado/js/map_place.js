
var placeSearch, autocomplete;



$("#pesquisaLocalizacao").on("focus", function() {
	autocomplete = new google.maps.places.Autocomplete(document.getElementById('pesquisaLocalizacao'), {types: ['geocode']});
	autocomplete.setFields(['address_component']);
	autocomplete.addListener('place_changed', fillInAddress);
});

function fillInAddress() {
  var place = autocomplete.getPlace();
}

function geolocate() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			var circle = new google.maps.Circle(
				{center: geolocation, radius: position.coords.accuracy});
			autocomplete.setBounds(circle.getBounds());
		});
	}
}