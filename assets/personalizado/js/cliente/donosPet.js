$("#salvarSobre").on("click", function() {
	$.post(base_url+"salvarSobrePerfil", {
		nome: $("#nome").val(),
		telefone: $("#telefone").val()
	}, function(data) {
		if (data.tipo == "success") {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: "Perfil alterado com sucesso!"
			});

			setTimeout(function() {
				location.reload();
			}, 700);
		} else {
			$("#erroNome").html(data.campos.nome);
		}
	}, 'JSON');
});

$("#cadastrarPets").on("click", function()  {
	var nome_pets = $("#nome").val();
	var raca_pets = $("#raca").val();
	var data_pets = $("#data").val();
	var tipoAnimal = $("#tipoAnimal").val();
	var sexo_pets = $("input[name='customRadioInline1']:checked").val();

	$("#erroNome").html("");
	$("#erroRaca").html("");
	$("#erroSexo").html("");
	$("#erroAno").html("");

	if (sexo_pets == "f") {
		var mensagem = nome_pets+" cadastrada!";
	} else {
		var mensagem = nome_pets+" cadastrado!";
	}

	$.post(base_url+"cadastrarPets", {
		nome:nome_pets,
		raca:raca_pets,
		ano:data_pets,
		sexo:sexo_pets,
		tipoAnimal: tipoAnimal
	},function(data) {
		console.log(data);
		if (data.tipo == "success") {
			$("#modal-cadastroPets").modal('hide');
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: mensagem
			});

			setTimeout(function() {
				location.reload();
			}, 700);
		} else {
			$("#erroNome").html(data.campos.nome);
			$("#erroRaca").html(data.campos.raca);
			$("#erroPeso").html(data.campos.peso);
			$("#erroSexo").html(data.campos.sexo);
			$("#erroAno").html(data.campos.ano);
		}
	}, "JSON");	
});

$("#alterarPets").on("click", function() {
	var nome_pets = $("#nome").val();
	var raca_pets = $("#raca").val();
	var peso_pets = $("#peso").val();
	var data_pets = $("#data").val();
	var sexo_pets = $("input[name='customRadioInline1']:checked").val();
	var descricao_pets = $("#descricao").val();

	var url = window.location.href;
	url = url.split("/");
	var codigo_pets =  url.pop();

	$("#erroNome").html("");
	$("#erroRaca").html("");
	$("#erroPeso").html("");
	$("#erroSexo").html("");
	$("#erroAno").html("");

	$.post(base_url+"alterarPets", {
		nome:nome_pets,
		raca:raca_pets,
		ano:data_pets,
		sexo:sexo_pets,
		descricao:descricao_pets,
		codigoPet: codigo_pets
	}, function(data) {

		if (data.tipo = "success") {
			$("#modal-editarPerfil").modal('hide');
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: "Perfil alterado com sucesso!"
			});

			setTimeout(function() {
				location.reload();
			}, 700);
		} else {
			$("#erroNome").html(data.campos.nome);
			$("#erroRaca").html(data.campos.raca);
			$("#erroPeso").html(data.campos.peso);
			$("#erroSexo").html(data.campos.sexo);
			$("#erroAno").html(data.campos.ano);
		}
	},"JSON");
});

$("#estado").on("change", function() {
	var idEstadoCadastro = $(this).val();
	$("#cidade").attr("disabled",true);
	$("#cidade").html("<option> Carregando ... </option>");
	$.post(base_url+'buscarCidade', {
		idEstado: idEstadoCadastro
	}, function (data) {
		for (var i = 0; i <data.length; i++)  {
			if (i == 0) {
				$("#cidade").html("<option value ='"+data[i].idcidade+"'>"+data[i].nome+"</option>");
			} else {
				$("#cidade").append("<option value ='"+data[i].idcidade+"'>"+data[i].nome+"</option>");
			}
		}
	}, "JSON");
	$("#cidade").attr("disabled",false);
});

$(".agendar-modal").on("click", function() {
	var servico = $(this).attr("data-codigoservico");
	$("#codigoServico").val(servico);
	$("#erroNome").html("");
	$("#erroData").html("");
	$("#erroHorario").html("");
	$(".horario").html("<p class='text-white' align='center'> <b>Selecione uma data</b></p>");
	$("#data").val("");
	$("#animal").val("");
	$("#modal-agendamento").modal();
});

$("#data").on("change", function(){
	var dataServico = $(this).val();
	var url = window.location.href;
	url = url.split("/");
	var codigoServico = url.pop();
	codigo_petshop = $(this).attr("data_codigoPetshop");

	$(".horario").html("<p class='text-white' align='center'> <b>Carregando ...</b></p>");

	$.post(base_url+"horarioServicos", {
		data: dataServico,
		codigoServico: codigoServico,
		codigoPetshop: codigo_petshop
	}, function(data) {
		if (data.tipo == "success") {
			$(".horario").html(data.html);
		} else {
			$(".horario").html("<p class='text-white' align='center'><b>"+data.mensagem+"</b></p>");
		}
	},"JSON");
});

$(".btn-confimarReserva").on("click", function() {
	Swal.fire({
		html: '<h3>Você deseja confirmar a reserva?</h3>',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#green',
		cancelButtonColor: 'grey',
		confirmButtonText: 'sim',
		cancelButtonText: 'Não'
	}).then((result) => {
		if (result.value) {
			$.post(base_url+"confirmarReserva", {}, function(data) {
				if (data.tipo == "success") {
					$("#modal-agendamento").modal('hide');
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 3000
						});

						Toast.fire({
							type: 'success',
							title: data.mensagem
						});

						setTimeout(function() {
							location.reload();
						}, 800);	
				} else {
					Swal.fire({
					  text: 'Não foi possivel realizar o pagamento - '+data.mensagem,
					  showClass: {
					    popup: 'animated fadeInDown faster'
					  },
					  hideClass: {
					    popup: 'animated fadeOutUp faster'
					  }
					})
				}
			},'JSON');
		}
	});
});

$("#btn-adicionar-carrinho").on('click', function() {
	var codigoAnimal = $("#animal").val();
	var dataAgendamento = $("#data").val();
	var url = window.location.href;
	url = url.split("/");
	var codigoServico = url.pop();
	var hora = $("input[name='customRadioInline1']:checked").val();
	var codigoFuncionario = $("#funcionario").val();

	$("#erroNome").html("");
	$("#erroData").html("");
	$("#erroHorario").html("");

	$.post(base_url+"agendarServico", {
		codigoPet: codigoAnimal,
		data: dataAgendamento,
		codigoServico: codigoServico,
		horario: hora,
		codigoFuncionario: codigoFuncionario
	}, function(data) {
		if (data.tipo == "success") {
			$("#modal-agendamento").modal('hide');
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: "Servico adicionado no carrinho!"
			});

			setTimeout(function() {
				window.location.href = base_url+"carrinho";
			}, 800);
		} else {
			$("#erroNome").html(data.campos.codigoPet);
			$("#erroData").html(data.campos.data);
			$("#erroHorario").html(data.campos.horario);
		}
	},"JSON");
});

$(".removerPedido").on("click", function(){
	$.post(base_url+"removerIntem", {
		codigoReserva: $(this).attr('data-codigo')
	},function(data) {
		if (data.tipo == "success") {
			location.reload();
		}
	},'JSON');
});

$(".salvarCartao").on("click", function() {
	$("#erroNumeroCartao").html("");
	$("#erroValidade").html("");
	$("#erroCVV").html("");
	$("#erroNomeTitular").html("");
	$("#erroCPFTitular").html("");

	$.post(base_url+"salvarCartao", {
		numero: $("#cartao").val(),
		dataExpiracao:$("#validade").val(),
		cvv: $("#cvv").val(),
		nome:$("#nomeTitular").val(),
		cpf:$("#cpf").val()
	},function(data) {
		if (data.tipo == "success") {
			$("#modal-agendamento").modal('hide');
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: "cartão salvo com successo!"
			});

			setTimeout(function() {
				window.location.href = base_url+"carrinho";
			}, 800);
		} else {
			$("#erroNumeroCartao").html(data.campos.numero);
			$("#erroValidade").html(data.campos.dataExpiracao);
			$("#erroCVV").html(data.campos.cvv);
			$("#erroNomeTitular").html(data.campos.nome);
			$("#erroCPFTitular").html(data.campos.cpf);

		}
	},'JSON');
});

$("#excluirCartao").on("click", function(){
	$.post(base_url+"excluirCartao", {
		codigoCartao: $(this).attr('data_codigo')
	},function(data) {
		if (data.tipo == "success") {
			location.reload();
		}
	},'JSON');
});

$("#tipoAnimal").on("change", function() {
	$.post(base_url+"obterRacasPets", {
		codigoTipoAnimal: $(this).val()
	},function(data) {
		if (data.tipo == "success") {
			$("#raca").attr('disabled', true);
			$("#raca").html('<option> Carregando </option>');
			$("#raca").html('<option> Selecione uma opcao </option>');
			

			for (var i = 0; i < data.racas.length; i++) {
				$("#raca").append(
					"<option value='"+data.racas[i].idracas+"'>"+data.racas[i].nome+"</option>"
				);
			}

			$("#raca").attr('disabled', false);
		}
	},'JSON');
});

$(".informacao-modal").on("click", function() {
	var servico = $(this).attr("data-codigoservico");
	$(".sobre").html("");

	$.post(base_url+"detalharServico", {
		codigoServico: servico
	},
	function(data) {
		if (data.tipo == "success") {
			$(".sobre").html(data.descricao);
		}
	},"JSON");

	setTimeout(function() {
		$("#modal-informacao").modal();
	}, 400);
});

$(".btn-consultas").on("click", function() {
	$("#titulo").html("Consultas");
	
	var url = window.location.href;
	url = url.split("/");
	var codigo_pets = url.pop();

	$.post(base_url+"obterConsultas", {
		codigoAnimal: codigo_pets,
		situacao: ['0','2']
	},
	function(data) {
		if (data.tipo == "success") {
			$(".conteudo").html(data.html);
			$("#titulo").html("Consultas");
		}
	},"JSON");
});



$(".btn-historico").on("click", function() {
	$("#titulo").html("Consultas");
	
	var url = window.location.href;
	url = url.split("/");
	var codigo_pets = url.pop();


	$.post(base_url+"obterConsultas", {
		codigoAnimal: codigo_pets,
		situacao: ['1','3']
	},
	function(data) {
		if (data.tipo == "success") {
			$(".conteudo").html(data.html);
			$("#titulo").html("Historico");
		}
	},"JSON");
});

$(".conteudo").on("click",".btn-cancelar", function() {
	Swal.fire({
		title: 'Você deseja realizar o cancelamento?',
		html: '<h5><font color="red">Antes de realizar o cancelamento entre em contato com o estabelecimento, para que '+
		'ele possa aprovar a solicitação.</font></h5>',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'sim',
		cancelButtonText: 'Não'
	}).then((result) => {

		if (result.value) {
			var codigo = $(this).attr("data-consulta");

			$.post(base_url+"solicitarCancelamento", {
				codigoReserva: codigo
			},
			function(data) {
				if (data.tipo == "success") {
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
					});

					Toast.fire({
						type: 'success',
						title: "solicitação envianda com sucesso!"
					});

					setTimeout(function() {
						location.reload();
					}, 700);
				}
			},"JSON");
		}
	})
});

$(".btn-encontrar-petshop").on("change", function() {
	$.post(base_url+"buscarPetShop", {
		codigoServico: $("#servico").val()
	},function(data) {
		if (data.tipo == "success") {
			$(".listarPetshop").html(data.html);
		}
	},'JSON');
});

$("#btn-cadastroPets").on('click', function() {
	$(".div-cadastroPets").addClass('show');
	$(".div-listarPets").removeClass('show');
});

$("#btn-listarPets").on('click', function() {
	$(".div-cadastroPets").removeClass('show');
	$(".div-listarPets").addClass('show');
});

function alterarAbaNavegacao() {
	var urlPagina =  window.location.href;
	
	if (urlPagina.match("pesquisa") || urlPagina.match("reservarServico")) {
		$(".dir2").removeClass("text-orange");
		$(".dir2").addClass("text-red");
	} else if(urlPagina.match("selecionarperfilpets")) {
		$(".dir3").removeClass("text-orange");
		$(".dir3").addClass("text-red");
	} else if(urlPagina.match("perfilpets")) {
		$(".dir3").removeClass("text-orange");
		$(".dir3").addClass("text-red");
	} else if(urlPagina.match("noticias")) {
		$(".dir1").removeClass("text-orange");
		$(".dir1").addClass("text-red");
	} else if(urlPagina.match("perfil")) {
		$(".dir4").removeClass("text-orange");
		$(".dir4").addClass("text-red");
	}

}

$(".btn-ativar-cartao").on("click", function() {
	var url = window.location.href;
	url = url.split("/");

	var codigoAnimal = url.pop();
	$.post(base_url+'ativarServico', {
		codigoPet: codigoAnimal
	}, function(data) {
		if (data.tipo == "success") {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: "Serviço ativado com sucesso!"
			});

			setTimeout(function() {
				location.reload();
			}, 300);	
		}
	}, 'JSON');
});


function verificarEndereco() {
	var urlPagina =  window.location.href;
	
	if (!urlPagina.match("/adicionar_endereco")) {
		$.post(base_url+"verificar_endereco", {}, function(data) {
			if(data.enderecoAtivo == false) {
				window.location.href = base_url+'adicionar_endereco';
			}
		},'JSON');
	}
}

function verificarAvaliacaoServico() {
	$.post(base_url+"verificarAvaliacaoServico", {}, function(data) {
		if (data.tipo == 'alert') {
			$(".texto").html(data.texto);
			$('.avaliar').attr('codigoPetshop', data.codigoPetshop);
			$('.avaliar').attr('codigoReservaServico', data.codigoReservaServico);
			$("#modal-avaliacao").modal();
		}
	}, 'JSON');
}

$(".selecionarEndereco").on("click", function() {
    var codigo = $(this).attr('data_endereco');
    $.post(base_url+"selecionarEndereco", {
        codigoEndereco: codigo
    }, function(data){
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });

        location.reload();
     }, "JSON");
});

$(".add-comentario").on("click", function() {
	$(this).removeClass('show');
	$(".div-comentario").addClass('show');
});

$(".avaliar").on('click', function() {
	 $.post(base_url+"salvarAvaliacao", {
        comentario: $('#comentario').val(),
        nota: $("input[name='estrela']:checked").val(),
        codigoReservaServico: $(this).attr('codigoReservaServico'),
       	codigoPetshop: $(this).attr('codigoPetshop')
    }, function(data){
    	$("#modal-avaliacao").modal('hide');
     }, "JSON");
});

$(".btn-descricao").on("click", function(){
	$(".btn-avaliacoes").removeClass("active");
});


$(".btn-avaliacoes").on("click", function(){
	$(".btn-descricao").removeClass("active");
});

verificarAvaliacaoServico();
alterarAbaNavegacao();
verificarEndereco();