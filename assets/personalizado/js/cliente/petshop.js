$("#sobre").on("click", function(e) {
	e.preventDefault();
	$.post(base_url+"modalSobre",{}, function(data) {
		if(data.tipo == "success") {
			$(".modalFooterConteudo").html(data.html);
			$(".tituloModal").html("Minhas Informações");
			$(".botaoModal").attr("id","salvarSobrePetshop");
			$("#modal-footer").modal();
		}
	},"JSON");
});

function verificarEndereco() {
	var urlPagina =  window.location.href;
	
	if (!urlPagina.match("/endereco")) {
		$.post(base_url+"verificar_endereco", {}, function(data) {
			if(data.enderecoAtivo == false) {
				$("#modal-endereco-estabelecimento").modal();
			} else {
				verificarPagamento();
			}
		}, 'JSON');
	}
}

$(".adicionarEndereco").on('click', function() {
	$("#modal-endereco-estabelecimento").modal('hide');
	$("#collapseTwo").addClass('show');
});
$("#btn-agendarVacina").on('click', function(){
	$(".div-agendamento-vacina").addClass('show');
	$(".div-listaVacina").addClass('hidden');
});
$(".adicionarDadosBancarios").on('click', function() {
	$("#modal-banco-estabelecimento").modal('hide');
	$("#collapseOne").addClass('show');
});

function verificarFoto() {
	$.post(base_url+"verificarFoto", {}, function(data) {
		if(data.foto == false) {
			$("#modal-foto").modal();
		} 
	},'JSON');
}

function verificarPagamento() {
	$.post(base_url+"verificarPagamento", {}, function(data) {
		if(data.pagamento == false) {
			$("#modal-banco-estabelecimento").modal();
		} else {
			verificarVeterinario();
		}
	},'JSON');
}

function verificarVeterinario() {
	var urlPagina =  window.location.href;
	
	if (!urlPagina.match("/funcionarios")) {
		$.post(base_url+"verificarVeterinario", {}, function(data) {
			if(data.veterinarios == false) {
				$("#modal-funcionario-estabelecimento").modal();
			}  else {
				verificarFoto();
			}
		},'JSON');
	}
}

$(".adicionarFoto").on('click', function() {
	$("#modal-foto").modal('hide');
});

$(".adicionarFuncionario").on('click', function() {
	$("#modal-funcionario-estabelecimento").modal('hide');
	$(".div-funcionario").addClass("show");
	$(".listar-funcionario").removeClass('show');
});

$(".adicionar-funcionario").on('click', function() {
	$(".div-funcionario").addClass("show");
	$(".listar-funcionario").removeClass('show');
});

$("#cadastroServico").on("click",function() {
	$(".tipoConsulta").attr("disabled", false);
	$(".tipoServico").attr("disabled", false);
	$("#modal-cadastroServico").modal();
});

$(".tipoServico").on("change", "", function() {
	var servico = $(this).val();
	
	if (servico == "3") {
		$(".nomeServico").html("<select class='form-control tipoConsulta' disabled>"+
			"<option value=''>Selecione</option>"+
			"</select>");

		$("#tipoConsulta").attr("disabled", false);

		$.post(base_url+"tipoConsulta",{}, function(data) {
			if(data.tipo == "success") {
				for (var i = 0 ; i<data.consultas.length; i++) {
					$(".tipoConsulta").append(
						"<option value='"+data.consultas[i].nome+"'>"+data.consultas[i].nome+"</option>"
					);
				}
			}
		},"JSON");

		setTimeout(function() {
			$(".tipoConsulta").attr("disabled", false); 
		}, 1300);
		
	} else {
		$(".nomeServico").html("<input type='text' class='form-control tipoConsulta'>");
		$(".tipoConsulta").val($(".tipoServico :selected").text());
	}
});

$("#salvarServico").on("click", function() {
	var servico = $(".tipoServico").val();
	var nomeServico = $(".tipoConsulta").val();
	var precoServico = $("#preco").val();
	var descricaoServico = $("#descricao").val();
	var tempoEstimado = $("#tempo").val();

	$("#erroServico").html("");
	$("#erroNome").html("");
	$("#erroPreco").html("");
	$("#erroDescricao").html("");

	$.post(base_url+"salvarServico",{
		tipoServico: servico,
		preco: precoServico,
		nome: nomeServico,
		descricao: descricaoServico,
		tempo: tempoEstimado
	}, function(data) {
		if (data.tipo == "alert") {
			$("#erroServico").html(data.retorno.tipoServico);
			$("#erroNome").html(data.retorno.nome);
			$("#erroPreco").html(data.retorno.preco);
			$("#erroDescricao").html(data.retorno.descricao);
			$("#erroTempo").html(data.retorno.tempo);
		} else {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: 'Serviço cadastrado!'
			});

			setTimeout(function() {
				location.reload();
			}, 900);

			$("#modal-cadastroServico").modal('hide');
		}
	}, "JSON");
});

$(".modal-footer").on("click", "#salvarSobrePetshop", function() {
	var nomeSobre = $("#nomePetshop").val();
	var cnpjSobre = $("#cnpj").val();
	var razaoSocial = $("#razaoSocial").val();
	var aberturaSobre = $("#horarioAbertura").val();
	var fechamentoSobre = $("#horarioFechamento").val();
	var enderecoSobre = $("#endereco").val();
	var descricaoSobre = $("#descricaoSobre").val();
	var contato = $("#telefone").val();
	var celular = $("#celular").val();

	$.post(base_url+"salvarSobre", {
		nome: nomeSobre,
		cnpj: cnpjSobre,
		razaoSocial: razaoSocial,
		abertura: aberturaSobre,
		fechamento: fechamentoSobre,
		endereco: enderecoSobre,
		descricao : descricaoSobre,
		telefone: contato,
		celular: celular
	}, function (data) {
		if (data.tipo == "alert") {
			$("#erroNomeSobre").html(data.retorno.nome);
			$("#erroAbertura").html(data.retorno.abertura);
			$("#erroFechamento").html(data.retorno.fechamento);
			$("#erroDescricaoSobre").html(data.retorno.descricao);
			$("#erroEndereco").html(data.retorno.endereco);
			$("#erroCNPJSobre").html(data.retorno.cnpj);
			$("#erroRazaoSocialSobre").html(data.retorno.razaoSocial);
			$("#errorCelular").html(data.retorno.razaoSocial);
		} else {

			$("#erroNomeSobre").html("");
			$("#erroAbertura").html("");
			$("#erroFechamento").html("");
			$("#erroDescricaoSobre").html("");
			$("#erroEndereco").html("");
			$("#erroCNPJSobre").html("");
			$("#erroRazaoSocialSobre").html("");
			$("#errorCelular").html("");


			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: 'Informações atualizada!'
			});


			$("#modal-footer").modal('hide');

			setTimeout(function() {
				location.reload();
			}, 100);
		}
	}, "JSON");
});

$(".editarServico").on("click", function() {
	var servico = $(this).attr("data-servico");
	$.post(base_url+"obterServico",{
		codigoServico: servico
	}, function(data) {
		if(data.tipo == "success") {
			$(".tipoConsulta").val(data.servico.nome);
			$(".tipoServico").val(data.servico.fk_idTipoServico);
			$("#preco").val(data.servico.preco);
			$("#descricao").val(data.servico.descricao);

			$(".modal-footer").html(
				"<button type='button' class='btn btn-secondary voltar'>Voltar</button>"+
				"<button type='button' class='btn btn-white botaoModalServico'  data-servico ='"+servico+"'"+
				"id='alterarServico'>Salvar </button>");

			$("#modal-title-notification").html("Editar servico");
			$(".tipoServico").attr("disabled", true);

		}
		$("#modal-cadastroServico").modal();
	},"JSON");
});

$(".modal-footer").on("click", "#alterarServico", function() {
	var servico = $(this).attr("data-servico");
	var nomeServico = $(".tipoConsulta").val();
	var precoServico = $("#preco").val();
	var descricaoServico = $("#descricao").val();
	var horario = [];

	$("#erroServico").html("");
	$("#erroNome").html("");
	$("#erroPreco").html("");
	$("#erroDescricao").html("");

	$(".horario:checked").each(function(){
		horario.push($(this).val());
	});

	$.post(base_url+"alterarServico", {
		codigoServico: servico,
		preco: precoServico,
		descricao: descricaoServico,
		horarios:horario
	}, function(data) {
		if (data.tipo == "alert") {
			$("#erroPreco").html(data.retorno.preco);
			$("#erroDescricao").html(data.retorno.descricao);
		} else {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: 'Serviço alterado!'
			});

			setTimeout(function() {
				location.reload();
			}, 900);

			$("#modal-cadastroServico").modal('hide');
		}
	}, "JSON");
});

$(".excluirServico").on("click", function() {
	var servico = $(this).attr("data-servico");

	$.post(base_url+"deletarServico", {
		codigoServico: servico
	}, function(data) {
		
		if (data.tipo == "success") {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: 'Serviço excluido!'
			});

			setTimeout(function() {
				location.reload();
			}, 900);
		}
	},"JSON");
});

$("#buscarAtendimento").on("click", function() {
	var data_servico = $("#data").val();
	var situacao_servico = $("#situacao").val();

	$.post(base_url+"buscarAtendimento",{
		data: data_servico,
		situacao: situacao_servico
	}, function(data) {
		if (data.tipo == "success") {
			$(".atendimentos").html(data.html);
		} else {
			$(".atendimentos").html(data.mensagem);
		}
	},'JSON');
});

$(".atendimentos").on("click", ".ver-atendimento", function() {
	var codigoAgendamento = $(this).attr("data-agendamento");

	$.post(base_url+"detalharAtendimento",{
		codigoAtendimento: codigoAgendamento
	}, function(data) {
		if (data.tipo == "success") {
			$(".detalheAtendimento").html(data.html);
			$("#titulo").html(data.titulo);
			$(".salvarStatus").attr("disabled", false);

			if (data.situacao == 2) {
				$(".salvarStatus").html("Confirmar cancelamento?");
				$(".salvarStatus").attr("data-situacao", data.situacao);

			} else if (data.situacao == 1) {
				$(".salvarStatus").html("Atendimento realizado!");
				$(".salvarStatus").attr("disabled", true);
			} else if (data.situacao == 3) {
				$(".salvarStatus").html("Atendimento cancelado!");
				$(".salvarStatus").attr("disabled", true);
			} else if (data.situacao == 0) {
				$(".salvarStatus").html("Atendimento realizado?");
				$(".salvarStatus").attr("data-situacao", data.situacao);
			}

			$(".salvarStatus").attr("data-codigoAtendimento", data.codigoAtendimento);
			$("#modal-atendimento").modal();
		}
	},'JSON');
});

$(".salvarStatus").on("click", function() {
	
	var situacao = $(this).attr('data-situacao');
	var codigoAtendimento = $(this).attr('data-codigoAtendimento');

	if (situacao == 0) {
		var mensagem = "O atendimento foi realizado?";
		var status = 1;
	} else if (situacao == 2) {
		var mensagem = "Deseja confirmar o cancelamento?";
		var status = 3;
	}
	
	if (situacao == 0 || situacao == 2) {

		$("#modal-atendimento").modal('hide');
		Swal.fire({
			title: mensagem,
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'sim',
			cancelButtonText: 'Não'
		}).then((result) => {
			
			if (result.value) {
				$.post(base_url+"atualizarAtendimeto",{
					codigoAtendimento: codigoAtendimento,
					status: status
				}, function(data) {
					if (data.tipo == "success") {
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 3000
						});

						Toast.fire({
							type: 'success',
							title: 'Atendimento atualizado!'
						});
					}
				}, "JSON");
			}
		});
	}
});

$(".modal-footer").on("click", ".voltar", function() {
	$("#modal-cadastroServico").modal('hide');
	setTimeout(function() {
		location.reload();
	}, 2);
});

$(".btn-ativar").on("click", function () {
	var urlPagina =  window.location.href;
	
	$("#modal-ativarServico").modal('hide');
	
	if (urlPagina.match("vacinas"))  {		
		$.post(base_url+"ativarServicoVacina", {
		}, function(data) {	
			if (data.tipo == "success") {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'success',
					title: 'Servico ativado com sucesso!'
				});

				setTimeout(function() {
					location.reload();
				}, 900);
			}
		}, 'JSON');
	}
});

$("#btn-buscarCartaoVacina").on("click", function() {
	var situacao = $("#situacao").val();
	var nome = $("#nome").val();

	$.post(base_url+"buscarCartaoVacina", {
		situacao: situacao,
		nome: nome
	}, function(data) {	
		$(".listarCartao").html(data.html);
	}, 'json');
});

$("#dataVacina").on("change", function() {
	$("#alertaHorarioReservado").html("carregando...");
	$.post(base_url+"verificarHorarioDisponivel", {
		data: $(this).val(),
		codigoPetshop: $(".btn-agendar").attr('data-codigoPetshop')
	}, function(data) {	
		if (data.tipo == "success") {
			$(".horariosVancinas").html(data.horarios);
		} else {
			$(".horariosVancinas").html(data.mensagem);
		}
	}, 'json');
});


$(".btn-agendar").on("click", function() {
	var urlPagina =  window.location.href;
	var urlPagina = urlPagina.split("/");
	
	$("#errorVacina").html("");
	$("#errorFuncionario").html("");
	$("#erroData").html("");
	$("#alertaHorarioReservado").html("");
	$("#erroValor").html("");

	$.post(base_url+"agendarVacina", {
		codigoFuncionario: $("#funcionario").val(),
		codigoCartao: urlPagina.pop(),
		vacina: $("#vacina").val(),
		data: $("#dataVacina").val(),
		valor: $("#valor").val(),
		horario: $("input[name='customRadioInline1']:checked").val()
	},function(reserva) {
		if (reserva.tipo == "success") {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: reserva.mensagem
			});

			setTimeout(function() {
				location.reload();
			}, 900);			
		} else {
			$("#errorVacina").html(reserva.campos.vacina);
			$("#errorFuncionario").html(reserva.campos.codigoFuncionario);
			$("#erroData").html(reserva.campos.data);
			$("#alertaHorarioReservado").html(reserva.campos.horario);
			$("#erroValor").html(reserva.campos.valor);
		}

	}, 'JSON');	
});

$(".listaVacina").on("click", ".btn-abriVacina", function() {
	$.post(base_url+"Sistema_interno/petshop/Vacina/detalharConsultaVacina", {
		codigoVacina: $(this).attr("data-vacina")
	}, function(data) {
		if(data.tipo == "success") {
			$(".detalheConsultaVacina").html(data.html);
			$("#modal-consultaVacina").modal();
		}
	}, "JSON");
});




$(".cadastrarFuncionario").on("click", function() {
	var nome_vte = $("#nome").val();
	
	$("#errorNome").html('');

	var horario = [];
	$(".horario:checked").each(function(){
		horario.push($(this).val());
	});

	$.post(base_url+"registrarVeterinario", {
		nome: nome_vte,
	},function(data) {
		if (data.tipo == "success") {
			$("#modal-funcionario").modal('hide');

			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: 'Funcionario cadastrado com sucesso!'
			});

			setTimeout(function() {
				location.reload();
			}, 900);
		} else {
			$("#errorNome").html(data.campos.nome);
		}
	}, 'JSON');

});

$(".listar-veterinario").on("click", function() {
	$.post(base_url+"listarVeterinarios", {
	}, function(data) {	
		$(".painel").html(data.html);
	}, 'json');
});

$(".funcionario").on("click", "#atualizarFuncionario", function() {
	var nome_vte = $("#nome").val();
	var ra_vte = $("#ra").val();
	
	$("#errorNome").html('');
	$("#erroHorarioCadastro").html('');

	var horario = [];
	$(".horario:checked").each(function(){
		horario.push($(this).val());
	});

	$.post(base_url+"atualizarFuncionario", {
		nome: nome_vte,
		ra: ra_vte,
		horarios: horario,
		codigoVeterinario: $(this).attr("data-veterinario")
	},function(data) {
		if (data.tipo == "success") {
			$("#modal-cadastroVeterinario").modal('hide');

			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: 'Funcionario atualizado com sucesso!'
			});

			setTimeout(function() {
				location.reload();
			}, 900);
		} else {
			
			$("#errorNome").html(data.campos.nome);
			$("#erroHorarioCadastro").html(data.campos.horarios);
		}
	}, 'JSON');

});

$(".painel").on("click", ".btn-editarVeterinario", function() {
	$("#modal-title-funcionario").html("Editar funcionario");
	var codigoVeterinario = $(this).attr("data-veterinario");
	
	$.post(base_url+"editarFuncionario", {
		codigoVeterinario: $(this).attr("data-veterinario")
	}, function(data) {	
		if (data.tipo == 'success') {
			$("#nome").val(data.veterinario.nome);
			$("#ra").val(data.veterinario.ra);

			var totalHorario = 23;
			for(var i = 0; i < totalHorario; i++) {
				$("#"+(i+1)).attr("checked", false);	
			}

			// Marcar os horario no formulario
			for(var i = 0; i < data.horarios.length; i++) {
				$("#"+data.horarios[i].codigoHora).attr("checked",true);	
			}
		}

		$(".botaoModal").attr("data-veterinario", codigoVeterinario);
		$(".botaoModal").attr("id","atualizarFuncionario");
		$("#modal-cadastroVeterinario").modal();

	},"json");
});



$(".btn-desativarVeterinario").on("click", function() {
	Swal.fire({
		title: 'Você deseja realizar a exclusão do funcionario?',
		html: '',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'sim',
		cancelButtonText: 'Não'
	}).then((result) => {

		if (result.value) {
			$.post(base_url+"deletarVeterinario", {
				codigoVeterinario: $(this).attr("data-veterinario")
			}, function(data) {	
				if (data.tipo == "success") {
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
					});

					Toast.fire({
						type: 'success',
						title: 'Funcionario excluido com sucesso!'
					});

					setTimeout(function() {
						location.reload();
					}, 900);
				}
			}, 'json');
		}
	});
});


$(".listaVacina").on("click", ".cancelar-vacina", function() {
	Swal.fire({
		title: 'Deseja cancelar a consulta?',
		html: '',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'sim',
		cancelButtonText: 'Não'
	}).then((result) => {

		if (result.value) {
			$.post(base_url+"atualizarConsultaVacina", {
				codigoVacina: $(this).attr("data-vacina"),
				situacao: '3'
			}, function(data) {	
				if (data.tipo == "success") {
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
					});

					Toast.fire({
						type: 'success',
						title: 'Consulta atualizada!'
					});

					setTimeout(function() {
						location.reload();
					}, 900);
				}
			}, 'json');
		}
	});
});

$("#btn-historico_vacina").on("click", function() {
	$(".div-listaVacina").removeClass('hidden');
	$(".div-agendamento-vacina").removeClass('show');

	$.post(base_url+"listarVacina", {
		codigoCartao: $(this).attr("data-vacina"),
		situacao: ['2','3']
	}, function(data) {
		if(data.tipo == "success") {
			$("#titulo-consulta").html("<h5>Historico de vacinas</h5>");
			$(".listaVacina").html(data.html);
		}
	}, "JSON");
});

$("#btn-vacina_pendente").on("click", function() {
	$(".div-listaVacina").removeClass('hidden');
	$(".div-agendamento-vacina").removeClass('show');

	$.post(base_url+"listarVacina", {
		codigoCartao: $(this).attr("data-vacina"),
		situacao: '1'
	}, function(data) {
		if(data.tipo == "success") {
			$("#titulo-consulta").html("<h5>Vacinas agendadas</h5>")
			$(".listaVacina").html(data.html);
		}
	}, "JSON");
});

$(".listaVacina").on("click", ".confirmar-atendimento", function() {
	Swal.fire({
		title: 'A consulta foi realizada?',
		html: '',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'sim',
		cancelButtonText: 'Não'
	}).then((result) => {

		if (result.value) {
			$.post(base_url+"atualizarConsultaVacina", {
				codigoVacina: $(this).attr("data-vacina"),
				situacao: '2'
			}, function(data) {	
				if (data.tipo == "success") {
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
					});

					Toast.fire({
						type: 'success',
						title: 'Consulta atualizada!'
					});

					setTimeout(function() {
						location.reload();
					}, 900);
				}
			}, 'json');
		}
	});
});

$("#salvarDadosBancarios").on('click', function() {
	$.post(base_url+'gerarTokenSMS', {}, function(data) {
		if(data.tipo == 'success') {
			$("#modal-autenticacao").modal();
		}
	}, 'json');
});

$("#confirmarDadosBancarios").on("click", function() {
	$.post(base_url+"salvarDadosBancario", {
		agencia: $("#Agencia").val(),
		digitoAgencia: $("#digitoAgencia").val(),
		banco: $("#banco").val(),
		numeroConta: $("#conta").val(),
		digitoConta: $("#digitoConta").val(),
		tipoConta: $("#tipoConta").val(),
		codigo: $("#codigo").val()
	}, function(data) {
		if(data.tipo == "success") {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			Toast.fire({
				type: 'success',
				title: 'Dados bancarios salvo com sucesso!'
			});

			$("#modal-autenticacao").modal('hide');

			setTimeout(function() {
				location.reload();
			}, 900);
		} else {
			if(data.mensagem != "") {
				$("#erroCodigo").html(data.mensagem);
			} else {
				$("#modal-autenticacao").modal('hide');
				$("#erroAgencia").html(data.campos.agencia);
				$("#erroDigitoAgencia").html(data.campos.digitoAgencia);
				$("#erroDigitoBanco").html(data.campos.banco);
				$("#erroNumeroConta").html(data.campos.numeroConta);
				$("#erroDigitoConta").html(data.campos.digitoAgencia);
				$("#erroTipoConta").html(data.campos.tipoConta);
			}
		} 
	}, "JSON");
});


function ativarModal() {
	$("#sobre").click();
}

function notificarPetShop() {
	$.post(base_url+"notificarPetshop", {
	}, function(data) {
		if (data.tipo == "success") {
			$.notify({
				message: data.mensagem
			},{
				type: 'default',
				timer: 5000,
				placement: {
					from: 'top',
					align: 'right'
				}
			});
		}
	}, 'JSON');
}

function alterarAbaNavegacao() {
	var urlPagina =  window.location.href;

	if (urlPagina.match("financeiro")) {
		$(".dir1").addClass("active");
		$(".dir2").removeClass("active");
		$(".dir3").removeClass("active");
		$(".dir4").removeClass("active");
		$(".dir5").removeClass("active");
		$(".dir6").removeClass("active");
		$(".dir7").removeClass("active");

	} else if(urlPagina.match("servicos")) {
		$(".dir1").removeClass("active");
		$(".dir2").addClass("active");
		$(".dir3").removeClass("active");
		$(".dir4").removeClass("active");
		$(".dir5").removeClass("active");
		$(".dir6").removeClass("active");
		$(".dir7").removeClass("active");
	} else if(urlPagina.match("vacinas")) {
		$(".dir1").removeClass("active");
		$(".dir2").removeClass("active");
		$(".dir3").removeClass("active");
		$(".dir4").removeClass("active");
		$(".dir5").addClass("active");
		$(".dir6").removeClass("active");
		$(".dir7").removeClass("active");
	}
}

function AnaliseServico() {
	var urlPagina =  window.location.href;

	if (urlPagina.match("vacinas")) {
		$.post(base_url+"analisaServicoAtivo", {
		}, function(data) {
			if (data.tipo == 'alert') {
				$(".conteudo-ativar").html(
					"<img class='card-img-top' src='"+base_url+"assets/personalizado/imagem/vacinaPetshop.jpg'>"
					);
				$("#modal-ativarServico").modal();
			}
		},'JSON');
	}
}


alterarAbaNavegacao();
notificarPetShop();
AnaliseServico();