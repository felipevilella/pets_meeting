function atualizacaoChat() {
  var  refreshIntervalId =  setInterval(function mensagem_chat(){
    var idusuario_usuario = $("#identificadorusuario0").val();
    var idamigo_usuario = $("#idDestinario").val();
    $.ajax({
      url:base_url+'mensagem',
      type:'post',
      dataType:"Json",
      data:{
        idusuario: idusuario_usuario,
        idamigo: idamigo_usuario,
      },
      success:function(data){
        $("#chat-messages").html(data.html);
        clearInterval(refreshIntervalId);
        $('#chat-messages').animate({
          scrollTop: $('#chat-messages')[0].scrollHeight}, "slow");
      }, error:function(data){
        clearInterval(refreshIntervalId);
      }
    });
  },1000);

}

// Obter o id do usuario para buscar as mensagem
$("#mensagem0").on("click",function() {
  var id = $("#identificadorusuario0").val();
  var nome = $("#nomeUsuario0").val();

  //inserindo o nome no chat
  $("#nomeChat").html(nome);
  $("#idDestinario").val(id);

  atualizacaoChat();
});

$("#mensagem1").on("click",function() {
  var id = $("#identificadorusuario1").val();
  var nome = $("#nomeUsuario1").val();

  //inserindo o nome no chat
  $("#nomeChat").html(nome);
  $("#idDestinario").val(id);
  atualizacaoChat();
});

$("#mensagem2").on("click",function(){
  var id = $("#identificadorusuario2").val();
  var nome = $("#nomeUsuario2").val();

  //inserindo o nome no chat
  $("#nomeChat").html(nome);
  $("#idDestinario").val(id);
  atualizacaoChat();
});

$("#mensagem3").on("click",function(){
  var id = $("#identificadorusuario3").val();
  var nome = $("#nomeUsuario3").val();

  //inserindo o nome no chat
  $("#nomeChat").html(nome);
  $("#idDestinario").val(id);
  atualizacaoChat();
});

$("#mensagem4").on("click",function(){
  var id = $("#identificadorusuario4").val();
  var nome = $("#nomeUsuario4").val();

  //inserindo o nome no chat
  $("#nomeChat").html(nome);
  $("#idDestinario").val(id);
  atualizacaoChat();
});

$("#mensagem5").on("click",function(){
  var id = $("#identificadorusuario5").val();
  var nome = $("#nomeUsuario5").val();

  //inserindo o nome no chat
  $("#nomeChat").html(nome);
  $("#idDestinario").val(id);
  atualizacaoChat();
});


/*// Digitar a mensagem
var typingTimer; 
var doneTypingInterval = 5000; 
$('#digitarMensagem').keyup(function() {
  var nome = $("#nomeusuario").val();

  $("#retorno").html( nome +" está digitando...</font>");
  if ($('#myInput').val) {
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
  }
});*/


//user is "finished typing," do something
function doneTyping() {
  $("#retorno").html('');
}



$(document).keypress(function(e) {
  if (e.which == 13) {
    $('#enviar_chat').trigger('click'); 
  }
});
//Enviar a mensagem
$("#enviar_chat").on("click",function()  {
 $("#retorno").html("");

 var idusuario_usuario = $("#identifuser").val();
 var idamigo_usuario = $("#idDestinario").val();
 var mensagem_usuario = $("#digitarMensagem").val();

 if(mensagem_usuario != "") {
   $.ajax( {
     url:base_url+'enviarMensagem',
     type:'post',
     dataType:"Json",
     data: {
      idusuario: idusuario_usuario,
      idamigo: idamigo_usuario,
      mensagem: mensagem_usuario
    },
    success:function(data) {
      atualizacaoChat();
    }
  });
   
 }
 $("#digitarMensagem").val("");
});
notificacao();


// Pusher

Pusher.logToConsole = false;
var pusher = new Pusher('2bc20d8431a54e1665b3', {
    cluster: 'eu',
    forceTLS: true
});

var channelchat = pusher.subscribe('chat');
channelchat.bind('enviar-mensagem', function(data) {
  atualizacaoChat();
});
