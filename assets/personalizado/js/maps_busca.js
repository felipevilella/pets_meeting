function initPage(){

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position){
      latitude = position.coords.latitude;
      longitude = position.coords.longitude;
      initMap();
    });

  }
}

var latitude = $("#latitude").val();
var longitude = $("#longitude").val();

function initMap(latitude, longitude) {
  var mapPropriedade =  {
    center: new google.maps.LatLng(latitude, longitude),
    zoom: 17,
    draggable: true,
    zoomControl: false,
    scrollwheel: false,
    panControl: false,
    scaleControl: false,
    streetViewControl: false,
    disableDefaultUI: true,
  }
    map = new google.maps.Map(document.getElementById('map'), mapPropriedade);
}



initMap(latitude, longitude);

  // Definindo o icone do petshop no mapa
  var icons = {
      petshop: {
        icon: base_url + 'assets/personalizado/imagem/petshop.png'
      }
  }; 

  initMap(latitude, longitude);
  var point = new google.maps.LatLng(latitude, longitude);
  var marker = new google.maps.Marker({
     map: map,
     position: point,
     icon: icons["petshop"].icon
  });
  
}

window.onload = initPage;







