<?php

function conveterData($data){
	$data = explode("-", $data);
    return $data[2]."/".$data[1]."/".$data[0];
}

function tirarAcentos($texto) {
	return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"), $texto);
}

function removerCaracterNaoNumericos($texto) {
	return preg_replace("/[^0-9]/", "", $texto);
}

