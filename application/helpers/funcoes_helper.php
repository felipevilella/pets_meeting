<?php 
function calcularDistancia($lat1, $long1, $lat2, $long2) {
	$lat1 = deg2rad($lat1);
	$lat2 = deg2rad($lat2);
	$long1 = deg2rad($long1);
	$long2 = deg2rad($long2);


	$dist = (6371 * acos(cos($lat1) * cos($lat2) * cos($long2 - $long1) + sin($lat1) * sin($lat2)));
	$dist = number_format($dist, 2, ',', '');
	$dist = explode(",", $dist);
	$dist = $dist[0].",".substr($dist[1], 0,2);

	return $dist;
}

function formatarMonetario($valor) {
	return number_format($valor, 2, ',', '.');
}

function padronizaHorario($horario) {
	return date("H:i", strtotime($horario));
}

function converterData($data) {
	return date("d/m/Y", strtotime($data));
}

function converterDataMes($data) {
	return date("d/m", strtotime($data));
}

function converterDataEng($data) {
	$data = explode("/", $data);
	return $data[2]."-".$data[1]."-".$data[0];
}

function filtrarDiaSemana($numero) {
	switch ($numero) {
		case '0':
			return "Dom";
			break;
		case '1':
			return "Seg";
			break;
		case '2':
			return "Ter";
			break;
		case '3':
			return "Qua";
			break;
		case '4':
			return "Qui";
			break;
		case '5':
			return "Sex";
			break;
		case '6':
			return "Sab";
			break;
	}
}

function filtrarDiaSemanaExtenso($numero) {
	switch ($numero) {
		case '0':
			return array("Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado");
			break;
		case '1':
			return array("Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo");
			break;
		case '2':
			return array("Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo", "Segunda");
			break;
		case '3':
			return array("Quarta", "Quinta", "Sexta", "Sábado", "Domingo", "Segunda", "Terça");
			break;
		case '4':
			return array("Quinta", "Sexta", "Sábado", "Domingo", "Segunda", "Terça", "Quarta");
			break;
		case '5':
			return array("Sexta", "Sábado", "Domingo", "Segunda", "Terça", "Quarta", "Quinta");
			break;
		case '6':
			return array("Sábado", "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta");
			break;
	}
}

function redondarValor($valor) {
	$valorDecimal = explode(".", $valor);

	if(intval($valorDecimal[1]) >= 50) {
		return ceil($valor);
	} else {
		return floor($valor);
	}
}

function horarioServidor() {
	return date('H:i:s', strtotime(date('H:i:s').' + 1 hours'));
}

function removerMascara($numero) {
	return preg_replace("/[^0-9]/", "", $numero);
}

function removerEspacoCartao($string) {
	$texto = explode(" ", $string);
	return $texto[0]."".$texto[1]."".$texto[2]."".$texto[3];    
}

function montarHorario($horarioInicial, $horarioFinal, $tempoServico) {
	$horarioDisponiveis = array();
	$posicao = 0;
	
	# Montar o horario de acordo com os serviços
	while (strtotime($horarioInicial) < strtotime($horarioFinal)) {
		
		if ($posicao == 0) {
			$horarioDisponiveis[$posicao] = padronizaHorario($horarioInicial);
			$posicao++;
		} 

		# Calcular horario
		$hora = calcularHorario($horarioInicial, $tempoServico);	
		$horarioInicial = $hora;
		
		# Montando o horario
		$horarioDisponiveis[$posicao] = $hora;
		$posicao++; 
	}
	
	return $horarioDisponiveis;
}

/**
* @author Felipe Vilella
* @param String horario
* @param String horaAdicional
* @return String hora
**/
function calcularHorario($horario, $horaAdicional) {

	$horaAdicional = explode(":", padronizaHorario($horaAdicional));
	
	if ($horaAdicional[0] == '00') {
		$hora = date('H:i', strtotime(padronizaHorario($horario).' +'.$horaAdicional[1].' minutes'));
	} else {
		if (isset($hora)) {
			$hora = date('H:i', strtotime($hora.' +'.$horaAdicional[0].' Hours'));
		} else {
			$hora = date('H:i', strtotime(padronizaHorario($horario).' +'.$horaAdicional[0].' Hours'));
		}
	}

	return $hora;
}

/**
* Reponsavel por reconhecer qual dispositivo que o cliente está utilizando
* @author Felipe Vilella
**/
function reconhecerDispositivo() {
	$ci =& get_instance();
	$ci->load->library('user_agent');

	$dispositivo = $ci->agent->mobile();
	$navagedor = $ci->agent->browser();
	$versao = $ci->agent->version();
	$plataforma = $ci->agent->platform();

	return array(
		'dispositivo' => $dispositivo,
		'navagedor' => $navagedor,
		'versao' => $versao,
		'plataforma' => $plataforma
	);
}

function verificarFotoUsuario($foto) {
	$ci =& get_instance();
	$ci->load->helper('url');

	if ($foto) {
		return base_url("assets/personalizado/foto_usuario/".$foto);
	} else {
		return base_url("assets/personalizado/imagem/avatar.jpg");
	}
}

function verificarFotoAnimal($foto) {
	$ci =& get_instance();
	$ci->load->helper('url');

	if ($foto) {
		return base_url("assets/personalizado/fotos_pets/".$foto);
	} else {
		return base_url("assets/personalizado/imagem/addFoto.jpg");
	}
}

function verificarFotoServico($idServico) {
	if ($idServico == 3) {
		return base_url("assets/personalizado/imagem/artes/veterinario.jpg");
	} else {
		return base_url("assets/personalizado/imagem/artes/banho.jpg");
	}
}

function verificarFotoPetshop($foto) {
	if ($foto) {
		return  base_url("assets/personalizado/foto_usuario/".$foto);
	} else {
		return base_url("assets/personalizado/imagem/semfoto.jpg");
	}
}


function montarAnuncioDesaparecimento($lat, $lng) {
	$ci =& get_instance();

	$ci->load->model("desaparecimento_model");
	$ci->load->model("localizacao_model");
	$ci->load->model("pets_model");

	$ci->load->helper("form_helper");

	$desaparecidos =  $ci->desaparecimento_model->getDesaparecimento();
	$listaDesparecimento = array();

	foreach ($desaparecidos as $desaparecido) {
		$animal = $ci->pets_model->buscarPetsPorIdPets($desaparecido["fk_idpets"]);
		$localizacao = $ci->localizacao_model->getLocalizacaoPorIdUsuario($animal["fk_idUsuario"]);

		# Diferença entre os pontos
		$distancia = calcularDistancia($lat, $lng, $localizacao["latitude"], $localizacao["longitude"]);
		
		if(doubleval($distancia) < 30 && doubleval($distancia) != 0) {
			$informacao = array(
				"nome" => strtoupper($animal["nome"]),
				"fotoPrincipal" => $animal["fotoPrincipal"],
				"raca" => $animal["racaAnimal"],
				"texto" => $desaparecido["texto"],
				"ultimoLugar" => $desaparecido["ultimoLugar"],
				"dataDesaparecimento" => conveterData($desaparecido["data"]),
				"distancia" => $distancia
			);
			array_push($listaDesparecimento, $informacao);
		}	
	}

	return $listaDesparecimento;
}

function obterLocalizaçãoPorIP() {
	$ip = $_SERVER["REMOTE_ADDR"];
	$dadosLocalizacao = file_get_contents("https://freegeoip.app/json/$ip");

	$data = json_decode($dadosLocalizacao, true);
	return $data;
}

function obterLocalizacaoPorEndereco($endereco) {
	$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCAAbe8Z0i7XexLH7hooViwxV-bBAYo-Mw&address=" . urlencode($endereco);
	$resultado = json_decode(file_get_contents($url));

    return $resultado->results[0]->geometry->location;
}

function salvarLocalizacao($latitude, $longitude) {
	$ci =&  get_instance();

	$ci->load->library('session');
	$ci->load->model('localizacao_model');
	$ci->load->model('usuario_model');
	$ci->load->model('login_model'); 

	$idusuario = $ci->session->idusuario_session;

	$dadosLocalizacao = array(
		"latitude" => $latitude,
		"longitude" => $longitude,
		"fk_idusuario" => $idusuario
	);

	$dadosAtualizacao = array("localizacao" => "0");

	$usuario = $ci->usuario_model->buscarUsuario($ci->session->idusuario_session);
	$sessaoUsuario = array(
		"email" => $usuario["email"],
		"senha" => $usuario["senha"],
		"idUsuario" => $usuario["idUsuario"],
		"nome"=> $usuario["nome"],
		"comercial" => $usuario["comercial"],
		"longitude" => $longitude,
		"latitude" => $latitude,
		"estado"=> $usuario['fk_idEstado'],
		"cidade" => $usuario["fk_idCidade"]
	);

	$ci->login_model->session_usuario($sessaoUsuario);
	$ci->localizacao_model->atualizatStatusAtivacao($idusuario, $dadosAtualizacao);

	$localizacao = $ci->localizacao_model->getLocalizacaoPorIdUsuario($usuario['idUsuario']);

	if ($localizacao) {
		$ci->localizacao_model->deletarLocalizacao($usuario['idUsuario']);
	}
	
	$ci->localizacao_model->setLocalizacaoAtual($dadosLocalizacao);

	return json_encode(array(
		"tipo" => "success",
		"retorno" => "localizacao salvar com sucesso"
	));
}

function mySortDistancia($distanciaA, $distanciaB) {
	if ($distanciaA == $distanciaB) {
		return 0;
	} 

	return($distanciaA < $distanciaB) ? -1:1;
}


function filtrarCidade($nome) {
	$ci =& get_instance();
	$ci->load->model('localizacao_model');

	$cidades = $ci->localizacao_model->buscarCidade();

	foreach ($cidades as $cidade) {
		if (strtolower($cidade['nome']) == strtolower($nome)) {
			return $cidade['idcidade'];
		}
	}
}

function filtrarEstado($nome) {
	$ci =& get_instance();
	$ci->load->model('localizacao_model');

	$estados = $ci->localizacao_model->buscarEstado();

	foreach ($estados as $estado) {
		if (strtolower($estado['sigla']) == strtolower($nome)) {
			return $estado['idestado'];
		}
	}
}

function definirPorcentagemPagamento($valorTotal, $valorUnitario) {
	$valorComDesconto = doubleval(($valorTotal*90)/100);
	return doubleval(($valorUnitario*100) / $valorComDesconto);
}