 <?php

 function optionsPusher() {
 	$options = array(
 		'cluster' => 'us2',
 		'useTLS' => true
 	);

 	return $options;
 }

/**
* Canal  pusher
* @author Felipe Vilella
**/
function canalPusher($nomeCanal, $nomeEvento, $informacao = "") {
 	# incluindo o arquivo pusher
 	require 'vendor/autoload.php';

 	# Obtendo os dados da chave do pusher
 	$options = optionsPusher();
 	$pusher = new Pusher\Pusher(
		'8e4bd12b694e6a7ce91e',
		'803f250fe7b3225dc68b',
		'920347',
		$options
	);

 	# Chamado o evento do pusher
 	$pusher->trigger($nomeCanal, $nomeEvento, $informacao);	
}
