<?php
	function gerarBoleto($dados) {
		$dadosTransacao = array(
			'amount' => intval(removerMascara($dados['valor'])),
			'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME,
			'payment_method' => 'boleto',
			'customer' => array(
				'type' => 'individual',
				'country' => 'br',
				'name' => $dados['nome'],
				'documents' => array(array(
					'type' => 'cpf',
					'number'=> $dados['cpf']
				))
			)
		);

		$gerarBoleto = curl_post('transactions', $dadosTransacao);

		if (array_key_exists('errors',$gerarBoleto)) {
			return array('tipo' => 'alert', 'mensagem' => $gerarBoleto['errors'][0]['message']);
		} else if(array_key_exists('status', $gerarBoleto)) {
			if ($gerarBoleto['status'] == 'authorized') {
				return array(
					'tipo' => 'success',
				 	'mensagem' => 'boleto gerado com sucesso',
				 	'url' => $gerarBoleto['boleto_url'],
				 	'codigoBarra' => $gerarBoleto['boleto_barcode'],
				);
			}
		}
    }

    function cartaoCredito($dados) {
    	$items = array();
    	$recipients = array();
    	
    	# Obter produtos
    	foreach ($dados['produtos'] as $produto) {
    		$produto = array(
				"id" =>  $produto['idServico'],
		        "title"=>  $produto['nome'],
		        "unit_price" => intval(removerMascara($produto['preco'])),
		        "quantity" => '1',
		        "tangible" => true
		    );

		    array_push($items, $produto);
    	}

    	# Obter recebedores do produto
    	foreach ($dados['recebedores'] as $recebedor) {
    		$recebedores = array(
				"recipient_id" => $recebedor['idRecebedor'],
		      	"percentage" => $recebedor['valorPorcetagem'],
		      	"liable" => false,
		      	"charge_processing_fee" => false
			);

			array_push($recipients, $recebedores);
    	}

    	# adicionar rebedor pets meeting
    	$recebedorPetsMeeting = array(
    		"recipient_id" => 're_ck4yxzgnx07rcv06e0ycdrrmi',
		    "percentage" => 10,
		    "liable" => true,
		    "charge_processing_fee" => true
		);

		array_push($recipients, $recebedorPetsMeeting);


		$dadosCartaoCredito = array(
			'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME,
			'amount' =>  intval(removerMascara($dados['total'])),
			'card_holder_name' => $dados['nomePortador'],
			'card_number' => $dados['numeroCartao'],
			'card_cvv' => $dados['codigoCVV'],
			'card_expiration_date' => $dados['expiracaoData'],
			'customer'=> array(
				"external_id" => $dados['idCliente'],
		      	"name" => $dados['nomeCliente'],
		      	"type" => "individual",
		      	"country" => "br",
		      	"email" => $dados['email'],
		      	"documents" => array(array(
		      		'type' => 'cpf',
					'number'=> $dados['cpf']
		      	)),
		      	'phone_numbers' => array($dados['telefone']),
			), 
			"billing" => array(
				"name" => $dados['nomeCliente'],
		      	"address" => array (
		        	"country"=> "br",
		        	"state"=> $dados['estado'],
		        	"city"=> $dados['cidade'],
		        	"neighborhood"=> $dados['bairro'],
		        	"street"=> $dados['rua'],
		        	"street_number"=> $dados['numero'],
		        	"zipcode"=> $dados['cep']
		      	)
			), 
			'items' => $items, 
			"split_rules" => $recipients
			);

		$gerarPagamento = curl_post('transactions', $dadosCartaoCredito);

		if (array_key_exists('errors', $gerarPagamento)) {
			return array('tipo' => 'alert', 'mensagem' => $gerarPagamento['errors'][0]['message']);
		} else if(array_key_exists('status', $gerarPagamento)) {
			if ($gerarPagamento['status'] == 'authorized') {
				return array(
					'tipo' => 'success',
				 	'mensagem' => 'Pagamento realizado  com sucesso',
				 	'token' => $gerarPagamento['token'],
				 	'data'=> $gerarPagamento['date_created']
				);
			} else if ($gerarPagamento['status'] == 'refused') {
				return array(
					'tipo' => 'danger',
				 	'mensagem' => 'Pagamento recusado!',
				 	'token' => $gerarPagamento['token'],
				 	'data'=> $gerarPagamento['date_created']
				);
			}
		}
	}

	

	function cadastrarContaBancaria($dados) {
		$dadosBancario = array(
			"agencia" => $dados['agencia'], 
		    "agencia_dv" => $dados['digitoAgencia'], 
		    'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME,
		    "bank_code" => $dados['codigoBanco'], 
		    "conta" => $dados['conta'], 
		    "conta_dv" => $dados['digitoConta'], 
		    "document_number" => $dados['CNPJ'], 
		    "legal_name" => $dados['nomeEmpresa'], 
		    "type" => filtrarTipoconta($dados['tipoConta'])
		);
		
		$inserirDadosBancarios = curl_post('bank_accounts', $dadosBancario);

		if (array_key_exists('errors',$inserirDadosBancarios)) {
			return array('tipo' => 'alert', 'mensagem' => $inserirDadosBancarios['errors'][0]['parameter_name']);
		} else {
			return array(
				'tipo' => 'success',
			 	'mensagem' => 'Cliente cadastrado com sucesso!',
			 	'idDadosBancario' => $inserirDadosBancarios['id'],
			 	'dataCriacao' => $inserirDadosBancarios['date_created'],
			);
		}
	}


	function cadastrarRecebedor($codigoBanco, $diaTransferencia) {
		$dadosRecebedor = array(
			"anticipatable_volume_percentage"=> "85", 
		    'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME,
		    "automatic_anticipation_enabled"=> "false", 
		    "bank_account_id"=> $codigoBanco, 
		    "transfer_day"=> $diaTransferencia, 
		    "transfer_enabled"=> "true", 
		    "transfer_interval"=> "monthly"
		);

		$cadastrarRecebedor = curl_post('recipients', $dadosRecebedor);

		if (array_key_exists('errors',$cadastrarRecebedor)) {
			return array('tipo' => 'alert', 'mensagem' => $cadastrarRecebedor['errors'][0]['parameter_name']);
		} else {
			return array(
				'tipo' => 'success',
			 	'mensagem' => 'Cliente cadastrado com sucesso!',
			 	'idRecebedor' => $cadastrarRecebedor['id'],
			 	'dataCriacao' => $cadastrarRecebedor['date_created'],
			 	'dataAtualizacao' => $cadastrarRecebedor['date_updated']
			);
		}
	}

	function atualizarRecebedor($idDadosBancario, $codigoRecebedor) {
		$dadosRecebedor = array(
			'anticipatable_volume_percentage' => '85', 
		    'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME,
		    'bank_account_id' => $idDadosBancario,
		    'recipient_id' => $codigoRecebedor
		);

		$atualizarRecebedor = curl_post('recipients', $dadosRecebedor);

		if (array_key_exists('errors',$atualizarRecebedor)) {
			return array('tipo' => 'alert', 'mensagem' => $atualizarRecebedor['errors'][0]['message']);
		} else {
			return array(
				'tipo' => 'success',
			 	'mensagem' => 'recebedor atualizado com sucesso!'
			);
		}
	}

	function realizarSaque($idRecebedor, $valor) {
		$dadosSaque = array(
			"amount" => $valor, 
    		'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME,
    		"recipient_id" => $idRecebedor
		);

		$realizarSaque = curl_post('transfers', $dadosSaque);

		if (array_key_exists('errors',$realizarSaque)) {
			return array('tipo' => 'alert', 'mensagem' => $realizarSaque['errors'][0]['message']);
		} else {
			return array(
				'tipo' => 'success',
			 	'mensagem' => 'transferencia agendada com sucesso!'
			);
		}
	}

	function verificarSaldo($idRecebedor) {
		$dadosRecebedor = array(
			"recipient_id" => $idRecebedor,
			'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME
		);

		$saldo = curl_get('balance',$dadosRecebedor);

		if (array_key_exists('errors',$saldo)) {
			return array('tipo' => 'alert', 'mensagem' => $saldo['errors'][0]['message']);
		} else {
			return array(
				'tipo' => 'success',
			 	'fundo' => $saldo['waiting_funds']['amount'],
			 	'transferencia' => $saldo['transferred']['amount'],
			);
		}
	}

	function estorno($idTransacao) {
		$dados = array(
			'api_key' => KEY_PAGAR_ME,
			'encryption_key' => CRIPTOGRAFIA_PAGAR_ME
		);

		$estorno = curl_post("transactions/$idTransacao/refund", $dados);

		if (array_key_exists('errors',$estorno)) {
			return array('tipo' => 'alert', 'mensagem' => $estorno['errors'][0]['message']);
		} else {
			return array(
				'tipo' => 'success',
			 	'mensagem' => "Estorno realizado com sucesso"
			);
		}	
	}

    function curl_post($url, $dados) {
    	$con = curl_init(END_POINT_PAGAR_ME."/".$url);
		curl_setopt($con, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json'));	
		curl_setopt($con, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($con, CURLOPT_POSTFIELDS, json_encode($dados));
		curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($con,CURLOPT_CONNECTTIMEOUT ,3);
  		curl_setopt($con,CURLOPT_TIMEOUT, 20);

  		$retorno = json_decode(curl_exec($con), true);
		return $retorno;
    }

    function curl_get($url, $dados) {
    	$con = curl_init(END_POINT_PAGAR_ME."/".$url);
		curl_setopt($con, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json'));	
		curl_setopt($con, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($con, CURLOPT_POSTFIELDS, json_encode($dados));
		curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($con,CURLOPT_CONNECTTIMEOUT ,3);
  		curl_setopt($con,CURLOPT_TIMEOUT, 20);

  		$retorno = json_decode(curl_exec($con), true);
		return $retorno;
    }

    function curl_put($url, $dados) {
    	$con = curl_init(END_POINT_PAGAR_ME."/".$url);
		curl_setopt($con, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json'));	
		curl_setopt($con, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($con, CURLOPT_POSTFIELDS, json_encode($dados));
		curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($con,CURLOPT_CONNECTTIMEOUT ,3);
  		curl_setopt($con,CURLOPT_TIMEOUT, 20);

  		$retorno = json_decode(curl_exec($con), true);
		return $retorno;
    }


    function filtrarTipoconta($tipoConta) {
    	switch ($tipoConta) {
    		case '1':
    			return 'conta_corrente';
    			break;
    		case '2':
    			return 'conta_poupanca';
    			break;
    		case '3':
    			return 'conta_corrente_conjunta';
    			break;
    		case '4':
    			return 'conta_poupanca_conjunta';
    			break;	
    	}
    }

    function filtrarTipocontaPorNome($tipoConta) {
    	switch ($tipoConta) {
    		case 'conta_corrente':
    			return '1';
    			break;
    		case 'conta_poupanca':
    			return '2';
    			break;
    		case 'conta_corrente_conjunta':
    			return '3';
    			break;
    		case 'conta_poupanca_conjunta':
    			return '4';
    			break;	
    	}
    }

