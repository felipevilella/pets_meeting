  <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 700px; background-image: url(<?= $perfilPet["fotoPrincipal"]?>); background-size: cover; background-position: center top;">
    <span class="mask bg-gradient-warning opacity-6"></span>
    <div class="container-fluid d-flex align-items-center">
      <div class="row">
        <div class="col-lg-7 col-md-10">
          <h3 class="display-2 text-white"><?=$perfilPet["mensagem"];?> <?=$perfilPet["nome"];?>!</h3>
          <h4 class="text-white"> <font class= "textoCapa"></h4>
           <br> <a href="<?= $url?>"><button class="btn btn-primary">Acessar</button></a>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
     var options = {
      strings: ['Veja minhas consultas.', "Entre em contato com o meu dono.", "Veja minhas vacinas."],
      typeSpeed: 100,
      loop:true
    }

    var typed = new Typed(".textoCapa", options);
  </script>