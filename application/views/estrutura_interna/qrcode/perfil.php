<div class="wrapper">
    <div class="profile-background"> 
        <div class="filter-black"></div>  
    </div>
    <div class="profile-content section-nude">
        <div class="container">
            <div class="row owner">
                <div class="col-md-2 col-md-offset-5 col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3 text-center">
                    <div class="avatar">
                        <img  src="<?= $perfilPet['fotoPrincipal']?>" class='img-circle img-no-padding img-responsive'>
                    </div>
                    <div class="name">
                        <h4><?= $perfilPet["nome"]?><br/><small><?= $perfilPet["nomeraca"]?></small></h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                  <btn class="btn btn-danger"><i class="fa fa-mobile" aria-hidden="true"></i> Ligar para o dono</btn>
              </div>
          </div>
          <div class="profile-tabs">
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#Responsavel" data-toggle="tab">Dono</a></li>
                        <li><a href="#Consultas" data-toggle="tab">Consultas</a></li>
                        <li><a href="#Vacinas" data-toggle="tab">Vacinas</a></li>
                    </ul>
                </div>
            </div>
            <div id="my-tab-content" class="tab-content">
                <div class="tab-pane active" id="Responsavel">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <ul class="list-unstyled follows">
                                <li>
                                    <div class="row">
                                        <div class="col-md-2 col-md-offset-0 col-xs-3 col-xs-offset-2">
                                            <img  src="<?= $usuario['fotoPrincipal'];?>"  class='img-circle img-no-padding img-responsive'>
                                        </div>
                                        <div class="col-md-7 col-xs-4">
                                            <h6><?=$usuario["nome"]?><br /><small><?= $usuario["nomeCidade"];?></small></h6>
                                        </div>
                                        <div class="col-md-3 col-xs-2">
                                            <div class="unfollow" rel="tooltip" title="Unfollow">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <hr/>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane text-center" id="Consultas">
                    <ul class="list-unstyled follows">
                        <ul class="list-unstyled follows">
                            <?php foreach ($consultas as $consulta): ?>
                                <li>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <h6><?=$consulta["nomeServico"];?>
                                            <?php if ($consulta["situacao"] == 1): ?>
                                                <i class="fa fa-check-square" aria-hidden="true"></i>
                                                <?php else: ?>
                                                 <i class="fa fa-square" aria-hidden="true"></i>
                                             <?php endif ?>
                                             <br>
                                             <small><?=$consulta["hora"];?> - <?=$consulta["data"];?></small>
                                         </h6>

                                     </div>
                                 </div>
                             </li><hr/>
                         <?php endforeach ?>
                     </ul>
                 </div>
                 <div class="tab-pane text-center" id="Vacinas">
                  <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <ul class="list-unstyled follows">
                            <?php foreach ($historicoVacinas as $historicoVacina): ?>
                                <li>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <h6><?=$historicoVacina["nomeVacina"];?>
                                            <?php if ($historicoVacina["status"] == 2): ?>
                                                <i class="fa fa-check-square" aria-hidden="true"></i>
                                                <?php else: ?>
                                                 <i class="fa fa-square" aria-hidden="true"></i>
                                             <?php endif ?>
                                             <br>
                                             <small><?=$historicoVacina["hora"];?> - <?=$historicoVacina["data"];?></small>
                                         </h6>
                                     </div>
                                 </div>
                             </li><hr/>
                         <?php endforeach ?>    
                     </ul>
                 </div>
             </div>
         </div>
     </div>
 </div>        
</div>
</div>
</div> 
<footer class="footer-demo section-nude">
    <div class="container">
        <div class="copyright pull-right">
            &copy; 2019, Pets Meeting
        </div>
    </div>
</footer>