	<!-- start header Area -->
	<header id="header">
		<div class="container main-menu">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="<?=base_url('acessoInicial')?>">
						<img src="<?= base_url('assets/personalizado/imagem/logo.png');?>" width="45%">
					</a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li><a href="<?=base_url('cadastrar_estabelecimento')?>">CADASTRE SEU ESTABELECIMENTO</a></li>
						<!-- <li class="menu-active"><a href= "<?=base_url('entrar')?>"> ENTRAR</a></li> -->
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<section class="home-banner-area">
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-between">
				<div class="home-banner-content col-lg-6 col-md-6">
					<h1>
						Tudo  que <br> seu pet precisa<br> em um só lugar
					</h1>
					<p>Simples, fácil e prático. Já imaginou marcar consultas e ter o controle da saúde do seu animal de estimação na palma da sua mão? Então. Nos damos está oportunidade para você!</p>
					<div class="download-button d-flex flex-row justify-content-start">
					<!-- 	<div class="buttons flex-row d-flex">
							<i class="fa fa-apple" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Baixe o aplicativo</span> <br>
										on App Store
									</p>
								</a>
							</div>
						</div> -->
						<div class="buttons dark flex-row d-flex">
							<i class="fa fa-android" aria-hidden="true"></i>
							<div class="desc">
								<a href="https://play.google.com/store/apps/details?id=com.pets.pets_meeting_app" target="_blank">
									<p>
										<span>Baixe o aplicativo</span> <br>
										na App Store
									</p>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="banner-img col-lg-4 col-md-6">
					<img class="img-fluid" src="<?= base_url('assets/bootstrap/img/banner-img.png')?>" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- Start Pricing Area -->
	<section>
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<h2>Planos de preços </h2>
						</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="single-price">
						<div class="top-sec d-flex justify-content-between">
							<div class="top-left">
								<h4>Pets Tutor</h4>
								<p>para  <br>Donos de animais</p>
							</div>
							<div class="top-right">
								<h1>Gratis</h1>
							</div>
						</div>
						<div class="end-sec">
							<ul>
								<li>Acesso as consultas dos estabelecimentos cadastrados</li>
								<li>Agendamento de consultas</li>
								<li>Cadastro dos pets</li>
								<li>Acesso ao historico de consultas</li>
								<li>cartão de vacina virtual</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="single-price">
						<div class="top-sec d-flex justify-content-between">
							<div class="top-left">
								<h4>Pets Business</h4>
								<p>para  <br>Estabelecimento</p>
							</div>
							<div class="top-right">
								<h1>10%</h1>
							</div>
						</div>
						<div class="end-sec">
							<ul>
								<li>+ Taxa de 10% sobre o valor dos pedidos</li>
								<li>Cadastro de serviços</li>
								<li>Controle de atendimentos diarios</li>
								<li>Historico de consulta</li>
								<li>Acesso ao cartão de vacina virtual do clientes</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6"></div>
			</div>
		</div>
	</section>
