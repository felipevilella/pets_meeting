</div>
<div class="modal fade alterarfoto_usuario " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<form action="<?php echo base_url('alterarFotoUsuario');?>" method="post" enctype="multipart/form-data">
					<?php echo "<input type ='hidden' name='idusuario' id='idusuarioativo' value='".$idusuario."'>"; ?>
					<div class="modal-body">
						<div class="ModalContainer">
							<div class="col-md-12">
								<div class="fileinput fileinput-new text-center" data-provides="fileinput">
									<div class="fileinput-new thumbnail img-raised">
										<img src="<?php echo base_url('assets/personalizado/imagem/avatar.jpg');?>" class="col-md-12"  alt="...">
									</div>
									<div class="imagem_upload">
										<div class="fileinput-preview fileinput-exists thumbnail img-circle "></div>
									</div>
									<div>
										<span class="btn btn-raised btn-round btn-rose btn-file" class="col-md-12">
											<span class="fileinput-new">Selecione uma imagem</span>
											<span class="fileinput-exists">Alterar foto</span>
											<input type="file" name="fotousuarioperfil" />
										</span>
										<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
											<i class="fa fa-times"></i> remover</a>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
							<button type="submit" class="btn btn-warning">Salvar </button>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	<div class="modal fade modalFooter" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="tituloModal"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="modalFooterConteudo">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
					<button type="button" class="btn btn-warning botaoModal"  id="">Salvar</button>
				</div>
			</div>
		</div>
	</div>


<div class="modal fade informacaopesquisaPetMensagem" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<br>
							<div id="nomePet1" class="col-md-12"></div>
							<div class="row">
								<div class="col-md-6">
									<div id="Nomeraca"></div>
								</div>
								<div class="col-md-6">
									<div id="anoNascimento"></div>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
								</ol>
								<div class="carousel-inner">
									
								</div>
								<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</div>

						<div class="col-md-12">
							<div id = "CaracteristicasPets">
							</div>
						</div>
						<br><br><br>
						<div class="col-md-12">
							<button type="button" class="btn btn-danger col-md-12" data-dismiss="modal">Voltar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	var base_url = "<?php echo base_url();?>";
</script>

<script src="<?php echo base_url('assets/bootstrap/js/core/jquery.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/bootstrap/js/core/popper.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/bootstrap/js/core/bootstrap-material-design.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/bootstrap/js/plugins/perfect-scrollbar.jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/plugins/chartist.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/plugins/bootstrap-notify.js');?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/material-dashboard.min.js?v=2.1.0');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/sistema.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/chat.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/tabs.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/jasny-bootstrap.js');?>"></script>

<?php if ($scriptMaps): ?>
	<?php foreach ($scriptMaps as $script): ?>
		<script  src="<?php echo $script?>" async defer></script>
	<?php endforeach ?>
<?php endif ?>

<?php foreach ($scripts as $script): ?>
	<script src="<?php echo base_url($script);?>"></script>
<?php endforeach ?>


<script>
	$(document).ready(function() { md.initDashboardPageCharts();});
</script>
</body>
</html>

