<header id="header">
		<div class="container main-menu">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="<?=base_url('acessoInicial')?>">
						<img src="<?= base_url('assets/personalizado/imagem/logo.png');?>" width="45%">
					</a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<!-- <li class="menu-active"><a href= "<?= base_url('entrar');?>">ENTRAR</a></li> -->
						<li class="menu-active"><a href= "<?= base_url('acessoInicial');?>">Inicio</a></li>
					</ul>
				</nav>
			</div>
		</div>
</header>
<section class="banner-area">
	<div class="container">
		<div class="row banner-content">
			<div class="col-lg-12 d-flex align-items-center justify-content-between">
				<div class="left-part">
					<h1>
						O seu estabelecimento cresce <br> com a Pets Meeting
					</h1>
					<p>
						Quem sempre pensa em dar o melhor atendimento para o seu cliente, tá na Pets Meeting.
					</p>
				</div>
				<div class="right-part">

				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<div class="text-center home-about-right2">
	<h5 class="text-black">Cadastre seu Estabelecimento</h5><br>
</div>
<div class="container">
<div class="row">
	<div class="col-12">
		<div class="col-12">
			<div class="validation errror" id="erroNome"></div>
	    	<input name="name" placeholder="Qual o nome do seu estabelecimento?" name="nome"
				id="nome" class="common-input mb-20 form-control col-12" type="text">
		</div>
		<div class="col-12">
			<div class="validation errror" id="erroEmailCadastro"></div>
	    	<input name="name" placeholder="Qual o e-mail do seu estabelecimento?" name="emailCadastro"
				id="emailCadastro" class="common-input mb-20 form-control col-12" type="text">
			 
		</div>
		<div class="col-12">
			<div class="validation errror" id = "erroEstado"></div>
	    	<select class="mb-20 form-control" id="estadoBuscar">
	          <option value="">Selecione um estado</option>
	            <?php foreach ($estados as $estado): ?>
	              <option value= '<?= $estado["idestado"]?>'><?=$estado["nome"]?></option>
	            <?php endforeach ?>
	        </select>
				
		</div>
		<div class="col-md-12">
			<div class="validation errror" id = "erroCidade"></div>
			<div class="form-group mb-20">
	              <select id="Cidade" class="form-control" disabled>
	                <option value="">Selecione uma cidade</option>
	              </select>
	          
	        </div>
		</div>
		<div class="col-md-12">
			<div class="validation errror" id="erroSenha"></div>
			<input name="name" name="password" type='password' id="password" placeholder="Digite sua senha" class="common-input mb-20 form-control col-12">	 
		</div>
		<div class="col-md-12">
			<div class="validation errror" id="retornosenha"></div>
			<input name="name"  name="senha" type='password' id="password1" placeholder="Repita sua senha" class="common-input mb-20 form-control col-12">
		</div>
		<div class="col-12">
			 <button type="button" class="btn btn-success col-12" id="Cadastrar">Cadastrar</button>
		</div>
		<input id="comercial" class="form-control hidden" value="1">
	</div>
</div>
</div>