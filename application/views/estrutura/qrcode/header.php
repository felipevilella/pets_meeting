<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="../assets/paper_img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>Pets meeting</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    
    <link href="<?php echo base_url('assets/personalizado/qrcode/css/bootstrap.css');?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/personalizado/qrcode/css/ct-paper.css');?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/personalizado/qrcode/css/style.css');?>" rel="stylesheet" /> 
    <link href="<?php echo base_url('assets/personalizado/qrcode/css/style1.css');?>" rel="stylesheet" /> 
        
    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/bootstrap_dashbord/js/plugins/@fortawesome/fontawesome-free/css/all.min.css');?>" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
