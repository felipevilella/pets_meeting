</body>

<script src="<?php echo base_url('assets/personalizado/qrcode/js/jquery-1.10.2.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/personalizado/qrcode/js/jquery-ui-1.10.4.custom.min.js');?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/personalizado/qrcode/js/bootstrap.js');?>" type="text/javascript"></script>

<!--  Plugins -->
<script src="<?php echo base_url('assets/personalizado/qrcode/js/ct-paper-checkbox.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/qrcode/js/ct-paper-radio.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/qrcode/js/bootstrap-select.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/qrcode/js/bootstrap-datepicker.js');?>"></script>

<script src="<?php echo base_url('assets/personalizado/qrcode/js/ct-paper.js');?>"></script>

<?php if ($scripts): ?>
	<?php foreach ($scripts as $script): ?>
		<script type="text/javascript" src="<?php echo $script;?>"></script>
	<?php endforeach ?>
<?php endif ?>


