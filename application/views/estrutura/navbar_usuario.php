<div class="wrapper ">
  <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <div class="logo">
    <?php if ($informacaoUsuario["comercial"] == 0): ?>
     <a class="navbar-brand" href="<?php echo base_url("pesquisa");?>"><center><img src="<?php echo base_url("assets/personalizado/imagem/logo.png");?>" width="50%"></center></a>
    <?php else: ?>
      <a class="navbar-brand" href="<?php echo base_url("financeiro");?>"><center><img src="<?php echo base_url("assets/personalizado/imagem/logo.png");?>" width="50%"></center></a>
    <?php endif ?>
   </div>
   <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item  ">
        <a class="nav-link" href="#">
          <div class="imagemPerfil">
           <center>
            <?php
            if (empty($informacaoUsuario["fotoPrincipal"])) {
              echo "<img class='img' src=".base_url('assets/personalizado/imagem/avatar.jpg')."  id='alterarfotousuario'>";
            } else {
              echo "<img class='img' src='".base_url("assets/personalizado/foto_usuario/".$informacaoUsuario["fotoPrincipal"])."' id='alterarfotousuario'>";
            }
            ?>    
          </center>
        </div>
        <center><p> <h5> <?php echo($nome);?></h5></p></center> <hr>
      </a>
    </li>

    <li class="nav-item" id="listaMensagem">
      <?php if ($informacaoUsuario["comercial"] == 0): ?>
        <a class="nav-link" href="#" id ="verconversa">
          <i class="material-icons">mail_outline</i>
          <p>Mensagem</p>
        </a>
        <?php else: ?>
          <a class="nav-link" href="#" id ="sobre">
            <i class="material-icons">chrome_reader_mode</i>
            <p>Sobre</p>
          </a>
        <?php endif ?>
        
      </li>
      <li class="nav-item" id="nav-pets">
        <?php if ($informacaoUsuario["comercial"] == 0): ?>
          <a class="nav-link" href="<?php echo base_url("selecionarperfilpets");?>" >
            <i class="material-icons" id ='btn-pets'>pets</i>
            <p>Meus pets</p>
          </a>
          <?php else: ?>
            <a class="nav-link"  id="btn-servicos"  href="<?php echo base_url("servicos");?>">
              <i class="material-icons">store</i>
              <p>Meus serviços</p>
            </a>
          <?php endif ?>
          
        </li>
        <li class="nav-item active" id="nav-encontrar">
          <?php if ($informacaoUsuario["comercial"] == 0): ?>
            <a class="nav-link" href="<?php echo base_url("pesquisa");?>" id="btn-encontrar">
              <i class="material-icons">store</i>
              <p>Pet shop</p>
            </a>
            <?php else: ?>
              <a class="nav-link" id="nav-financeiro" href="<?php echo base_url("financeiro");?>" >
                <i class="material-icons" id ='btn-pets'>attach_money</i>
                <p>Gestão</p>
              </a> 
            <?php endif ?>
            
          </li>
          <li class="nav-item" id="nav-clinicas">
            <?php if ($informacaoUsuario["comercial"] == 0): ?>
              <a class="nav-link" href="#">
                <i class="material-icons">notification_important</i>
                <p>Alertas</p>
              </a>
              <?php else: ?>
               <a class="nav-link" href="#">
                <i class="material-icons">calendar_today</i>
                <p>Atendimento diarios</p>
              </a>
            <?php endif ?>
            
          </li>
          <li class="nav-item " id = "nav-Walker">
            <a class="nav-link" href="">
              <i class="material-icons">settings</i>
              <p>Configuração</p>
            </a>
          </li>
          <li class="nav-item active-pro ">
            <a class="nav-link" href="<?php echo base_url('sair');?>">
              <i class="material-icons">exit_to_app</i>
              <p>Sair</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
        <div class="container-fluid">
          <div class="navbar-wrapper">
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarToggler">

          </ul>
        </div>
      </div>
    </nav>

