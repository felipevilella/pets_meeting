<doctype html>
  <html lang="pt-br">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Pets meeting  </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons'?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="<?php echo base_url('assets/bootstrap/css/material-dashboard.css?v=2.1.0');?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/personalizado/css/style.css');?>" rel="stylesheet" />
    <link rel="stylesheet"  href="<?php echo base_url('assets/personalizado/css/formatacao.css');?>" type="text/css">
    <link href="<?php echo base_url('assets/personalizado/css/style.css');?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/personalizado/css/jasny-bootstrap.css');?>" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89048428-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-89048428-2');
  </script>
  

</head>
