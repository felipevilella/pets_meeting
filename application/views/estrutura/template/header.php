<!DOCTYPE html>
<html lang="pt-br" class="no-js">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="img/fav.png">
	<meta name="author" content="codepixer">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta charset="UTF-8">
	<title>Pets Meeting</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:400,600|Roboto:400,400i,500" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/linearicons.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/magnific-popup.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/nice-select.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/hexagons.min.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/owl.carousel.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/main.css')?>">
	
</head>

<body>
