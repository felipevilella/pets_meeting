<footer class="footer-area section-gap">
		<div class="container">

			<div class="footer-bottom row align-items-center">
				<p class="footer-text m-4 col-md-12" align="center"><font size="2px">&copy;<script>document.write(new Date().getFullYear());</script> Todos os diretos reservados a Pets Meeting <!---& Colorlib--></a></font></p>
			</div>
		</div>
	</footer>
	<script type="text/javascript">
	  var base_url = "<?php echo base_url();?>";
	</script>
	<script src="<?= base_url('assets/bootstrap/js/vendor/jquery-2.2.4.min.js')?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	 crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/bootstrap/js/tilt.jquery.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/vendor/bootstrap.min.js')?>"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="<?= base_url('assets/bootstrap/js/easing.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/hoverIntent.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/superfish.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/jquery.ajaxchimp.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/jquery.magnific-popup.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/hexagons.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/jquery.nice-select.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/waypoints.min.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/mail-script.js')?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/main.js')?>"></script>
	
	<script src="<?php echo base_url('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js');?>"></script>
	<script src="<?php echo base_url('assets/personalizado/js/filepond.min.js');?>"></script>
	<script src="<?= base_url('assets/personalizado/js/sistema.js');?>"></script>
</body>
<script>
  window.TrackJS &&
  TrackJS.install({
    token: "ee6fab19c5a04ac1a32a645abde4613a",
    application: "argon-dashboard-free"
  });


/* $(document).ready(function() {
    var urlPagina =  window.location.href;
  
    if (urlPagina.match("/pesquisa")) {
      $("#loading-animais").removeClass("hidden");
      setTimeout(function() {
          $("#loading-animais").fadeOut("slow");
          $("#loading-animais").addClass("hidden");
      },2000);
    } 
  });*/

  function cadastro_success(){
   Swal(
    'Cadastrado!',
    'Conta cadastrada com sucesso, em breve entraremos em contato!',
    'success'
    )
 }

 function email_success(){
   Swal(
    'Enviado!',
    'Verifique sua caixa de entrada!',
    'success'
    )
 }
 function senha_success(){
   Swal(
    'Senha alterada!',
    'acesse sua conta com a sua nova senha!',
    'success'
    )
 }

 function senha_error() {
   Swal({
    type: 'error',
    title: 'Oops...',
    text: 'Senhas divergentes!',
  })
 }
 function email_error() {
   Swal({
    type: 'error',
    title: 'Oops...',
    text: 'E-mail informado está incorreto ou não existe!',
  })
 }

  </script>
</html>