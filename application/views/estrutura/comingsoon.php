<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Pets Meeting</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
  
  <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.9"></script>

  <style>
    body,h1 {font-family: "Raleway", sans-serif}
    body, html {height: 100%}
    .bgimg {
      background-image: url('assets/personalizado/imagem/dog_fundo.jpg');
      min-height: 100%;
      background-position: center;
      background-size: cover;
    }
  </style>

</head>
<body>

  <div class="bgimg w3-display-container w3-animate-opacity w3-text-white">
    <div class="w3-display-topleft w3-padding-large w3-xlarge">
      <img src="assets/personalizado/imagem/logo.png" style="width: 50%">
    </div>
    <div class="w3-display-middle">
      <h1 class="w3-jumbo w3-animate-top" > COMING SOON</h1>
      <hr class="w3-border-grey" style="margin:auto;width:40%">
      <p class="w3-large w3-center" > <b>Novidades:</b>  <font id= "textoAmostra">Estamos construindo o site</font></p>
    </div>
    <div class="w3-display-bottomleft w3-padding-large">
      &copy; PetsMeeting 2019
    </div>
  </div>

  <script type="text/javascript">
  // Inserindo caracter para o efeito da digitação do texto
  var options = {
    strings: ["Estamos construindo o site", "Pet shop", "Cartão virtual"],
    typeSpeed: 100,
    loop:true
  }

var typed = new Typed("#textoAmostra", options);
  </script>

</body>
</html>