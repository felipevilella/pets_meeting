	<div class="header bg-gradient-white pt-md-8">
		<div class="container-fluid">
			<div class="header-body">
					<h3 class="ct-title" id="content"><font color>Serviços</font></h3>
					 <div aria-labelledby="dropdownMenuButton">
						<button type="button" class="btn btn-warning btn-sm" data-toggle="dropdown">
							<?= $endereco['rua']; ?>, <?= $endereco['numero']; ?> <i class="ni ni-bold-down"></i>
						</button>
						<ul class="dropdown-menu">
							<?php foreach ($enderecos as $endereco): ?>
								<li> 
									<a class="dropdown-item selecionarEndereco" href="#" data_endereco='<?= $endereco['idEndereco']; ?>'>
										<?= $endereco['rua']; ?>, <?= $endereco['numero']; ?> 
									</a>
								</li>
							<?php endforeach ?>
					    </ul>
					</div>
						<hr>
			</div>
		</div>
	</div>
	<div class="col-md-12"> 
		<div class="form-group">
			<select class="form-control  btn-encontrar-petshop" id="servico">
				<option value = ""> Selecione um serviço </option>
				<?php foreach ($servicos as $servico): ?>
					<option value = "<?= $servico['idtipoServico'];?>" > <?= $servico['nome']; ?></option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
	
	<div class="listarPetshop">
		<?=$petshops?>
	</div>