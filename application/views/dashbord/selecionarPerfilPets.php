<div class="header bg-gradient-white pb-3 pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			<div class="">
				<h3 class="ct-title" id="content"><font color>Meus pets</font></h3><button type="button" class="btn btn-warning btn-sm" id='btn-cadastroPets'>
					+ Cadastre seu peludinho aqui.
				</button>
			</div>
		</div>
	</div>
</div>
<div class="collapse div-cadastroPets">
	<div class="col-md-6"> 
		<label >Nome</label>
		<div class="input-group input-group-merge input-group-alternative">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="ni ni-circle-08"></i></span>
			</div>
			<input class="form-control" name="nome" id="nome" placeholder="nome" type="text">
		</div>
		<span id="erroNome"  class="errror"></span>
	</div>

	<div class="col-md-6"><br> 
		<label >Tipo</label>
		<div class="form-group">
			<select class="form-control" id="tipoAnimal">
				<option value = "" > Selecione uma opção </option>
				<?php foreach ($tipoAnimais as $tipoAnimal): ?>
					<option value = "<?=$tipoAnimal["idtipoAnimal"]?>" > <?=$tipoAnimal["nome"]?></option>
				<?php endforeach ?>
			</select>
			<span id="erroTipo" class="errror"></span>
		</div>
	</div>
	<div class="col-md-6"> 
		<label >Raça</label>
		<div class="form-group">
			<select class="form-control" id="raca">
				<option value = "" > Selecione uma opção </option>
				<?php foreach ($racaPets as $raca): ?>
					<option value = "<?=$raca["idracas"]?>" > <?=$raca["nome"]?></option>
				<?php endforeach ?>
			</select>
			<span id="erroRaca" class="errror"></span>
		</div>
	</div>
	<div class="col-md-6"> 
		<label >Ano de nascimento</label>
		<div class="input-group input-group-merge input-group-alternative">
			<div class="input-group-prepend">
				<span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
			</div>
			<input class="form-control" type="month" name="data" id="data">
		</div>
		<span id="erroAno" class="errror"></span>
	</div>
	<div class="col-md-6"> 
		<label >Sexo</label><br>
		<div class="custom-control custom-radio custom-control-inline">
			<input type="radio" id="customRadioInline1" name="customRadioInline1" value="m" class="custom-control-input">
			<label class="custom-control-label" for="customRadioInline1">Menino</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input type="radio" id="customRadioInline2" name="customRadioInline1" value="f" class="custom-control-input">
			<label class="custom-control-label" for="customRadioInline2">Menina</label>
		</div><br>
		<span id="erroSexo" class="errror"></span>
	</div>
	<br>
	<div class="col-12">
		<div class="row">
			<div class="col-6">
				<button type="button" class="btn btn-sm btn-white col-12" id="btn-listarPets">voltar</button>
			</div>
			<div class="col-6">
				<button type="button" class="btn btn-sm btn-danger col-12" id="cadastrarPets">Cadastrar</button>
			</div>
		</div>
	</div>
	<br>
</div>

<div class="collapse show div-listarPets">
	<?php if ($dadosPets): ?>
		<?php foreach ($dadosPets as $pets) : ?>
		<a href="<?php echo base_url('perfilpets/'.$pets["idanimais"]);?>">
			<div class="card">
				<div class="card-body">
					<div class="row align-items-center">
						<div class="col-auto">
							<?php if(!$pets["fotoPrincipal"]): ?>
								<img src="<?php echo base_url('assets/personalizado/imagem/addFoto.jpg')?>" class="imagem-perfil-selecionar-perfil">
								<?php else: ?>
									<img src="<?php echo base_url("assets/personalizado/fotos_pets/".$pets["fotoPrincipal"])?>" class="imagem-perfil-selecionar-perfil">
								<?php endif?>
							</div>
							<div class="col ml--2">
								<h4 class="mb-1">
									<a href="<?php echo base_url('perfilpets/'.$pets["idanimais"]);?>">
										<b><font color="black"><?=$pets["nome"]?></font></b>
									</a>
								</h4>
								<h5><a href="<?php echo base_url('perfilpets/'.$pets["idanimais"]);?>">
										<font color="black">
											<?=$pets["nomeraca"]?>		
										</font>
									</a>
								</h5>
							</div>
						</div>
					</div>
				</div></a>
			<?php endforeach ?>
		<?php else: ?>
			<hr>
			<p align="center"><img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_animais.jpg')?>" class='imagem-fundo'></p>
			<p align="center"> Parece que está faltando alguem!<br> Cadastre seu peludinho! </p>
	<?php endif ?>
</div>



