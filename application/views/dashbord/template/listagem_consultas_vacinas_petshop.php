<?php if ($vacinas): ?>  
  <?php foreach ($vacinas as $vacina): ?>
    <div class="<?= $dispositivos['plataforma'] != 'Android' && $dispositivos['plataforma'] != 'iOS'? 'card':''?>">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-auto">
            <a href="#" class="avatar rounded-circle">
             <img src="<?php echo base_url('assets/personalizado/imagem/artes/veterinario.jpg');?>" 
             class='imagem-tipo-servico'>
            </a>
          </div>
          <div class="col ml--2">
            <h4 class="mb-0">
               <?= $vacina["nomeVacina"]?>
            </h4>
            <small><?=$vacina["hora"];?> - <?=$vacina["data"]?></small><br>
            <small> Agendado por  <?= $vacina["nomePetshop"]?></small>
         </div>
         <div class="col-auto">

          <?php if ($vacina["status"] == 1 && $codigoPetshop == $vacina['fk_idPetshop']): ?>
            <a href="#" class="confirmar-atendimento" data-vacina="<?=$vacina['idVacina'];?>">
              <i class="ni ni-check-bold text-green"></i>
            </a>
            <a href="#" class="cancelar-vacina" data-vacina="<?=$vacina['idVacina'];?>">
              <i class="ni ni-fat-remove text-red"></i>
            </a>
          <?php endif ?>

          <?php if ($vacina["status"] == 3): ?>
           <i class="ni ni-fat-remove"></i>
         <?php endif ?>

         <?php if ($vacina["status"] == 2): ?>
           <i class="ni ni-check-bold"></i>
         <?php endif ?>
       </div>
     </div>
   </div>
 </div>
<?php endforeach ?>
<?php else: ?>
   <p align="center">
      <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_vacinas.jpg')?>" class='<?= $dispositivos['plataforma'] != 'Android' && $dispositivos['plataforma'] != 'iOS'? 'imagem-fundo-sem-atendimento-estabelecimento':'imagem-fundo-sem-consulta'?>'>
    </p>
    <p align="center"> Que pena! ainda não possui vacinas. </p>
<?php endif ?>
