<?php if ($consultas): ?>
  <?php foreach ($consultas as $consulta): ?>
    <div class="card col-12">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-5">
             <h2><center><b><?=$consulta["dataMes"]?></b></center></h2>
             <h4><center><?=$consulta["diaSemana"]?></center></h4>
          </div>
          <div class="col ml--2">
            <h4 class="mb-0">
             <?= $consulta["nomeServico"]?>
           </h4>
           <p class="text-sm text-muted mb-0"><?=$consulta["nomePetshop"];?></p>
           <small><?=$consulta["hora"];?></small>
         </div>
          <?php if ($consulta["situacao"] == 0): ?>
            <div class="col-12"><br>
              <button type="button" class="btn-sm btn-white btn-cancelar col-12"
               data-consulta="<?=$consulta['idReservaServico'];?>" data-codigoPetshop = "<?=$consulta['idReservaServico'];?>">
             Cancelar reserva</button>
          <?php endif ?>

          <?php if (($consulta["situacao"] == 0 && (strtotime($consulta["data"]) == strtotime($consulta["dataAtual"]))) && (
            $consulta["hora"] > date("H:i"))): ?>
          <div class="col-12"><br>
              <button type="button" class="btn-sm btn-white btn-cancelar col-12" disabled>Cancelar reserva</button>
          <?php endif ?>

          <?php if ($consulta["situacao"] == 1): ?>
            <div class="col-1">
             <i class="ni ni-check-bold text-green"></i>
          <?php endif ?>
          <?php if ($consulta["situacao"] == 3): ?>
            <div class="col-1">
             <i class="ni ni-fat-remove text-red"></i>
          <?php endif ?>

          <?php if ($consulta["situacao"] == 2): ?>
            <div class="col-12"><br>
            <button type="button" class="btn-sm btn-white btn-warning col-12" disabled> Pendente </button>
          <?php endif ?>
          </div>
        </div>
      </div>
    </div>
<?php endforeach ?>
  <?php else: ?>
    <p align="center">
      <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_consultas.jpg')?>" class='imagem-fundo-sem-consulta'>
    </p>
    <p align="center"> Parece que  <?= $dadosPets["nome"];?> está sem consulta.<br> Clique em serviços para agendar! </p>   
<?php endif ?>

