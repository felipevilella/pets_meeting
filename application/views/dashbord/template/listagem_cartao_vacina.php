<?php if ($cartoesVacina): ?>
  <?php foreach ($cartoesVacina as $cartaoVacina): ?>
    <div class="<?= $dispositivos['plataforma'] != 'Android' && $dispositivos['plataforma'] != 'iOS'? 'col-md-4' :''?> ">
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col-auto">
              <a href="#" class="avatar rounded-circle">
                <img alt="Image placeholder" src="<?= $cartaoVacina["fotoAnimal"];?>" class='imagem-perfil-selecionar-perfil-estabelecimento'>
              </a>
            </div>
            <div class="col ml--2">
              <h4 class="mb-0">
                <a href="#!"><?= $cartaoVacina["nomeAnimal"]?></a> 
                <?php if ($cartaoVacina["situacao"] == 1): ?>  
                  <label class="fa-blink">Novo</label>
                <?php endif ?>
              </h4>
              <p class="text-sm text-muted mb-0" align="justify">Responsavel <?= $cartaoVacina["nomeUsuario"]?></p>
            </div>
            <div class="col-auto">
              <a href="<?= $cartaoVacina['url']?>">
                <button type="button" class="btn btn-sm btn-warning">
                  Abrir
                </button>
              </a>
            </div>
          </div>
        </div>
      </div><br>
    </div>
  <?php endforeach ?>
  <?php else: ?>
    <div class="card">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-12">
            <p align="center"><img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_vacinas.jpg')?>" class="<?= $dispositivos['plataforma'] != 'Android' && $dispositivos['plataforma'] != 'iOS'? 'imagem-fundo-sem-cartao': 'imagem-fundo-sem-consulta'?>"></p>
          </p>
        </div>
        <div class="col-12">
          <p align="center">Cliente não encontrando ou inexistente</p>
        </div>
      </div>
    </div>
  </div>

<?php endif ?>
