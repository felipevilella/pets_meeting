<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    Pets Meeting
  </title>
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/bootstrap_dashbord/js/plugins/nucleo/css/nucleo.css');?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/bootstrap_dashbord/js/plugins/@fortawesome/fontawesome-free/css/all.min.css');?>" rel="stylesheet" />

   <?php if ((isset($informacaoUsuario['comercial']) && $informacaoUsuario["comercial"] == 1) && ($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS")): ?>
      <link href="<?php echo base_url('assets/bootstrap_dashbord/css/argon-dashboard-personalizado.min.css');?>" rel="stylesheet" />
    <?php else: ?>
      <link href="<?php echo base_url('assets/bootstrap_dashbord/css/argon-dashboard.min.css?v=1.1.0');?>" rel="stylesheet" />
  <?php endif?>

  <link href="<?php echo base_url('assets/bootstrap_dashbord/css/loading-btn.css');?>" rel="stylesheet"/>
  <link href="<?php echo base_url('assets/personalizado/css/style.min.css');?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/personalizado/css/jasny-bootstrap.css');?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/personalizado/css/filepond.min.css');?>" rel="stylesheet" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89048428-2"></script>
  <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.9"></script>
  <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
  <script type="text/javascript"> var base_url = "<?php echo base_url();?>";</script>

  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-89048428-2');

    var pusher = new Pusher('8e4bd12b694e6a7ce91e', {
      cluster: 'us2',
      forceTLS: true
    });

    var nome = "pets-meeting-"+"<?=$idusuario?>";
    var channel = pusher.subscribe(nome);

    channel.bind('atualizar-foto-perfil', function(data) {
      var url = window.location.href;

      if(url.match('perfilpets')) {
         $.get(base_url+"carregarFotoPets", {}, function(data) {
          if (data.tipo == "success") {
            $(".imagem-perfil-pets-perfil").attr("src", data.foto);
            console.log(data);
          }
        },'JSON');
      } else if(url.match('perfil')) {
          $.get(base_url+"carregarFotoPerfil", {}, function(data) {
            if (data.tipo == "success") {
              $(".imagem-perfil").attr("src", data.foto);
            }
          },'JSON');
      }
    });

  </script>
  
</head>

<body class="">