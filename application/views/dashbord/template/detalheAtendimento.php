<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <b>Situação: </b><br>  <?=$atendimento['status']?> 
            </div>
            <div class="col-md-3">
                <b>Horario: </b><br>  <?=$atendimento['hora']?> 
            </div>
            <div class="col-md-3">
               <b>Data: </b>  <?=$atendimento['data']?>
           </div>
           <div class="col-md-3">
               <b>Valor: </b><br> R$ <?=$atendimento['preco']?>
           </div>
           <div class="col-md-12">
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <div class="avatar-group">
                        <a href="#" class="avatar avatar-sm rounded-circle" data-toggle="tooltip" data-original-title="Felipe Vilella">
                            <img src="<?= $atendimento['fotoPrincipal'];?>" class='imagem-tipo-atendimento'>
                        </a>    
                        <a href="#" class="avatar avatar-sm rounded-circle" data-toggle="tooltip" data-original-title="Marli">
                            <img src="<?= $atendimento['fotoPrincipalAnimal'];?>" class='imagem-tipo-atendimento'>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <b>Nome: </b><br>  <?=$atendimento['nomeAnimal']?> 
                </div>
                <div class="col-md-4">
                    <b>Responsavel: </b><br>  <?=$atendimento['nomeUsuario']?> 
                </div>
            </div>
        </div>
    </div>
</div>

</div>
