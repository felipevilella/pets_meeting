<?php if ($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS"): ?>
  <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
          <a class="logo" href="<?php echo base_url("financeiro");?>">
            <img src="<?= base_url('assets/bootstrap_dashbord/img/logo.png');?>" class="navbar-brand-img"> 
          </a>

        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item  active">
              <?php if ($informacaoUsuario["comercial"] == 0): ?>
                <a class=" nav-link  dir1 active "href="<?php echo base_url("pesquisa");?>"> 
                  <i class="ni ni-shop text-orange"></i> Pet Shop
                </a>
                <?php else: ?>
                 <a class=" nav-link dir1 active "href="<?php echo base_url("financeiro");?>"> 
                  <i class="ni ni-chart-pie-35 text-orange"></i> Gestão
                </a> 
              <?php endif ?>
            </li>
            <li class="nav-item">
              <?php if ($informacaoUsuario["comercial"] == 0): ?>
                <a class="nav-link dir2" href="<?php echo base_url("selecionarperfilpets");?>">
                  <i class="ni ni-favourite-28 text-orange"></i> Meus Pets
                </a>
                <?php else: ?>
                 <a class="nav-link dir2" href="<?php echo base_url("servicos");?>"> 
                  <i class="ni ni-basket text-orange"></i> Meus Serviços
                </a>
              <?php endif ?>
            </li>           
            <?php if ($informacaoUsuario["comercial"] == 1): ?>
              <li class="nav-item">
                <a class="nav-link dir5" href="<?php echo base_url("vacinas");?>">
                  <i class="ni ni-briefcase-24 text-orange"></i> Vacinas
                </a>
              </li>
            <?php endif?>
            <li class="nav-item">
              <?php if ($informacaoUsuario["comercial"] == 1): ?>
                <a class="nav-link dir7" href="#" id="sobre">
                  <?php else: ?>
                    <a class="nav-link dir7" href="#" id="soborangeono">
                    <?php endif ?>
                    <i class="ni ni-badge text-orange"></i> Minha informações
                  </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
      <div class="main-content">
          <nav class="navbar navbar-top navbar-expand navbar-light bg-secondary border-bottom">
            <div class="container-fluid">
              <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
                <div class="form-group mb-0">
                  <div class="input-group input-group-alternative">
                    <input class="form-control" placeholder="Search" type="text" hidden>
                  </div>
                </div>
              </form>
              <!-- User -->
              <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                  <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                      <span class="avatar avatar-sm rounded-circle">
                        <?php
                        if (empty($informacaoUsuario["fotoPrincipal"])) {
                          echo "<img class='imagem-perfil-menu' src=".base_url('assets/personalizado/imagem/avatar.jpg').">";
                        } else {
                          echo "<img class='imagem-perfil-menu' src='".base_url("assets/personalizado/foto_usuario/".$informacaoUsuario["fotoPrincipal"])."'>";
                        }
                        ?>  
                      </span>

                      <div class="media-body ml-2 d-none d-lg-block">
                        <span class="mb-0 text-sm  font-weight-bold"><?= $nome?></span>
                      </div>
                    </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <a href="#" class="dropdown-item"  data-toggle="modal" data-target="#modal-fotoPerfil">
                      <i class="ni ni-circle-08"></i>
                      <span>Alterar foto</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo base_url('sair');?>" class="dropdown-item">
                      <i class="ni ni-user-run"></i>
                      <span>Sair</span>
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
         <div class="main-content">
          <?php if ($dispositivos["plataforma"] == "Android" || $dispositivos["plataforma"] == "iOS"): ?>
          <div class="container-fluid">
            <nav class="navbar navbar-horizontal fixed-bottom fixed-left navbar-expand-md  bg-white" style="border-top: solid 0.1px #ada0a0;">
              <div class="container-fluid texto-nav">
                <ul class="nav">
                  <li class="nav-item col-3">
                    <?php if ($informacaoUsuario["comercial"] == 0): ?>
                      <a class=" nav-link active "href="<?php echo base_url("noticias");?>"> 
                        <?php else: ?>
                          <a class=" nav-link active "href="<?php echo base_url("financeiro");?>"> 
                          <?php endif ?>
                          <i class="ni ni-shop text-orange center col-12 dir1"></i><br> <p>Inicio</p>
                        </a>
                      </li>
                      <li class="nav-item col-3">
                        <?php if ($informacaoUsuario["comercial"] == 0): ?>
                          <a class=" nav-link active "href="<?php echo base_url("pesquisa");?>"> 
                            <i class="ni ni-basket text-orange center dir2"></i> <br> <p align='center'>Serviços</p>
                          </a>
                        <?php endif ?>
                      </li>
                      <li class="nav-item col-3">
                        <?php if ($informacaoUsuario["comercial"] == 0): ?>
                          <a class="nav-link" href="<?php echo base_url("selecionarperfilpets");?>">
                            <i class="ni ni-favourite-28 text-orange center dir3"></i><br><p align='center'>Pets</p>
                          </a>
                          <?php else: ?>
                           <a class="nav-link dir3" href="<?php echo base_url("servicos");?>"> 
                            <i class="ni ni-basket text-orange center dir3"></i> <br> <p align='center'>Serviços</p>
                          </a>
                        <?php endif ?>
                      </li>
                      <li class="nav-item col-3">
                        <?php if ($informacaoUsuario["comercial"] == 0): ?>
                          <a class="nav-link" href="<?php echo base_url("perfil");?>">
                            <i class="ni ni-badge text-orange center dir4"></i><br> <p align='center'> Perfil </p>
                          </a>
                          <?php else: ?>
                            <a class="nav-link" href="<?php echo base_url("vacinas");?>">
                              <i class="ni ni-briefcase-24 text-orange center dir4"></i><br> <p align='center'> Vacinas</p>
                            </a>
                          <?php endif ?>
                        </a>
                      </li>
                    </nav> 
                  </li>
                </ul>
              </div></div>
            <?php endif ?>
        <?php else: ?>
        <div class="main-content">
          <?php if ($dispositivos["plataforma"] == "Android" || $dispositivos["plataforma"] == "iOS"): ?>
          <div class="container-fluid">
            <nav class="navbar navbar-horizontal fixed-bottom fixed-left navbar-expand-md  bg-white" style="border-top: solid 0.1px #ada0a0;">
              <div class="container-fluid texto-nav">
                <ul class="nav">
                  <li class="nav-item col-3">
                    <?php if ($informacaoUsuario["comercial"] == 0): ?>
                      <a class=" nav-link active "href="<?php echo base_url("noticias");?>"> 
                        <?php else: ?>
                          <a class=" nav-link active "href="<?php echo base_url("financeiro");?>"> 
                          <?php endif ?>
                          <i class="ni ni-shop text-orange center col-12 dir1"></i><br> <p>Inicio</p>
                        </a>
                      </li>
                      <li class="nav-item col-3">
                        <?php if ($informacaoUsuario["comercial"] == 0): ?>
                          <a class=" nav-link active "href="<?php echo base_url("pesquisa");?>"> 
                            <i class="ni ni-basket text-orange center dir2"></i> <br> <p align='center'>Serviços</p>
                          </a>
                           <?php else: ?>
                           <a class="nav-link dir3" href="<?php echo base_url("atendimentos");?>"> 
                            <i class="ni ni-cart text-orange center dir3"></i> <br> <p align='center'>Reservas</p>
                          </a>
                        <?php endif ?>
                      </li>
                      <li class="nav-item col-3">
                        <?php if ($informacaoUsuario["comercial"] == 0): ?>
                          <a class="nav-link" href="<?php echo base_url("selecionarperfilpets");?>">
                            <i class="ni ni-favourite-28 text-orange center dir3"></i><br>
                            <p align='center'>Pets</p>
                          </a>
                          <?php else: ?>
                           <a class="nav-link dir3" href="<?php echo base_url("servicos");?>"> 
                            <i class="ni ni-basket text-orange center dir3"></i> <br> <p align='center'>Serviços</p>
                          </a>
                        <?php endif ?>
                      </li>
                      <li class="nav-item col-3">
                        <?php if ($informacaoUsuario["comercial"] == 0): ?>
                          <a class="nav-link" href="<?php echo base_url("perfil");?>">
                            <i class="ni ni-badge text-orange center dir4"></i><br> <p align='center'> Perfil </p>
                          </a>
                          <?php else: ?>
                            <a class="nav-link" href="<?php echo base_url("vacinas");?>">
                              <i class="ni ni-briefcase-24 text-orange center dir4"></i><br> <p align='center'> Vacinas</p>
                            </a>
                          <?php endif ?>
                        </a>
                      </li>
                    </nav> 
                  </li>
                </ul>
              </div></div>
            <?php endif ?>
<?php endif?>
          
     