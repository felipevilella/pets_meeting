
</div>
<?php if (!isset($telaCheia)): ?>
    <?php if ($dispositivos["plataforma"] == "Android" || $dispositivos["plataforma"] == "iOS"): ?>
    <br><br><br>
  <?php endif ?>
<?php endif ?>

<div class="modal fade " id="modal-footer" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-default modal-dialog-centered modal-" role="document">
    <div class="modal-content bg-gradient-orange">

      <div class="modal-header">
        <h5 class="modal-title tituloModal" id="modal-title-notification"></h5>
      </div>

      <div class="modal-body">
        <?php if (isset($sobre)): ?>
         <?php if ($sobre == 0): ?>
          <h4 class="modal-title">
            Seja bem vindo ao Pets Meeting, informe os seus dados para que seu estabelecimento seja divulgado.
          </h4><hr>

        <?php endif ?>
      <?php endif ?>
      <div class="row modalFooterConteudo">
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Voltar</button>
       <button type="button" class="btn btn-white botaoModal">Salvar</button>
     </div>
   </div>
 </div>
</div>
</div>


<div class="modal fade" id="modal-fotoPerfil" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
  <div class="modal-dialog  modal-" role="document">
    <div class="modal-content">
      <form action="<?php echo base_url('alterarFotoUsuario');?>" method="post" enctype="multipart/form-data">
          <input type="file" name="fotousuarioperfil" class="filepond" />
       </form>
    </div>
  </div>
</div>

 <div class="modal fade " id="modal-autenticacao" tabindex="-1" role="dialog" aria-labelledby="modal-autenticacao" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog modal-white modal-dialog-centered modal-" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tituloModal">Confirme sua alteração</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p align="center">Em breve você receberá uma mensagem de texto no numero do celular cadastro com seu codigo de confirmação</p>
          <input type="number" id='codigo' class="form-control"> 
           <div class="validation errror" id="erroCodigo"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
          <button type="button" class="btn btn-warning"  id="confirmarDadosBancarios">Confirmar</button>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade " id="modal-foto" tabindex="-1" role="dialog" aria-labelledby="modal-foto" aria-hidden="true">
  <div class="modal-dialog modal-white modal-dialog-centered modal-" role="document">
    <div class="modal-content bg-gradient-white">
    <div class="modal-body">
        <p align="center">
          <img src="<?php echo base_url('assets/personalizado/imagem/artes/adicionar_foto.jpg')?>" class='imagem-fundo-sem-consulta '>
          </p>
        </div>
        <div class="col-12">
          <p align="center"><b>Adicione uma foto do seu estabeleciomento!</b></p>
        </div>
      <div class="row modalFooterConteudo">
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-warning adicionarFoto" data-toggle="modal" data-target="#modal-fotoPerfil">Adicionar Foto</button>
     </div>
   </div>
 </div>
</div>

<div class="modal fade " id="modal-funcionario-estabelecimento" tabindex="-1" role="dialog" aria-labelledby="modal-funcionario" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-white modal-dialog-centered modal-" role="document">
    <div class="modal-content bg-gradient-white">
    <div class="modal-body">
        <p align="center">
          <img src="<?php echo base_url('assets/personalizado/imagem/artes/funcionario.jpg')?>" class='imagem-fundo-sem-consulta '>
          </p>
        </div>
        <div class="col-12">
          <p align="center"><b>Adicione um funcionario para que possa disponibilizar horarios para os seus clientes!</b></p>
        </div>
      <div class="row modalFooterConteudo">
      </div>
      <div class="modal-footer">
       <a href="#"><button type="button" class="btn btn-warning adicionarFuncionario">Adicionar funcionario</button></a>
     </div>
   </div>
 </div>
</div>

<div class="modal fade " id="modal-endereco-estabelecimento" tabindex="-1" role="dialog" aria-labelledby="modal-endereco-estabelecimento" aria-hidden="true">
  <div class="modal-dialog modal-white modal-dialog-centered modal-" role="document">
    <div class="modal-content bg-gradient-white">
    <div class="modal-body">
        <p align="center">
          <img src="<?php echo base_url('assets/personalizado/imagem/artes/endereco.jpg')?>" class='imagem-fundo-sem-consulta '>
          </p>
        </div>
        <div class="col-12">
          <p align="center"><b>Para que os seus clientes possam encontrar o seu serviço, é necessario adicionar um endereço.</b></p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-warning adicionarEndereco">Adicionar endereço</button>
      </div>
     </div>
   </div>
 </div>

<div class="modal fade " id="modal-banco-estabelecimento" tabindex="-1" role="dialog" aria-labelledby="modal-banco-estabelecimento" aria-hidden="true">
  <div class="modal-dialog modal-white modal-dialog-centered modal-" role="document">
    <div class="modal-content bg-gradient-white">
    <div class="modal-body">
        <p align="center">
          <img src="<?php echo base_url('assets/personalizado/imagem/artes/banco.jpg')?>" class='imagem-fundo-sem-consulta '>
          </p>
        </div>
        <div class="col-12">
          <p align="center"><b>Para que os seus clientes possam reservar os seus serviços, e você receber direto na sua conta no 5 dia util. Será necessario cadastrar os seus dados bancários!
          </b></p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-warning adicionarDadosBancarios">Adicionar dados bancarios</button>
      </div>
     </div>
   </div>
 </div>

 <div class="modal fade " id="modal-avaliacao" tabindex="-1" role="dialog" aria-labelledby="modal-avaliacao" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog modal-white modal-dialog-centered modal-" role="document">
    <div class="modal-content bg-gradient-white">
       <div class="modal-body">
         <p align="center" class="texto"> </p>
          <div class="estrelas col-12">
            <input type="radio" name="estrela" value="" checked>
            
            <label for="estrela_um"><i class="fas fa-star"></i></label>
            <input type="radio" id="estrela_um" name="estrela" value="1">
            
            <label for="estrela_dois"><i class="fas fa-star"></i></label>
            <input type="radio" id="estrela_dois" name="estrela" value="2">
            
            <label for="estrela_tres"><i class="fas fa-star"></i></label>
            <input type="radio" id="estrela_tres" name="estrela" value="3">
            
            <label for="estrela_quatro"><i class="fas fa-star"></i></label>
            <input type="radio" id="estrela_quatro" name="estrela" value="4">
            
            <label for="estrela_cinco"><i class="fas fa-star"></i></label>
            <input type="radio" id="estrela_cinco" name="estrela" value="5">
        
          </div>

          <p align="center">
            <br><button class="btn-sm btn-white add-comentario collapse show"> Deixar um comentário</button>
          </p>

          <div class="collapse div-comentario">
            <textarea class="form-control" id="comentario" rows="3" placeholder="Deixe um comentário sobre o atendimento"></textarea>
         
        </div>
     </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-warning avaliar">Avaliar</button>
      </div>
   </div>
 </div>





<div id="loading-animais" class="hidden"></div>

<script src="<?php echo base_url('assets/bootstrap_dashbord/js/plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/bootstrap_dashbord/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js');?>"></script>
<script src="<?php echo base_url('assets/bootstrap_dashbord/js/plugins/chart.js/dist/Chart.min.js');?>"></script>
<script src="<?php echo base_url('assets/bootstrap_dashbord/js/plugins/chart.js/dist/Chart.extension.js');?>"></script>
<script src="<?php echo base_url('assets/bootstrap_dashbord/js/argon-dashboard.min.js?v=1.1.0');?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/plugins/bootstrap-notify.js');?>"></script>
<script src="<?php echo base_url('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js');?>"></script>


<script src="<?php echo base_url('assets/personalizado/js/tabs.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/jasny-bootstrap.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/jquery.mask.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/mascara.js');?>"></script>
<script src="<?php echo base_url('assets/personalizado/js/filepond.min.js');?>"></script>



<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-transform/dist/filepond-plugin-image-transform.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.js"></script> 
<script src="https://unpkg.com/filepond-plugin-image-crop/dist/filepond-plugin-image-crop.js"></script>

<script src="<?php echo base_url('assets/personalizado/js/sistema.js');?>"></script>

<script type="text/javascript">
  var base_url = "<?php echo base_url();?>";
</script>

<?php foreach ($scripts as $script): ?>
  <script src="<?php echo base_url($script); ?>"></script>
<?php endforeach ?>

<?php if (isset($scriptMaps)): ?>
  <?php foreach ($scriptMaps as $script): ?>
    <script  src="<?php echo $script?>" async defer></script>
  <?php endforeach ?>
<?php endif ?>

<script>
  window.TrackJS &&
  TrackJS.install({
    token: "ee6fab19c5a04ac1a32a645abde4613a",
    application: "argon-dashboard-free"
  });


  function cadastro_success(){
   Swal(
    'Cadastrado!',
    'clique para continuar!',
    'success'
    )
 }

 function email_success(){
   Swal(
    'Enviado!',
    'Verifique sua caixa de entrada!',
    'success'
    )
 }
 function senha_success(){
   Swal(
    'Senha alterada!',
    'acesse sua conta com a sua nova senha!',
    'success'
    )
 }

 function senha_error() {
   Swal({
    type: 'error',
    title: 'Oops...',
    text: 'Senhas divergentes!',
  })
 }
 function email_error() {
   Swal({
    type: 'error',
    title: 'Oops...',
    text: 'E-mail informado está incorreto ou não existe!',
  })
 }

  </script>



</body>

<?php if (isset($sobre)): ?>
  <?php if ($sobre == 0): ?>
    <script> ativarModal();</script>
    <?php else: ?>
      <script> verificarEndereco();</script>
  <?php endif ?>
<?php endif ?>

