   <h6 class="heading-small text-muted mb-4">Funcionarios</h6>
   <div class="row">
    <div class="col-md-12">
      <?php foreach ($veterinarios as $veterinario): ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <a href="#" class="avatar avatar-xl rounded-circle">
                    <img alt="Image placeholder" src="<?= base_url('assets/personalizado/imagem/veterinarioPesoa.jpg')?>">
                  </a>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                   <?= $veterinario["nome"]?>
                 </h4>
               </div>
               <div class="col-auto">
                  <button type="button" class="btn btn-sm btn-warning btn-desativarVeterinario"  
                  data-veterinario="<?=$veterinario['idveterinario'];?>">
                   Excluir
                 </button>   
               <button type="button" class="btn btn-sm btn-primary btn-editarVeterinario" data-veterinario="<?=$veterinario['idveterinario'];?>">
                 Editar
               </button>
             </div>
           </div>
         </div>
       </div>
     </div>
   <?php endforeach ?>
 </div>
