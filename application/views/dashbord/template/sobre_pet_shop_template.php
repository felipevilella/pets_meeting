
<div class="col-md-12"> 
  <div class="form-group">
    <label >CNPJ</label>
    <div class="nomeServico">
     <?php if (isset($sobre["cnpj"])): ?>
      <input type="text" name="cnpj" id="cnpj" class="form-control" value = '<?= $sobre["cnpj"] ?>'>
      <?php else: ?>
        <input type="text" name="cnpj" id="cnpj" class="form-control" value = ''>
      <?php endif ?>
    </div>
    <span id="erroCNPJSobre"  class="errorCampo"></span>
  </div>
</div>

<div class="col-md-12"> 
  <div class="form-group">
    <label > Razão social</label>
    <div class="nomeServico">
     <?php if (isset($sobre["razaoSocial"])): ?>
      <input type="text" name="razaoSocial" id="razaoSocial" class="form-control" value = '<?= $sobre["razaoSocial"] ?>'>
      <?php else: ?>
        <input type="text" name="razaoSocial" id="razaoSocial" class="form-control" value = ''>
      <?php endif ?>
    </div>
    <span id="erroRazaoSocialSobre"  class="errorCampo"></span>
  </div>
</div>

<div class="col-md-12"> 
  <div class="form-group">
    <label >Nome do estabelecimento</label>
    <div class="nomeServico">
     <?php if (isset($sobre["nome"])): ?>
      <input type="text" name="nomePetshop" id="nomePetshop" class="form-control" value = '<?= $sobre["nome"] ?>'>
      <?php else: ?>
        <input type="text" name="nomePetshop" id="nomePetshop" class="form-control" value = '<?= $nome ?>'>
      <?php endif ?>
    </div>
    <span id="erroNomeSobre"  class="errorCampo"></span>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
    <label >Abre as</label>
    <?php if (isset($sobre["horarioAbertura"])): ?>
      <input type="time" name="horarioAbertura" id="horarioAbertura" class="form-control" value ='<?= $sobre["horarioAbertura"] ?>'>
      <?php else: ?>
        <input type="time" name="horarioAbertura" id="horarioAbertura" class="form-control" placeholder="">
      <?php endif ?>
      <span id="erroAbertura"  class="errorCampo"></span>
    </div>
  </div>

  <div class="col-md-6">
    <div class="form-group">
      <label >Fecha as</label>
      <?php if (isset($sobre["horarioFechamento"])): ?>
        <input type="time" name="horarioFechamento" id="horarioFechamento" class="form-control" value ='<?= $sobre["horarioFechamento"] ?>'>
        <?php else: ?>
          <input type="time" name="horarioFechamento" id="horarioFechamento" class="form-control" placeholder="">
        <?php endif ?>
        
        <span id="erroFechamento"  class="errorCampo"></span>
      </div>
    </div>
    
    <div class="col-md-12">
      <div class="form-group">
        <label id="contador_caracter">Telefone</label>
        <?php if (isset($sobre["contato"])): ?>
          <input type="text" name="telefone" id="telefone" class="form-control" value ='<?= $sobre["contato"] ?>'>
          <?php else: ?>
            <input type="text" name="telefone" id="telefone" class="form-control" placeholder="">
          <?php endif ?>
          <span id="erroEndereco"  class="errorCampo"></span>
        </div>
      </div>

        <div class="col-md-12">
          <div class="form-group">
            <label id="contador_caracter">Celular</label>
            <?php if (isset($sobre["celular"])): ?>
              <input type="text" name="celular" id="telefone" class="form-control telefone" value ='<?= $sobre["celular"] ?>' disabled>
              <?php else: ?>
                <input type="text" name="celular" id="celular" class="form-control telefone" placeholder="">
              <?php endif ?>
              <span id="errorCelular"  class="errorCampo"></span>
            </div>
        </div>


        <div class="col-md-12"> 
          <div class="form-group">
            <label id="contador_caracter">Descrição do estabelecimento</label>
            <?php if (isset($sobre["descricao"])): ?>
              <textarea id="descricaoSobre" class="form-control" heigth="100px" maxlength="300"><?=$sobre["descricao"]?></textarea>
              <?php else: ?>
                <textarea id="descricaoSobre" class="form-control" heigth="100px" maxlength="300" placeholder="Descreva para os seus clientes sobre o seu estabelecimento"></textarea>
              <?php endif ?>
              <span id="erroDescricaoSobre"  class="errorCampo"></span>
            </div>
          </div>

