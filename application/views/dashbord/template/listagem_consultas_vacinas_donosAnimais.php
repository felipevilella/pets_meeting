<?php if ($vacinas): ?>  
  <?php foreach ($vacinas as $vacina): ?>
    <div class="card">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-auto">
            <a href="#" class="avatar avatar-xl rounded-circle">
              <img src="<?php echo base_url('assets/personalizado/imagem/artes/veterinario.jpg');?>" class='imagem-tipo-servico'>
            </a>
          </div>
          <div class="col ml--2">
            <h4 class="mb-0">
             <?= $vacina["nomeVacina"]?>
           </h4>
           <small><?=$vacina["hora"];?> - <?=$vacina["data"]?></small><br>
            <small> Agendado por  <?= $vacina["nomePetshop"]?></small>
         </div>
         <div class="col-auto">

          <?php if ($vacina["status"] == 1): ?>
            <a href="#">
              <i class="ni ni-time-alarm text-green"></i>
            </a>
            <a href="#" class="btn-abriVacina" data-vacina="<?=$vacina['idVacina'];?>">
              <i class="ni ni-align-left-2 text-blue"></i>
            </a>
          <?php endif ?>

          <?php if ($vacina["status"] == 3): ?>
           <i class="ni ni-fat-remove tex-red"></i>
         <?php endif ?>

         <?php if ($vacina["status"] == 2): ?>
           <i class="ni ni-check-bold text-green"></i>
         <?php endif ?>
       </div>
     </div>
   </div>
 </div>
<?php endforeach ?>
<?php else: ?>
    <p align="center">
      <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_vacinas.jpg')?>" class='imagem-fundo-sem-consulta'>
    </p>
    <p align="center"> Que pena! você ainda não possui vacinas. </p>
<?php endif ?>
