<?php if ($horarios): ?>
	<?php foreach ($horarios as $key => $horario): ?>
		<input type="hidden" id='funcionario' value="<?=$veterinario['codigoVeterinario']?>">
		<div class="custom-control custom-radio custom-control-inline">
			<input type="radio" id="customRadioInline<?=$horario?>" name="customRadioInline1" value="<?=$horario?>" class="custom-control-input">
			<label class="custom-control-label" for="customRadioInline<?=$horario?>"><h4><?=$horario?></h4></label>
		</div>
	<?php endforeach ?>
	<?php else: ?>
		<p align="center">
			<img src= '<?= base_url("assets/personalizado/imagem/artes/sem_horario_disponivel.jpg")?>' class="imagem-fundo-agendamento">
		</p>
		<p align="center">
			<font color="black"> Desculpe, o dia selecionado<br>  não possui reservas disponiveis!</font>
		</p>
<?php endif ?>
		
	

	