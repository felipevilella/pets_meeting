<?php if ($servicos): ?>
	<?php foreach ($servicos as $servico): ?>
		<div class="card">
			<div class="card-body">
				<div class="row align-items-center">
					<div class="col-auto">
						<a href="<?= $servico['url']?>">
							<img alt="Image placeholder" src="<?= $servico["fotoPrincipal"];?>" class='imagem-tipo-servico'>
						</a>
					</div>
					<div class="col ml--2">
						<a href="<?= $servico['url']?>"><h5><b><?= $servico["nomeServico"];?></b></h5></a>
						<a href="<?= $servico['url']?>"><h6><?= $servico["nome"];?></h6></a>
						<a href="<?= $servico['url']?>"><h6><?= $servico["endereco"];?> ° <?= $servico["localizacao"];?> km </h6></a>
					</div>
				</div>
			</div>
		</div></a>
	<?php endforeach ?>
	<?php else: ?>
		<p align="center">
			<img src="<?php echo base_url('assets/personalizado/imagem/artes/servico_nao_encontrado.jpg')?>" class='imagem-fundo-servicos'>
		</p>
		<p align="center"> Serviço não encontrado<br> proximo a sua região! </p>
<?php endif ?>
