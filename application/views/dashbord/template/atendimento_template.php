<?php if ($atendimentos): ?>
	<?php foreach ($atendimentos as $atendimento): ?>
	<div class="col-12">
		<div class="card-body">
			<div class="row align-items-center">
				<div class="col-auto">
					<div class="avatar-group">
						<a href="#" class="avatar avatar-sm rounded-circle" data-toggle="tooltip" data-original-title="<?=$atendimento["nomeUsuario"]?>">
							<img  src="<?=$atendimento["fotoPrincipal"]?>" class='imagem-tipo-atendimento'>
						</a>
						<a href="#" class="avatar avatar-sm rounded-circle" data-toggle="tooltip" data-original-title="<?=$atendimento["nomeAnimal"]?>">
							<img  src="<?=$atendimento["fotoPrincipalAnimal"]?>" class='imagem-tipo-atendimento'>
						</a>
					</div>
				</div>
				<div class="col ml--2">
					<h4 class="mb-0">
						<a href=""><?=$atendimento["nomeServico"]?></a>
					</h4>
					<p class="text-sm text-muted mb-0"><?=$atendimento["categoria"]?></p>
					<small><?=$atendimento["data"]?> - <?=$atendimento["hora"]?></small>
				</div>
				<div class="col-auto">
					<button type="button" data-agendamento="<?=$atendimento['idReservaServico']?>"
					 class="btn btn-sm btn-default ver-atendimento">Visualizar</button>
				</div>
			</div>
		</div>
	</div>
<?php endforeach ?>
<?php else: ?>
	<div class="col-12">
      <div class="card-body">
        <div class="col-12">
          <p align="center">
            <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem-atendimentos.jpg')?>" class="<?= $dispositivos['plataforma'] != 'Android' && $dispositivos['plataforma'] != 'iOS'? 'imagem-fundo-sem-atendimento':'imagem-fundo-sem-consulta'?>">
            </p>
          </div>
          <div class="col-12">
            <p align="center"> Você não possui atendimento agendado dentro desse periodo.</p>
          </div>
        </div>
    </div>
<?php endif ?>
