<div class="header bg-gradient-white pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			<div class="">
				<h3 class="ct-title" id="content"><center>Cartão de credito</center></h3><hr>
			</div>
		</div>
	</div>
</div>
<center><font size="2"> <i class="fas fa-lock"></i> Você está em uma área com conexão segura</font></center>
<div class="card-body">
  	<div class="row">
    	<div class="col-12">
      		<input type='text' name='cartao' id='cartao' class="form-control col-12 cartao" placeholder="Numero do cartão" 
      		 value="">
      		<div class="validation errror" id="erroNumeroCartao"></div><br>
  		</div>
  		<div class="col-6">
      		<input type='text' name='validade' id='validade' class="form-control col-12 validade" placeholder="Validade" 
      		 value="" >
      		<div class="validation errror" id="erroValidade"></div><br>
  		</div>
  		<div class="col-6">
      		<input type='text' name='cvv' id='cvv' class="form-control col-12 cvv" placeholder="CVV" maxlength="4" value="">
      		<div class="validation errror" id="erroCVV"></div><br>
  		</div>
  		<div class="col-12">
      		<input type='text' name='nomeTitular' id='nomeTitular' class="form-control col-12 nomeTitular" placeholder="Nome do titular" 
      		 value="">
      		<div class="validation errror" id="erroNomeTitular"></div><br>
  		</div>
  		<div class="col-12">
      		<input type='text' name='cpf' id='cpf' class="form-control col-12 cpf" placeholder="CPF" 
      		 value="">
      		<div class="validation errror" id="erroCPFTitular"></div><br>
  		</div><br>

  		<div class="col-12">
  			<button class="col-12 btn btn-danger salvarCartao">Salvar</button>
  		</div>
  	</div>
</div>
