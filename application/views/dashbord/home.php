<div class="header bg-gradient-white  pb-8 pt-5 pt-md-8">
  <div class="container">
    <div class="header-body">
      <div class="col-12">
        <h3 class="card-title text-uppercase text-muted mb-0"> <font color="black"><b>O que está procurando?</b></font> </h3><br>
       <div class="row">
        <div class="col-6">
          <a href="<?= base_url('pesquisa/1');?>"><button class="col-12 btn btn-white btn-sm"> 
            <img class=" avatar avatar-xl" src="<?php echo base_url('assets/personalizado/imagem/artes/banho.jpg');?>">
            <br>Banho e Tosa
          </button> </a>          
        </div>
        <div class="col-6">
           <a href="<?= base_url('pesquisa/3');?>"><button class="col-12 btn btn-white btn-sm"> 
            <img class=" avatar avatar-xl" src="<?php echo base_url('assets/personalizado/imagem/artes/veterinario.jpg');?>">
            <br>Consultas
          </button> </a>           
        </div>
      </div>
    </p>
  </div>
</div>
</div>
</div>
<div class="container-fluid mt--7">
  <div class="col-12">
        <h5 class="card-title text-uppercase text-muted mb-0"><b>Notícias e</b></h5>
        <h3 class="card-title text-uppercase text-muted mb-0"><font color="black"><b>Promoção</b></font></h3>
        <div class="row">
          <div class="col-md-12">
            <img src="<?= base_url('assets/personalizado/imagem/artes/noticia.jpg');?>" class="imagem-fundo-noticias">
          </div>
          <div class="col-12">
            <h3>Agende uma consulta sem precisar sair de casa!</h3>
          </div>
        </div>
  </div>
</div>

