	<div class="header bg-gradient-white pb-3 pt-md-8">
		<div class="container-fluid">
			<div class="header-body">
				<div class="">
					<br>
					<h3 class="ct-title" id="content"><center>Minhas informações pessoais</center></h3><hr>
				</div>
			</div>

			 
			  <div class="form-group">
			    <label >Nome</label>
			    <div class="nomeServico">
			     <?php if (isset($informacaoUsuario["nome"])): ?>
			      <input type="text" name="nome" id="nome" class="form-control" value = '<?= $informacaoUsuario["nome"] ?>'>
			      <?php else: ?>
			        <input type="text" name="nome" id="nome" class="form-control" value = '<?= $nome ?>'>
			      <?php endif ?>
			    </div>
			    <span id="erroNomeSobre"  class="errror"></span>
			  </div>
			
		    
		    
		      <div class="form-group">
		        <label id="contador_caracter">Telefone</label>
		        <?php if (isset($contato["descricao"])): ?>
		          <input type="text" name="telefone" id="telefone" class="form-control" value ='<?= $contato["descricao"] ?>'>
		          <?php else: ?>
		            <input type="text" name="telefone" id="telefone" class="form-control" placeholder="">
		          <?php endif ?>
		          <span id="erroEndereco"  class="errror"></span>
		        </div>
		      <br>
		      <p align="center"><button type="button" id="salvarSobre" class="btn btn-sm btn-warning col-12">Salvar</button></p>
		</div>
	</div>