<div class="header bg-gradient-white pb-8 pt-1=0 pt-md-8">
	<!--    ><hr>    --> 

	<a href="#" data-toggle="modal" data-target="#modal-fotoPerfil">
		<img src="<?=$dadosPets['fotoPrincipal'];?>" class="imagem-perfil-pets-perfil ">
	</a>
</div>


<div class="container-fluid mt--9">
	<div class="col-xl-3 col-lg-6">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<h2 class="mb-0"><b><?= $dadosPets["nome"];?></b></center></h2>
				<h5><?= $dadosPets["racaAnimal"];?>
				<?php if ($dadosPets["sexo"] == "f"): ?>
					- Menina
					<?php else: ?>
						- Menino
					<?php endif ?>     
				</h5>
				<a href="#!" data-toggle="modal" data-target="#modal-editarPerfil">
					<img src="<?php echo base_url('assets/personalizado/imagem/editar.png')?>" class="imagem-editar-perfil">
				</a>
				<a href="#" data-toggle="modal" data-target="#modal-fotoPerfil"><img src="<?php echo base_url('assets/personalizado/imagem/camera.png')?>" class="imagem-camera-perfil-pets"></a>
				<?php if (isset($urlCartao)): ?>
					<a href="<?= $urlCartao?>"><button type="button" class="btn  btn-sm btn-white  col-md-12">Abrir cartão vacina virtual</button></a>
					<?php else: ?>
						<button type="button" class="btn btn-white  col-md-12 btn-sm btn-ativar-cartao"> + Criar Cartão vacina virtual</button>
					<?php endif ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 order-xl-2 mb-3 mb-xl-0">
				<div class="card-body pt-0 pt-md-4">
					<div class="text-center">
							<hr class="my-4"/>
							<div class="row">
								<div class="col-6">
									<button type="button" class="btn btn-sm btn-white col-md-12 btn-consultas">Minhas consultas</button>
								</div>
								<div class="col-6">
									<button type="button" class="btn btn-sm btn-white col-md-12 btn-historico">Meus Historico</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="conteudo">
						<?php if ($consultas): ?>
							<?= $consultas;?>
							<?php else: ?>
								<p align="center">
									<img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_consultas.jpg')?>" class='imagem-fundo-sem-consulta'>
								</p>
								<p align="center"> Parece que  <?= $dadosPets["nome"];?> está sem consulta.<br> Clique em serviços para agendar! </p>
							<?php endif ?>
						</div>
					</div>
				</div>
					<br><br>

				<div class="modal fade" id="modal-editarPerfil" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
					<div class="modal-dialog modal-warning " role="document">
						<div class="modal-content bg-gradient-orage">

							<div class="modal-header">
								<h4 class="modal-title" id="modal-title-notification">Editar perfil</h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>

							<div class="modal-body">
								<div class="row">
									<div class="col-md-6"> 
										<label >Nome</label>
										<div class="input-group input-group-merge input-group-alternative">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="ni ni-circle-08"></i></span>
											</div>
											<input class="form-control" name="nome" id="nome" value="<?=$dadosPets["nome"]?>" placeholder="nome" type="text">
										</div>
										<span id="erroNome"  class="errror"></span>
									</div>
									<div class="col-md-6"> 
										<label >Raça</label>
										<div class="form-group">
											<select class="form-control" id="raca">
												<option value = "<?= $dadosPets["fk_idraca"]?>" > <?= $dadosPets["racaAnimal"]?> </option>
												<?php foreach ($racaPets as $raca): ?>
													<?php if ($raca["idracas"] != $dadosPets["fk_idraca"]): ?>
														<option value = "<?=$raca["idracas"]?>" > <?=$raca["nome"]?></option>
													<?php endif ?>      
												<?php endforeach ?>
											</select>
											<span id="erroRaca" class="errror"></span>
										</div>
									</div>
									<div class="col-md-6"> 
										<label >Ano de nascimento</label>
										<div class="input-group input-group-merge input-group-alternative">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
											</div>
											<input class="form-control" type="year" value="<?= $dadosPets['ano_nascimento']?>" name="data" id="data">
										</div>
										<span id="erroAno" class="errror"></span>
									</div>
									<div class="col-md-6"> 
										<label >Sexo</label><br>
										<div class="custom-control custom-radio custom-control-inline">
											<?php if ($dadosPets["sexo"] == "m"): ?>
												<input type="radio" id="customRadioInline1" name="customRadioInline1" value="m" class="custom-control-input" checked>
												<?php else: ?>
													<input type="radio" id="customRadioInline1" name="customRadioInline1" value="m" class="custom-control-input">
												<?php endif ?>
												<label class="custom-control-label" for="customRadioInline1">Macho</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<?php if ($dadosPets["sexo"] == "f"): ?>
													<input type="radio" id="customRadioInline2" name="customRadioInline1" value="f" class="custom-control-input" checked>
													<?php else: ?>
														<input type="radio" id="customRadioInline2" name="customRadioInline1" value="f" class="custom-control-input">
													<?php endif ?>
													<label class="custom-control-label" for="customRadioInline2">Fêmea</label>
												</div>
												<span id="erroSexo" class="errror"></span>
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Voltar</button>
										<button type="button" class="btn btn-white" id="alterarPets">Salvar</button>
									</div>
								</div>
							</div>
						</div>
