<div class="div-completa">
<div class="card">
    <img src="<?= base_url('assets/personalizado/imagem/fundo-capa.jpg'); ?>" alt="Image placeholder" class="card-img-top imagen-capa">
    <div class="col-lg-3 order-lg-2">
        <div class="card-profile-image">
            <a href="#">
              <a href="#" data-toggle="modal" data-target="#modal-fotoPerfil">
                <img src="<?= $informacaoUsuario["fotoPrincipal"]?>" class="rounded-circle imagem-perfil">
              </a>
                <div class="camera-perfil"> <a href="#" data-toggle="modal" data-target="#modal-fotoPerfil"><img src="<?php echo base_url('assets/personalizado/imagem/camera.png')?>" id="fotoUsuario" width='8%' ></a></div>
            </a>
        </div>
    </div>
    <div class="card-body pt-3">
        <div class="row">
            <div class="col">
                <div class="card-profile-stats d-flex justify-content-center">
                    <div>
                        <span class="heading"><?= $totalAvaliacao? $totalAvaliacao: 0?></span>
                        <span class="description">Avalição</span>
                    </div>
                    <div>
                        <span class="heading"><?= $totalReservas? $totalReservas: 0?></span>
                        <span class="description">Reservas</span>
                    </div>
                    <div>
                        <span class="heading"><?= $totalPets? $totalPets: 0?></span>
                        <span class="description">Animais</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <h5 class="h3"><?= $nome?><span class="font-weight-light"></span></h5><hr>
            <h5 class="col-12">
                <a a href="<?php echo base_url('informacao_pessoais');?>">
                    <button type="button" class="btn btn-sm btn-white col-12">Minhas informações</button>
                </a>
            </h5>
            <h5 class="col-12">
                <a href="<?php echo base_url("adicionar_endereco");?>">
                    <button type="button" class="btn btn-sm btn-white col-12">Meus Endereços</button> 
                </a>
            </h5>
            <h5 class="col-12"><a href="#">
                <a href="<?php echo base_url('carrinho');?>">
                    <button type="button" class="btn btn-sm btn-white col-12"> Meu carrinho</button> 
                </a>
            </h5>
<!--             <h5 class="col-12">
               <a href="<?php echo base_url('qrcodeLeitura');?>">
                    <button type="button" class="btn btn-sm btn-white col-12"> Ler Qrcode</button> 
                </a>
            </h5> -->
            <h5 class="col-12">
                <a href="<?php echo base_url('sair');?>">
                    <button type="button" class="btn btn-sm btn-white col-12 btn-sair"> Sair</button> 
                </a>
            </h5>
        </div>
        

    </div>
    
</div>
</div>