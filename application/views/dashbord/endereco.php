  <div class="header bg-gradient-white pb-3 pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <div class="">
          <br>
          <h3 class="ct-title" id="content">Meus endereços</h3>
          <div class="form-group">
                <div class="input-group input-group-merge">
                  <input class="form-control cep" placeholder="Digite o seu CEP" type="text">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                  </div>
                </div>
              </div><hr>
      </div>
    </div>
  </div>
    <div class="collapse show" id="listaEndereco">
         <?php if (!$enderecos): ?>
            <?php if ($dispositivos["plataforma"] == "Android" || $dispositivos["plataforma"] == "iOS"): ?>
              <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_endereco.jpg');?>" width='100%'>
              <p align="center"> Nenhum endereço encontrado, digite o seu CEP para adicionar um novo.</p>
              <?php else: ?>
              <p align="center">
              <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_endereco.jpg');?>" width='50%'>
              <br> Nenhum endereço encontrado, digite o seu CEP para adicionar um novo.</p>
            <?php endif?>
           <?php else: ?>
            <?php foreach ($enderecos as $endereco): ?>
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="row align-items-center">
                        <div class="col-12">
                          <h4 class="mb-0">
                           <?= $endereco["rua"]?> <?= $endereco["numero"]?>, <?= $endereco["bairro"]?>
                         </h4>
                         <p class="text-sm text-muted mb-0"></p>
                         <small><?=$endereco["cep"];?></small>
                       </div>
                       <div class="col-auto">
                        <?php if ($endereco['principal'] == 0): ?>
                          <button type="button" class="btn btn-sm btn-default btn-ativarEndereco" data_codigo="<?=$endereco['idEndereco'];?>">
                            Usar
                          </button>
                          <?php else: ?>
                            <button type="button" class="btn btn-sm btn-success" data_codigo="<?=$endereco['idEndereco'];?>">
                            Ativo
                          </button>
                          <?php endif ?>
                          <a href="#">
                            <button type="button" class="btn btn-sm btn-primar btn-alteraEndereco" data_codigo="<?= $endereco['idEndereco'] ?>"
                              data_rua="<?= $endereco['rua'] ?>" data_numero="<?= $endereco['numero'] ?>" data_bairro="<?= $endereco['bairro'] ?>" data_cep="<?= $endereco['cep'] ?>">
                              alterar
                            </button>
                          </a>      
                          <button type="button" class="btn btn-sm btn-warning btn-excluirEndereco" data_codigo="<?=$endereco['idEndereco'];?>">
                            Excluir
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            <?php endforeach ?>
           </div>
         <?php endif ?>
      </div>
    </div>
 <div class="container-fluid mt--4">
      <div class="collapse" id="inserirEndereco">
        <div class="row">
          <div class="col-12">
            <label for="example-email-input" class="form-control-label">Rua</label>
            <input type='text' name='rua' id='rua' class="form-control col-12">
            <div class="validation errror" id="erroRua"></div>
          </div>
          <div class="col-6">
            <label for="example-email-input" class="form-control-label">Bairro</label>
            <input type='text' name='bairro' id='bairro' class="form-control col-12">
            <div class="validation errror" id="erroBairro"></div>
          </div>
          <div class="col-6">
            <label for="example-email-input" class="form-control-label">Numero</label>
            <input type='text' name='numero' id='numero' class="form-control col-12">
            </div>
          <div class="col-12"><br>
            <button class="col-12 btn btn-warning" id="salvarEndereco" data_estado='' data_cidade=''>Salvar</button>
          </div>
        </div>
    </div>
  </div>