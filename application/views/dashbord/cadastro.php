<!-- Header -->
<div class="header bg-gradient-orange py-7 py-lg-8">
</div>
<!-- Page content -->
<div class="container mt--8 pb-5">
  <div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-white shadow border-0">
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <small>Realize o seu cadastro</small>
            <div class='erroLogin'></div>
          </div>
          <form role="form">
            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                </div>
                  <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome">
                  <div class="validation errror" id="erroNome"></div>
              </div>
            </div>
            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                </div>
                  <input type="text" name="emailCadastro" class="form-control" id="emailCadastro" placeholder="E-mail">
                  <div class="validation errror" id="erroEmailCadastro"></div>
              </div>
            </div>
            <div class="form-group mb-3">
                <select class="form-control" id="estadoBuscar">
                  <option value="">Selecione um estado</option>
                    <?php foreach ($estados as $estado): ?>
                      <option value= '<?= $estado["idestado"]?>'><?=$estado["nome"]?></option>
                    <?php endforeach ?>
                </select>
              <div class="validation errror" id = "erroEstado"></div>
            </div>

            <div class="form-group mb-3">
                  <select id="Cidade" class="form-control" disabled>
                    <option value="">Selecione uma cidade</option>
                  </select>
              <div class="validation errror" id = "erroCidade"></div>
            </div>
            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                  <input type="password" class="form-control" name="password" id="password" placeholder="Digite sua senha">
                  <div class="validation errror" id="erroSenha"></div>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <input type="password" class="form-control" name="senha" id="password1" placeholder="Repita sua senha">
              </div>
              <div class="validation errror" id="retornosenha"></div>
            </div>
             <input id="comercial" class="form-control hidden" value="0">
            <div class="text-center">
              <button type="button" class="btn btn-warning col-12" id="Cadastrar">Cadastrar</button>
            </div>
          </form><hr>
        </div>
      </div>
    </div>
  </div>
</div>