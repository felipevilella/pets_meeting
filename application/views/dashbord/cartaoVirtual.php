<div class="header bg-gradient-white pb-8 pt-1=0 pt-md-8">
  <!--    ><hr>    --> 

  <a href="#" data-toggle="modal" data-target="#modal-fotoPerfil">
    <img src="<?=$cartaoVacina['fotoAnimal'];?>" class="imagem-perfil-pets-perfil ">
  </a> <div class="camera-perfil-pets"> 
</div>


<div class="container-fluid mt--6">
  <div class="col-xl-3 col-lg-6">
    <div class="card card-stats mb-4 mb-xl-0">
      <div class="card-body">
        <h2><b><?= $cartaoVacina["nomeAnimal"];?></b></center></h2>
        <h5><?= $cartaoVacina["racaAnimal"];?></h5>
        </div>
      </div>
    </div>
        <hr class="my-4"/>
        <div class="row">
          <div class="col-6">
            <button type="button" class="btn btn-sm btn-white col-md-12" id='btn-vacina_pendente' data-vacina = "<?=$codigoCartao?>">
              Vacinas Agendadas
            </button>
          </div>
          <div class="col-6">
            <button type="button" class="btn btn-sm btn-white col-md-12" id='btn-historico_vacina' data-vacina = "<?=$codigoCartao?>">
              Historico
            </button>
          </div>
        </div>
       </div>
      </div>
      
      <div class="listaVacina">
        <?=$historicoVacinas?>
      </div>
      

<div class="modal fade" id="modal-consultaVacina" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
    <div class="modal-content ">
     <div class="modal-header">
      <h5 class="modal-title" id="titulo">Informações da consulta</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="detalheConsultaVacina"> </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Voltar</button>

    </div>
  </div>
  
</div>
</div>

<div class="modal fade" id="modal-agendarVacina" tabindex="-1" role="dialog" aria-labelledby="modal-o" aria-hidden="true">
  <div class="modal-dialog modal-success modal-dialog-centered modal-" role="document">
    <div class="modal-content bg-gradient-default">

      <div class="modal-header">
        <h5 class="modal-title" id="modal-title-funcionario">Agendar Vacina</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body map-modal" id="modal-agendar">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label >Vacina</label>
              <select class="form-control" id="vacina">
                <option value = "" > Selecione uma opção </option>
                <?php foreach ($vacinas as $vacina): ?>
                  <option value = "<?=$vacina["codigoVacina"]?>" > <?=$vacina["nome"]?></option>
                <?php endforeach ?>
              </select>
              <span id="errorVacina"  class="errror"></span>
            </div>
          </div>
          <div class="col-md-6">
            <label >Valor</label>
            <div class="form-group">
              <input type="number" id="valor" class="form-control">
              <span id="erroValor"  class="errror"></span>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-control-label text-white" for="exampleDatepicker">Selecione a data</label>
              <input class="form-control" type="date"  id="dataVacina" value="">
              <span id="erroData"  class="errror"></span>
            </div>
          </div>

          <div class="col-md-12">
            <label id="contador_caracter">Horarios disponiveis</label><br>
            <div class="row col-md-12 horariosVancinas">
              <span id="alertaHorarioReservado"  class="errror"></span>
            </div> 
          </div>
        </div>
      </div>

      <div class="modal-footer funcionario">
        <button type="button" class="btn btn-white btn-agendar" data-codigoPetshop= '<?=$codigoPetshop?>'>Agendar</button>
      </div>

    </div>
  </div>
</div>


