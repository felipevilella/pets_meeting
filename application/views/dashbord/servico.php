
<div class="header bg-gradient-white pb-3 pt-md-8"><br>
	<center><h5 class="ct-title"><b>DETALHES DO SERVIÇO</b></h5></center>
	<img src="<?= $petshop['fotoPrincipal'] ?>" class='imagem-fundo-reserva'>
	<div class="col-12 titulo-reserva-servico"> 

		<h3 class="ct-title"><br>
			<b><?= $servico['nome'];?> </b></center>
		</h3>

		<a href="<?= $urlPetShop?>">
			<hr> 
			<h5 class="ct-title"> 
				<center><i class="ni ni-shop text-orange "></i><font color='red'> <?= strtoupper($petshop['nome']);?> <i class="ni ni-bold-right"></i></font></center>
			</h5>
			<hr>
		</a>

		<h5 class="ct-title"><?= $servico['descricao'];?></h5>
		<div class="container">
			<div class="row">
				<div class="col-2 selecionar_pet" >
					<h5 class="ct-title">Pet:</h5>
				</div>
				<div class="col-10 ">
					<select class="form-control " id="animal">
						<option value=""> Selecione o seu peludinho </option>
						<?php foreach ($pets as $pet): ?>
							<option value="<?=$pet['idanimais'];?>"><?=$pet["nome"];?></option>
						<?php endforeach ?> 
						<span id="erroNome"  class="errror"></span>
					</select>
				</div>
			</div>
		</br>
		<div class="row">  
			<div class="col-2">
				<h5 class="ct-title">Data:</h5>
			</div>
			<div class="col-10">
				<div class="input-group input-group-merge input-group-alternative">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
					</div>
					<input class="form-control" type="date"  id="data" value="" data_codigoPetshop="<?=$petshop['codigoPetshop']?>">
				</div>
				<span id="erroData"  class="errror"></span>
			</div>
			<h5 class="ct-title"><b>Horarios disponíveis: </b></h5>
			<div class="col-12">
				<div class="horario"> </div>
			</div>
		</div> 
	
	
	<div class="modal-footer">
		<button type="button" class="btn col-12 btn-danger" id="btn-adicionar-carrinho">Agendar R$  <?= $servico['preco'];?> </button> 
	</div>
</div>



