<div class="header bg-gradient-white pb-3 pt-md-8">
  <input type="hidden" id='latitude' value="<?=$petshop['latitude'];?>">
  <input type="hidden" id='longitude' value="<?=$petshop['longitude'];?>">
  <div style="width: 100%; height: 250px;"  id="map" class="modal-body map-modal"></div>
</div>
<div class="col-lg-3 order-lg-2">
  <div class="card-profile-image">
    <img src="<?= $petshop["fotoPrincipal"]?>" class="rounded-circle imagem-perfil">
  </div>
  <h3 class="ct-title"><font color='black'><center><b><?= $petshop['nome'];?></b></center></font></h3>
</div>

<div class="container">
  <div class="row">
    <div class="col-6">
      <button class="col-12 btn btn-sm btn-white btn-descricao"  data-toggle="tab" role="tab" href="#sobreEstabelecimento" aria-controls="sobreEstabelecimento" aria-selected="false">Sobre</button>
    </div>
    <div class="col-6">
      <button class="col-12 btn btn-sm btn-white btn-avaliacoes" data-toggle="tab" role="tab" href="#comentarioEstabelecimento" aria-controls="comentarioEstabelecimento" aria-selected="false">Avaliações</button>
    </div>
  </div>
</div>

<div class="card-body">
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="sobreEstabelecimento" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">

      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card-profile-stats d-flex justify-content-center">
              <div>
                <span class="heading"><?= $nota? $nota: 0?></span>
                <span class="description">Nota</span>
              </div>
              <div>
                <span class="heading"><?= $totalVenda? $totalVenda: 0?></span>
                <span class="description">Vendas</span>
              </div>
              <div>
                <span class="heading"><?= $totalComentario? $totalComentario: 0?></span>
                <span class="description">Comentários</span>
              </div>
            </div>
          </div>
        </div>
      </div> 

      <div class="col-12 titulo-reserva-servico"> 
       <h5 class="ct-title"><?=$petshop["descricao"]?></h5>
     </div>
   </div>
   <div class="tab-pane fade" id="comentarioEstabelecimento" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
    <div class="col-12 titulo-reserva-servico"> 
      <br><h3><b><center>Comentarios:</center></b><hr></h3>
      <?php if ($comentarios): ?>
        <?php foreach ($comentarios as $comentario) :?>
          <h4><b> <?= $comentario['nome']?></b></h4> 
          <div class="estrelas-avaliacao">
            <input type="radio" name="estrela" value="" >
            <?php if ($comentario['nota']): ?>
              <label for="estrela_um"><i class="fas fa-star <?= $comentario['nota'] >= 1 ? '':'hidden' ?>"></i></label>
              <input type="radio" id="estrela_um" name="estrela" value="1" disabled>

              <label for="estrela_dois"><i class="fas fa-star <?= $comentario['nota'] >= 2 ? '':'hidden' ?>"></i></label>
              <input type="radio" id="estrela_dois" name="estrela" value="2" disabled>

              <label for="estrela_tres"><i class="fas fa-star <?= $comentario['nota'] >= 3 ? '':'hidden' ?>"></i></label>
              <input type="radio" id="estrela_tres" name="estrela" value="3" disabled>

              <label for="estrela_quatro"><i class="fas fa-star <?= $comentario['nota'] >= 4 ? '':'hidden' ?>"></i></label>
              <input type="radio" id="estrela_quatro" name="estrela" value="4" disabled>

              <label for="estrela_cinco"><i class="fas fa-star <?= $comentario['nota'] == 5 ? '':'hidden' ?>"></i></label>
              <input type="radio" id="estrela_cinco" name="estrela" value="5" disabled>
            <?php endif ?>
          </div>
          <p align="justify" class="texto-comentario"><?= $comentario['comentario']?></p><hr>
        <?php endforeach?>
        <?php else: ?>
          <p align="justify" class="texto-comentario"> Este estabelecimento não possui avaliações </p>
        <?php endif ?>
      </div>
    </div>
  </div>
</div>
</div>
