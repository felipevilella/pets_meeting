<div class="header bg-gradient-white pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			<div class="">
				<br><h3 class="ct-title" id="content"><center>Carrinho</center></h3>
			</div>
		</div>
	</div>
</div>

<?php if ($servicosReservados): ?>
	<div class="card">
		<div class="card-body">
			<div class="row align-items-center">
				<?php foreach ($servicosReservados as $servicoReservado) : ?>
				    <div class="col-8">
				    	<h3 class="mb-0">
				            <?=$servicoReservado["nomePetshop"]?>

				        </h3>
				    </div>
				    <div class="col-4">
				    	<a href="#" data-codigo="<?=$servicoReservado['idCarrinho']?>" class='removerPedido'>
				    		<font color="red" size="1"> Remover intem</font>
				    	</a>
				    </div>
				    <hr>
				    <div class="col-12">
				        <p class="text-sm text-muted mb-0"> 
				        	<font color="black" size='3'>
				        		<b> 1X <?=$servicoReservado["nome"]?></b>
				        	</font>
				        	 -  
				        	<font color="black">
				        	 	R$ <?= $servicoReservado['preco'];?>
				        	</font>
				        </p>
				        <h5>
				        	° Serviço reservado para <?= $servicoReservado['nomeAnimal']?> <br>
				        	° Horário : <?= $servicoReservado['hora']?><br>
				        	° Data: <?= $servicoReservado['data']?>
				        </h5>

				     </div>
				<?php endforeach ?>
				 	<div class="col-12">
				 		<hr>
				        	<a href="<?=base_url('pesquisa')?>">
				        		<font color="red">
				        			<center>Reservar mais serviços</center>
				        		</font>
				        	</a>
				        <hr>
				    </div>
			</div>
		</div>
		    <div class="col-auto">
		    	<div class="container">
					<h4><font color="black"> Total : R$ <?= $total?></font></h4>
				</div>
			</div>
			</div>
		</div>
	</div></a><br>

	<div class="card">
		<div class="card-body">
			<div class="row align-items-center">
				<h3 class="col-6">
	            	Cupons
	        	</h3>
	        	<h6 class="col-6">
	        		<center>Adicionar um código</center>
	        	</h6>
			</div>
		</div>
	</div><br>

	<div class="card">
		<div class="card-body">
			<div class="row align-items-center">
				<div class="container">
					<h3 class="mb-0">
		           		Pagamento
		        	</h3>
			    </div>
		   	</div><hr>
		   	<div class="row align-items-center">
		   		<?php if (isset($numeroCartao)): ?>
		   			<div class="col-8">
						<h4 class="mb-0">
				           <?=$numeroCartao?>
				        </h4>
			    	</div>
			    	<div class="col-4">
			    	 <center>
			        	<h6 class="mb-0">
			            	<a href="#" data_codigo="<?=$codigoCartao?>" id='excluirCartao'><font color='red'>Remover</font></a>
			        	</h6>
			        </center>
		   		<?php else: ?>
		   			<div class="col-8">
						<h4 class="mb-0">
				            	Forma de pagamento
				        </h4>
			    	</div>
			    	<div class="col-4">
			    	 <center>
			        	<h6 class="mb-0">
			            	<a href="<?= base_url('adicionarPagamento')?>">Adicionar</a>
			        	</h6>
		        	</center>	
		   		<?php endif ?>
		   		
			    </div> 
			</div>
		</div>
	</div>

	<div class="card fixed">
		<div class="card-body">
				<?php if (isset($numeroCartao)): ?>
					<div class="row align-items-center">
						<button class="btn btn-danger col-12 btn-confimarReserva">Confirmar reserva</button>
					</div>
				<?php else: ?>
					<a href="<?= base_url('adicionarPagamento')?>">
						<div class="row align-items-center">
							<button class="btn btn-danger col-12">Adicionar cartão</button>
						</div>
					</a>
				<?php endif?>
		</div>
	</div>
		
	<?php else: ?>
		<hr>
		<p align="center"><img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_animais.jpg')?>" class='imagem-fundo'></p>
		<p align="center"> Parece que está sem algo!<br> Reserve um servico! </p>
	<?php endif ?> <br>



