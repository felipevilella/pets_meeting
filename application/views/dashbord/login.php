<!-- Header -->
<div class="header bg-gradient-orange py-7 py-lg-8">
</div>
<!-- Page content -->
<div class="container mt--8 pb-5">
  <div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-white shadow border-0">
<!--         <div class="card-header bg-transparent pb-5">
          <div class="btn-wrapper text-center">
           
          </div>
        </div> -->
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <small>Realize o seu login</small>
            <div class='erroLogin'></div>
          </div>
          <form role="form">
            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                </div>
                  <input type="email" name="email" class="form-control" id="email" placeholder="E-mail">
                  <div class="validation errror" id="emailRetorno"></div>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha">
              </div>
              <div class="validation errror"></div>
            </div>
            <div class="text-center">
              <button type="button" class="btn btn-warning col-12" id="Entrar">Entrar</button>
            </div>
          </form><hr>
            <div class="col-12">
              <a href="#" id='cadastro'><center><font color='orange'>Esqueci minha senha</font></buton></center></a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>