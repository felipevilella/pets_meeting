<?php if ($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS"): ?>
 <div class="container">
 	<h6 class="h3 d-inline-block mb-0">Cartão vacina</h6>
 	<p class="breadcrumb-item active" aria-current="page">Acompanhe as vacinas de <?= $cartaoVacina["nomeAnimal"];?> </p>
 	<div class="col-12">
 		<div class="row">
 			<div class="col-md-4">
 				<div class="card">
 					<div class="card-body">
 						<a href="#!">
 							<p align="center"> <img src="<?=$cartaoVacina['fotoAnimal'];?>" class="rounded-circle imagem-perfil-pets-perfil-estabelecimento"></p>
 						</a>
 						<div class="pt-4 text-center">
 							<h5 class="h3 title">
 								<span class="d-block mb-1"> <?= $cartaoVacina["nomeAnimal"];?></span>
 								<small class="h4 font-weight-light text-muted"><?= $cartaoVacina["racaAnimal"];?></small>
 							</h5>
 							<div class="h5 mt-12">
 								<hr>
 								<div class="col-auto">
 									<a href="#" class="avatar rounded-circle">
 										<img  src="<?=$cartaoVacina['fotoUsuario']?>" class='imagem-tipo-servico'>
 									</a>
 								</div> 
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 			<div class="col-8">
 				<div class="card widget-calendar">
 					<div class="card-header">
 						<div class="row align-items-center">
 						</div>
 						<div class="col-md-12">
 							<div class="row align-items-center">
 								<div class="col-4">
 									<button type="button" class="btn btn-sm btn-primary col-12" id="btn-agendarVacina"> Agendar </button>
 								</div>
 								<div class="col-4">
 									<button type="button" class="btn btn-sm btn-primary col-12" id='btn-vacina_pendente' data-vacina = "<?=$codigoCartao?>">
 										vacinas agendandas
 									</button>
 								</div>
 								<div class="col-4 text-right">
 									<button type="button" class="btn btn-sm btn-primary col-12" id='btn-historico_vacina' data-vacina = "<?=$codigoCartao?>">
 										Meu histórico
 									</button>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>

 				<div class="collapse div-agendamento-vacina">
 					<div class="card">
 						<div class="card-body">
 							<div class="row">
 								<div class="col-md-12">
 									<div class="form-group">
 										<label ><font color='Grey'>Vacina</font></label>
 										<select class="form-control" id="vacina">
 											<option value = "" > Selecione uma opção </option>
 											<?php foreach ($vacinas as $vacina): ?>
 												<option value = "<?=$vacina["codigoVacina"]?>" > <?=$vacina["nome"]?></option>
 											<?php endforeach ?>
 										</select>
 										<span id="errorVacina"  class="errror"></span>
 									</div>
 								</div>
 								<div class="col-md-6">
 									<label ><font color='Grey'>Valor</font></label>
 									<div class="form-group">
 										<input type="text" id="valor" class="form-control valor">
 										<span id="erroValor"  class="errror"></span>
 									</div>
 								</div>
 								<div class="col-md-6">
 									<div class="form-group">
 										<label class="form-control-label" for="exampleDatepicker">Selecione a data</label>
 										<input class="form-control" type="date"  id="dataVacina" value="">
 										<span id="erroData"  class="errror"></span>
 									</div>
 								</div>

 								<div class="col-md-12">
 									<label id="contador_caracter"><font color='Grey'>Horarios disponiveis</font></label><br>
 									<div class="row col-md-12 horariosVancinas">
 										<span id="alertaHorarioReservado"  class="errror">Selecione uma data</span>
 									</div> 
 								</div>
 							</div>
 							<br>
 							<button type="button" class="btn btn-default btn-agendar" data-codigoPetshop= '<?=$codigoPetshop?>'>Agendar</button>
            </div>
          </div>
        </div>

        <div class="card div-listaVacina">
          <div class="card-body">
           <div class="card-title" id="titulo-consulta"><h5> Vacinas Agendadas </h5></div>
           <div class="listaVacina">
            <?=$historicoVacinas?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else :?>
  <div class="header bg-gradient-white pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <br><h3 class="ct-title" id="content"><center>Cartão de vacina de <?= $cartaoVacina["nomeAnimal"];?></center></h3>
        <hr>
      </div>
    </div>
  </div>
  <div class="col-12">
    <div class="row">
      <div class="col-md-4">
          <div class="card-body">
            <a href="#!">
              <p align="center"> <img src="<?=$cartaoVacina['fotoAnimal'];?>" class="rounded-circle imagem-perfil-pets-perfil"></p>
            </a>
            <div class="pt-4 text-center">
              <h5 class="h3 title">
                <span class="d-block mb-1"> <?= $cartaoVacina["nomeAnimal"];?></span>
                <small class="h4 font-weight-light text-muted"><?= $cartaoVacina["racaAnimal"];?></small>
              </h5>
              <div class="h5 mt-12">
                <hr>
                <div class="col-auto">
                  <a href="#" class="avatar rounded-circle">
                    <img  src="<?=$cartaoVacina['fotoUsuario']?>" class='imagem-tipo-servico'>
                  </a>
                </div> 
              </div>
            </div>
          </div>
        </div>
      </div>
          <div class="card-header">
            <div class="row align-items-center">
            </div>
            <div class="col-md-12">
              <div class="row align-items-center">
                <div class="col-5">
                  <button type="button" class="btn btn-sm btn-white col-12" id="btn-agendarVacina"> Agendar </button>
                </div>
                <div class="col-7">
                  <button type="button" class="btn btn-sm btn-white col-12" id='btn-vacina_pendente' data-vacina = "<?=$codigoCartao?>">
                    vacinas agendandas
                  </button>
                </div>
                <div class="col-12"><br>
                  <button type="button" class="btn btn-sm btn-white col-12" id='btn-historico_vacina' data-vacina = "<?=$codigoCartao?>">
                    Meu histórico
                  </button>
                </div>
              </div>
            </div>
          </div>

        <div class="collapse div-agendamento-vacina">
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label ><font color='Grey'>Vacina</font></label>
                    <select class="form-control" id="vacina">
                      <option value = "" > Selecione uma opção </option>
                      <?php foreach ($vacinas as $vacina): ?>
                        <option value = "<?=$vacina["codigoVacina"]?>" > <?=$vacina["nome"]?></option>
                      <?php endforeach ?>
                    </select>
                    <span id="errorVacina"  class="errror"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <label ><font color='Grey'>Valor</font></label>
                  <div class="form-group">
                    <input type="text" id="valor" class="form-control valor">
                    <span id="erroValor"  class="errror"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="exampleDatepicker">Selecione a data</label>
                    <input class="form-control" type="date"  id="dataVacina" value="">
                    <span id="erroData"  class="errror"></span>
                  </div>
                </div>

                <div class="col-md-12">
                  <label id="contador_caracter"><font color='Grey'>Horarios disponiveis</font></label><br>
                  <div class="row col-md-12 horariosVancinas">
                    <span id="alertaHorarioReservado"  class="errror">Selecione uma data</span>
                  </div> 
                </div>
              </div>
              <br>
              <button type="button" class="btn btn-default btn-agendar" data-codigoPetshop= '<?=$codigoPetshop?>'>Agendar</button>
            </div>
          </div>

        <div class="div-listaVacina">
          <div class="card-body">
            <div class="card-title" id="titulo-consulta"><h5> Vacinas Agendadas </h5></div>
            <div class="listaVacina">
              <?=$historicoVacinas?>
            </div>
          </div>
        </div>
      </div>
  </div>
<?php endif?>

<div class="modal fade" id="modal-consultaVacina" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
    <div class="modal-content ">
     <div class="modal-header">
      <h5 class="modal-title" id="titulo">Informações da consulta</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="detalheConsultaVacina"> </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Voltar</button>

    </div>
  </div>
  
</div>
</div>


