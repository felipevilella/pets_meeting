<?php if ($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS"): ?>
  <div class="container">
    <div class="col-md-12">
      <h6 class="h3 d-inline-block mb-0">Meus atendimentos</h6>

      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <p class="breadcrumb-item active" aria-current="page">
           Acompanhe as consultas que foram marcadas pelos seus clientes.
         </p>
       </div>
     </div>
   </div>

   <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-md-4"> 
          <label>Escolha o dia </label>
          <div class="form-group">
            <select class="form-control" id="data">
              <?php foreach ($dias as $dia): ?>
                <option value ="<?=$dia['data']?>"> <?=$dia['dia']?></option>
              <?php endforeach ?>
              <option value =""> Ver todos</option>
            </select>
          </div>
        </div>
        <div class="col-md-4"> 
          <label>Status</label>
          <div class="form-group">
            <select class="form-control" id="situacao">
              <option value="">Selecione</option>
              <option value= "0" > A realizar </option>
              <option value="1"> Realizado</option>
              <option value="2">Pendente de cancelamento</option>
              <option value = "3" > Cancelado </option>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <label><br></label>
          <button type="button" class="btn btn-primary col-md-12" id="buscarAtendimento">Buscar</button>
        </div>
      </div>  
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-md-12"> 
          <div class="atendimentos">
            <div class="col-12">
              <p align="center">
                <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_consultas.jpg')?>" class='imagem-fundo-sem-atendimento'>
              </p>
              <p align="center"> Busque seus atendimentos de acordo com sua opção! </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php else: ?>
   <div class="header bg-gradient-white pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <br><h3 class="ct-title" id="content"><center>Meus atendimentos</center></h3><hr>
      </div>
    </div>
  </div>


  <div class="card-body">
    <div class="row">
      <div class="col-md-12"> 
        <div class="form-group">
          <select class="form-control" id="data">
            <option value = "" > Selecione um dia </option>
            <?php foreach ($dias as $dia): ?>
                <option value ="<?=$dia['data']?>"> <?=$dia['dia']?></option>
            <?php endforeach ?>
            <option value =""> Ver todos</option>
          </select>
          <select class="form-control" id="situacao">
            <option value="">Selecione um status</option>
            <option value= "0" > A realizar </option>
            <option value="1"> Realizado</option>
            <option value="2">Pendente de cancelamento</option>
            <option value = "3" > Cancelado </option>
          </select>
        </div>
      </div><br>
      <button type="button" class="btn btn-sm btn-white col-md-12" id="buscarAtendimento">Buscar</button>
    </div>  
  </div>
</div>

<div class="atendimentos">
  <div class="col-12">
    <p align="center">
      <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_consultas.jpg')?>" class='imagem-fundo-sem-consulta'>
    </p>
    <p align="center"> Busque seus atendimentos de acordo com sua opção! </p>
  </div>
</div>


<?php endif?>



<div class="modal fade" id="modal-atendimento" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
    <div class="modal-content ">
     <div class="modal-header">
      <h5 class="modal-title" id="titulo"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="detalheAtendimento"> </div>

    <div class="modal-footer">
      <button type="button" class="btn btn-primary salvarStatus"></button>
      <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Voltar</button>
    </div>
  </div>

</div>
</div>

</div>
