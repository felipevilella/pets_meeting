<?php if ($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS"): ?>
  <div class="container">
    <div class="col-md-12">
      <h6 class="h3 d-inline-block mb-0">Serviço</h6>

      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <p class="breadcrumb-item active" aria-current="page">
           Adicione os serviços que o seu estabelecimento oferece para os clientes.
         </p>
       </div>
       <div class="col-lg-6 col-5 text-right">
        <?php if ($sobre == 0): ?>
          <button type="button" class="btn btn-sm btn-neutral text-black" data-toggle="modal" data-target="#modal-cadastroServico" disabled>
            Adicionar
          </button>
          <?php else: ?>
            <button type="button" class="btn btn-sm btn-neutral text-black" data-toggle="modal" data-target="#modal-cadastroServico">
              Adicionar
            </button>
          <?php endif ?>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--12">
      <div class="row">
        <?php if ($servicos): ?>
          <?php foreach ($servicos as $servico): ?>
            <div class="card">
              <div class="card-body">
                <div class="row align-items-center">
                  <div class="col-auto">
                    <a href="#" class="avatar rounded-circle">
                      <img alt="Image placeholder" src="<?= $servico["imagem"];?>" class='imagem-tipo-servico'>
                    </a>
                  </div>
                  <div class="col ml--2">
                    <h4 class="mb-0">
                      <a href="#!"><?= $servico["nome"]?></a>
                    </h4>
                    <p class="text-sm text-muted mb-0" align="justify"><?= $servico["descricao"]?></p>
                  </div>
                  <div class="col-auto">
                    <button type="button" class="btn btn-sm btn-warning editarServico" data-servico='<?= $servico["idServico"];?>' >
                      Editar
                    </button>
                    <button type="button" class="btn btn-sm btn-danger excluirServico" data-servico='<?= $servico["idServico"];?>'>
                      Excluir
                    </button>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
          <?php else: ?> 
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <p align="center">
                    <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_servico.jpg')?>" class='imagem-fundo-sem-atendimento'>
                  </p>
                </div>
                <div class="col-12">
                  <p align="center"> 
                    Que pena! Você não possui serviços cadastrado! 
                  </p>
                </div>
              </div>
            </div>
          </div>
        <?php endif ?>
      </div>
      <?php else: ?>
       <div class="header bg-gradient-white pt-md-8">
        <div class="container-fluid">
          <div class="header-body">
            <br><h3 class="ct-title" id="content"><center>Meus serviços</center></h3>
            <button type="button" class="btn btn-sm btn-neutral text-black col-12" data-toggle="modal" data-target="#modal-cadastroServico">
              Adicionar
            </button>
            <hr>
          </div>
        </div>
      </div>
      <div class="container-fluid mt--12">
        <div class="row">
          <?php if ($servicos): ?>
            <?php foreach ($servicos as $servico): ?>
              <div class="card">
                <div class="card-body">
                  <div class="row align-items-center">
                    <div class="col-auto">
                      <a href="#" class="avatar rounded-circle">
                        <img alt="Image placeholder" src="<?= $servico["imagem"];?>" class='imagem-tipo-servico'>
                      </a>
                    </div>
                    <div class="col ml--2">
                      <h4 class="mb-0">
                        <a href="#!"><?= $servico["nome"]?></a>
                      </h4>
                    </div>
                    <div class="col-12">
                      <p class="text-sm text-muted mb-0" align="justify"><?= substr($servico["descricao"], 0, 120)?></p>
                    </div>

                    <div class="col-12"><br>
                      <div class="row">
                        <div class="col-6">
                          <button type="button" class="btn btn-sm btn-warning editarServico col-12" data-servico='<?= $servico["idServico"];?>' >
                            Editar
                          </button>
                        </div>
                        <div class="col-6">
                          <button type="button" class="btn btn-sm btn-danger excluirServico col-12" data-servico='<?= $servico["idServico"];?>'>
                            Excluir
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
            <?php else: ?> 
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <p align="center">
                      <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_servico.jpg')?>" class='imagem-fundo-sem-consulta'>
                    </p>
                  </div>
                  <div class="col-12">
                    <p align="center"> 
                      Que pena! Você não possui serviços cadastrado! 
                    </p>
                  </div>
                </div>
              </div>
            </div>
          <?php endif ?>
        </div>
      <?php endif?>



      <div class="modal fade" id="modal-cadastroServico" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <div class="modal-dialog modal-default  modal-dialog-centered modal-" role="document">
          <div class="modal-content bg-gradient-danger">

            <div class="modal-header">
              <h5 class="modal-title" id="modal-title-notification"><font color='white'>Adicione um serviço</font></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>

            <div class="modal-body">
              <div class="row">

                <div class="col-md-6"> 
                  <label> <font color='white'>Serviço</font></label>
                  <div class="form-group">
                    <select class="form-control tipoServico">
                      <option value = "" > Selecione uma opção </option>
                      <?php foreach ($tipoServicos as $servico): ?>
                       <option value="<?= $servico["idtipoServico"]?>" ><?= $servico["nome"]?></option>
                     <?php endforeach ?>
                   </select>
                   <span id="erroServico"  class="errorCampo"></span>
                 </div>
               </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label><font color='white'>Tempo estimado do serviço</font></label>
                  <input class="form-control" id="tempo" type="time">
                  <span id="erroTempo"  class="errorCampo"></span>
                </div>
              </div>

              <div class="col-md-6"> 
                <div class="form-group">
                  <label> <font color='white'>Nome</font></label>
                  <div class="nomeServico">
                    <input class="form-control tipoConsulta" placeholder="nome" type="text">
                  </div>
                  <span id="erroNome"  class="errorCampo"></span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label> <font color='white'>Preço</font></label>
                  <input class="form-control valor" id="preco" placeholder="00,00" type="text">
                  <span id="erroPreco"  class="errorCampo"></span>
                </div>
              </div>

              <div class="col-md-12"> 
                <div class="form-group">
                  <label> <font color='white'>Descrição</font></label>
                  <textarea id="descricao" class="form-control" heigth="50px" maxlength="176" placeholder="Descreva o seu servico"></textarea>
                  <span id="erroDescricao"  class="errorCampo"></span>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-link text-white ml-auto voltar">voltar</button>
              <button type="button" class="btn btn-white botaoModalServico"  id="salvarServico" >Salvar</button>
            </div>
          </div>
        </div>
      </div>></div>
