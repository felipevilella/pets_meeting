
<div class="header bg-gradient-warning pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <div>
          <p class="ct-lead"> 
           <font color = "white">
             <div class="validation errorCampo" id="erroCep"></div>
              <div class="form-group">
                <div class="row">
                <div class="input-group input-group-merge col-10">
                  <input class="form-control nome col-12" id='nome' placeholder="Qual o nome do seu funcionario?" type="text">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                  </div>

                </div>
                <div class="col-2">
                  <button class="btn btn-primary" type="button" id='cadastrarFuncionario' data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> Adicionar</button>
                </div>
                <div class="col-12">
                 <div class="errorCampo" id="errorNome"></div>
               </div>
              </div>
            </div>

            </font>
          </p>
        </div>
      </div>
    </div>
  </div>
    <div class="container-fluid mt--8">
      <p>
      <a class="btn btn-default hidden" data-toggle="collapse" href="#listaEndereco" role="button" aria-expanded="true" aria-controls="collapseExample"></a>
      <button class="btn btn-primary hidden" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"></button>
    </p>

    <div class="collapse show" id="listaEndereco">
      <div class="card card-body">
       <h3 class="card-title"> Meus Funcionarios </h3>
         <?php if (!$veterinarios): ?>
              <p align="center">
              <img src="<?php echo base_url('assets/personalizado/imagem/artes/funcionario.jpg');?>" width='50%'>
              <br> Nenhum funcionario encontrado, adicione um novo.</p>
           <?php else: ?>
            <?php foreach ($veterinarios as $veterinario): ?>
                <div class="col-md-12">
                    <div class="card-body">
                      <div class="row align-items-center">
                        <div class="col ml--2">
                          <h4 class="mb-0">
                           <?= $veterinario['nome']?>
                         </h4>
                         <p class="text-sm text-muted mb-0"></p>
                       </div>
                       <div class="col-auto"> 
                          <button type="button" class="btn btn-sm btn-warning btn-desativarVeterinario" data-veterinario="<?=$veterinario['idveterinario'];?>">
                            Excluir
                          </button>
                        </div>
                      </div>
                    </div>
                </div>
            <?php endforeach ?>
           </div>
         <?php endif ?>
      </div>
    </div>
