<?php if ($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS"): ?>
<div class="container">
  <div class="col-md-12">
    <h6 class="h3 d-inline-block mb-0">Vacinas</h6>
        <p class="breadcrumb-item active" aria-current="page">
          Acompanhe as vacinas dos seus clientes
        </p>
    </div>
  <div class="input-group mb-3">
    <input type="text" class="form-control" id="nome" placeholder="Qual é o nome do seu cliente?" aria-label="Recipient's username" aria-describedby="button-addon2">
    <div class="input-group-append">
      <button class="btn btn-outline-primary" type="button" id="btn-buscarCartaoVacina">Buscar</button>
    </div>
  </div>

  <div class="row col-md-12 listarCartao">
    <div class="card">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-12">
            <p align="center"><img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_vacinas.jpg')?>" class='imagem-fundo-sem-cartao'></p>
          </p>
        </div>
        <div class="col-12">
          <p align="center">Busque o nome do seu cliente para encontrar o animal de estimação.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
 <div class="header bg-gradient-white pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <br><h3 class="ct-title" id="content"><center>Vacinas</center></h3>
      <hr>
    </div>
  </div>
</div>
<div class="input-group mb-3">
    <input type="text" class="form-control" id="nome" placeholder="Qual é o nome do seu cliente?" aria-label="Recipient's username" aria-describedby="button-addon2">
    <div class="input-group-append">
      <button class="btn btn-outline-danger" type="button" id="btn-buscarCartaoVacina">Buscar</button>
    </div>
</div>

<div class="listarCartao">
    <div class="card ">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-12">
            <p align="center"><img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_vacinas.jpg')?>" class='imagem-fundo-sem-consulta'></p>
          </p>
        </div>
        <div class="col-12">
          <p align="center">Busque o nome do seu cliente para encontrar o animal de estimação.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif?>