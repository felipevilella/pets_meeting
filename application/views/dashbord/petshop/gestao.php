<?php if ($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS"): ?>
  <div class="container"><br>
    <h6 class="h3 d-inline-block mb-0">Gestão</h6>
    <p class="breadcrumb-item active" aria-current="page">Acompanhe em tempo real o resumo das suas atividades</p>
    <div class="row">
      <div class="col-md-3">
        <div class="card bg-gradient-white  border-0">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">    
                <h5 class="card-title text-uppercase text-muted mb-0 text-black">Serviços agendados</h5>
                <?php if ($TotalAgendamento): ?>
                 <span class="h2 font-weight-bold mb-0 text-black"><?=$TotalAgendamento?></span>
                 <?php else: ?>
                   <span class="h2 font-weight-bold mb-0 text-black">0</span>
                 <?php endif ?>
                 <div class="progress progress-xs mt-3 mb-0">
                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card bg-gradient-white  border-0">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-black">Fluxo de caixa</h5>
                <span class="h2 font-weight-bold mb-0 text-black">R$ <?=$fluxoCaixa?></span>
                <div class="progress progress-xs mt-3 mb-0">
                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card bg-gradient-white  border-0">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-black">A receber</h5>
                <span class="h2 font-weight-bold mb-0 text-black">R$ <?=$valorPagamento['transferencia']?></span>
                <div class="progress progress-xs mt-3 mb-0">
                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-md-3">
        <div class="card bg-gradient-white  border-0">
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0 text-black">Conta bancaria</h5>
                <?php if ($dadosBancarios): ?>
                  <span class="h2 font-weight-bold mb-0 text-black"> Ativo</span>
                  <div class="progress progress-xs mt-3 mb-0">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                  </div>
                  <?php else: ?>
                    <span class="h2 font-weight-bold mb-0 text-black"> Desativado</span>
                    <div class="progress progress-xs mt-3 mb-0"></div>
                  <?php endif ?>

                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-6">
          <div class="card-deck flex-column flex-xl-row">
            <div class="card">
              <div class="card-header">
                <a href="<?php echo base_url('atendimentos')?>">
                  <button type="button" class="btn btn-primary btn-sm">Abrir meus atendimentos</button>
                </a>
              </div>
              <div class="card-body p-0">
                <?php if ($agendamentos): ?>
                  <?php foreach ($agendamentos as $key => $agendamento): ?>
                    <?php 
                    if ($key < 4): ?>
                      <div class="col-12">
                        <div class="card-body">
                          <div class="row align-items-center">
                            <div class="col-auto">
                              <div class="avatar-group">
                                <a href="#" class="avatar avatar-sm rounded-circle" data-toggle="tooltip" data-original-title="<?=$agendamento["nomeUsuario"]?>">
                                  <img  src="<?=$agendamento["fotoPrincipal"]?>" class='imagem-tipo-atendimento'>
                                </a>
                                <a href="#" class="avatar avatar-sm rounded-circle" data-toggle="tooltip" data-original-title="<?=$agendamento["nomeAnimal"]?>">
                                  <img  src="<?=$agendamento["fotoPrincipalAnimal"]?> " class='imagem-tipo-atendimento'>
                                </a>
                              </div>
                            </div>
                            <div class="col ml--2">
                              <h4 class="mb-0">
                                <a href=""><?=$agendamento["nomeServico"]?></a>
                              </h4>
                              <p class="text-sm text-muted mb-0"><?=$agendamento["categoria"]?></p>
                              <small><?=$agendamento["data"]?> - <?=$agendamento["hora"]?></small>
                            </div>
                            <div class="col-auto">
                              <?=$agendamento["data"]?> as <?=$agendamento["hora"]?>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endif ?>   
                  <?php endforeach ?>
                  <?php else: ?>
                    <div class="col-12">
                      <div class="card-body">
                        <div class="col-12">
                          <p align="center">
                            <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_consultas.jpg')?>" class='imagem-fundo-sem-atendimento-estabelecimento'>
                          </p>
                        </div>
                        <div class="col-12">
                          <p align="center"> Você não possui antendimentos agendados</p>
                        </div>
                      </div>
                    </div>
                  <?php endif ?>
                  <hr>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
           <div class="col-md-12">
            <div class="accordion" id="accordionExample">
              <div class="card">
                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="mb-0">Meus dados bancarios</h4>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" >
                  <div class="card-body">
                    <div class="row">
                      <div class="col-6">
                        <label for="example-email-input" class="form-control-label">Agência</label>
                        <input type='text' name='Agencia' id='Agencia' class="form-control col-12" placeholder="Qual e o numero da sua agencia?" 
                        maxlength="4" value="<?= $dadosBancarios['agencia'] ? $dadosBancarios['agencia']: ''?>">
                        <div class="validation errror" id="erroAgencia"></div>
                      </div>
                      <div class="col-6">
                        <label for="example-email-input" class="form-control-label">Digito da agência</label>
                        <input type='text' name='digitoAgencia' id='digitoAgencia' class="form-control col-12"  placeholder="Qual e o digito da sua agencia?" maxlength="3"  value="<?= $dadosBancarios['agencia'] ? $dadosBancarios['digitoAgencia']: ''?>">
                        <div class="validation errror" id="erroDigitoAgencia"></div>
                      </div>
                      <div class="col-6">
                        <label for="example-email-input" class="form-control-label">Banco</label><br>
                        <select class="form-control" id='banco'>
                          <option value=""> selecione uma opcao</option>
                          <?php foreach ($bancos as $banco): ?>
                            <option value="<?= $banco['codigo'];?>" <?= $dadosBancarios['codigoBanco'] == $banco['codigo'] ? 'selected': ''?>>
                             <?= $banco['nome'];?></option>
                           <?php endforeach ?>
                         </select>
                         <div class="validation errror" id="erroDigitoBanco"></div>
                       </div>
                       <div class="col-6">
                        <label for="example-email-input" class="form-control-label">Numero conta</label>
                        <input type='text' name='conta' id='conta' class="form-control col-12"  placeholder="Qual e o numero da sua conta?" maxlength="9"
                        value="<?= $dadosBancarios['agencia'] ? $dadosBancarios['conta']: ''?>">
                        <div class="validation errror" id="erroNumeroConta"></div>
                      </div>
                      <div class="col-6">
                        <label for="example-email-input" class="form-control-label">Digito conta</label>
                        <input type='text' name='tipoConta' id='digitoConta' class="form-control col-12"  placeholder="Qual e o digito da sua conta?" maxlength="4" value="<?= $dadosBancarios['agencia'] ? $dadosBancarios['digitoConta']: ''?>">
                        <div class="validation errror" id="erroDigitoConta"></div>
                      </div>
                      <div class="col-6">
                        <label for="example-email-input" class="form-control-label">Tipo de conta</label><br>
                        <select class="form-control"  data-live-search="true" id='tipoConta'>
                          <option value=""> selecione uma opcao</option>
                          <option value="1" <?= $dadosBancarios['tipoConta'] == '1' ? 'selected': ''?>> Conta corrente</option>
                          <option value="2" <?= $dadosBancarios['tipoConta'] == '2' ? 'selected': ''?>> Conta poupança</option>
                        </select>
                        <div class="validation errror" id="erroTipoConta"></div>
                      </div>

                      <div class="col-12"><br>
                        <button class="col-12 btn btn-warning" id="salvarDadosBancarios">Salvar</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="accordion" id="meuEndereco">
              <div class="card">
                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  <h4 class="mb-0">Meus endereço</h4>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="collapseTwo" data-parent="#meuEndereco" >
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12"> 
                        <label for="example-email-input" class="form-control-label">CEP</label>
                        <div class="input-group input-group-merge">
                          <input class="form-control cep" placeholder="Digite o seu CEP" type="text" <?= $enderecos? 'disabled':''?>>
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                          </div>
                        </div>
                        <div class="collapse" id="inserirEndereco">
                          <label for="example-email-input" class="form-control-label">Rua</label>
                          <input type='text' name='rua' id='rua' class="form-control col-12">
                          <div class="validation errror" id="erroRua"></div>

                          <label for="example-email-input" class="form-control-label">Bairro</label>
                          <input type='text' name='bairro' id='bairro' class="form-control col-12">
                          <div class="validation errror" id="erroBairro"></div>


                          <label for="example-email-input" class="form-control-label">Numero</label>
                          <input type='text' name='numero' id='numero' class="form-control col-12"><br>
                          <button class="col-12 btn btn-warning" id="salvarEndereco">Salvar</button>

                        </div>
                      </div>
                    </div>
                    <?php if (!$enderecos): ?>
                      <div class="collapse show div-endereco">
                        <p align="center">
                          <img src="<?php echo base_url('assets/personalizado/imagem/artes/sem_endereco.jpg');?>" width='50%'>
                          <br> Nenhum endereço encontrado, digite o seu CEP para adicionar um novo.</p>
                          <?php else: ?>
                            <?php foreach ($enderecos as $endereco): ?>
                              <div class="col-md-12 collapse show div-endereco">
                                <div class="card-body">
                                  <div class="row align-items-center">
                                    <div class="col ml--2">
                                      <h4 class="mb-0">
                                       <?= $endereco["rua"]?> <?= $endereco["numero"]?>, <?= $endereco["bairro"]?>
                                     </h4>
                                     <p class="text-sm text-muted mb-0"></p>
                                     <small><?=$endereco["cep"];?></small>
                                   </div>
                                   <div class="col-auto">
                                    <a href="#">
                                      <button type="button" class="btn btn-sm btn-primar btn-alteraEndereco" data_codigo="<?= $endereco['idEndereco'] ?>"
                                        data_rua="<?= $endereco['rua'] ?>" data_numero="<?= $endereco['numero'] ?>" data_bairro="<?= $endereco['bairro'] ?>" data_cep="<?= $endereco['cep'] ?>">
                                        alterar
                                      </button>
                                    </a>      
                                    <button type="button" class="btn btn-sm btn-warning btn-excluirEndereco" data_codigo="<?=$endereco['idEndereco'];?>">
                                      Excluir
                                    </button>
                                  </div>
                                </div>
                              <?php endforeach ?>
                            </div>
                          <?php endif ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
               <div class="card">
                <div class="card-header border-0">
                  <div class="row align-items-center">
                    <div class="col">
                      <h3 class="mb-0">Meus funcionarios</h3>
                    </div>
                    <div class="col text-right">
                      <button class="btn btn-sm btn-primary adicionar-funcionario">Adicionar</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 collapse  div-funcionario">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" id='nome' placeholder="Qual o nome do seu funcionario?" aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-outline-primary cadastrarFuncionario" type="button">Adicionar</button>
                    </div>
                  </div>
                </div>

                <div class="table-responsive collapse listar-funcionario show">
                  <?php if (!$veterinarios): ?>
                    <p align="center">
                      <img src="<?php echo base_url('assets/personalizado/imagem/artes/funcionario.jpg');?>" width='50%'>
                      <br> Nenhum funcionario encontrado, adicione um novo.</p>
                      <?php else: ?>
                        <table class="table align-items-center table-flush">
                          <thead class="thead-light">
                            <tr>
                              <th scope="col-6">Nome</th>
                              <th scope="col-6">Opção</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($veterinarios as $veterinario): ?>
                              <tr>
                                <th scope="row">
                                  <?= $veterinario['nome']?>
                                </th>
                                <td>
                                  <button type="button" class="btn btn-sm btn-warning btn-desativarVeterinario col-12" data-veterinario="<?=$veterinario['idveterinario'];?>">
                                    Excluir
                                  </button>
                                </td>
                              </tr>
                            <?php endforeach ?>         
                          </tbody>
                        </table>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php else: ?>
          <div class="header bg-gradient-white pt-md-8">
            <div class="container-fluid">
              <div class="header-body">
                 <br> <h3 class="ct-title" id="content"><center>Gestão</center></h3><hr>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card bg-gradient-white  border-0">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">    
                    <h5 class="card-title text-uppercase text-muted mb-0 text-black">Serviços agendados</h5>
                    <?php if ($TotalAgendamento): ?>
                     <span class="h2 font-weight-bold mb-0 text-black"><?=$TotalAgendamento?></span>
                     <?php else: ?>
                       <span class="h2 font-weight-bold mb-0 text-black">0</span>
                     <?php endif ?>
                     <div class="progress progress-xs mt-3 mb-0">
                      <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card bg-gradient-white  border-0">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-black">Fluxo de caixa</h5>
                    <span class="h2 font-weight-bold mb-0 text-black">R$ <?=$fluxoCaixa?></span>
                    <div class="progress progress-xs mt-3 mb-0">
                      <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card bg-gradient-white  border-0">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-black">A receber</h5>
                    <span class="h2 font-weight-bold mb-0 text-black">R$ <?=$valorPagamento['transferencia']?></span>
                    <div class="progress progress-xs mt-3 mb-0">
                      <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="card bg-gradient-white  border-0">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0 text-black">Conta bancaria</h5>
                    <?php if ($dadosBancarios): ?>
                      <span class="h2 font-weight-bold mb-0 text-black"> Ativo</span>
                      <div class="progress progress-xs mt-3 mb-0">
                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                      </div>
                      <?php else: ?>
                        <span class="h2 font-weight-bold mb-0 text-black"> Desativado</span>
                        <div class="progress progress-xs mt-3 mb-0"></div>
                      <?php endif ?>

                    </div>
                  </div>
                </div>
              </div>
            <?php endif ?>

