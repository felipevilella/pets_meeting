<?php foreach  ($dadosConversa as $key => $conversa): ?>
 <?php

 $dateTime = explode(" ", $conversa["data"]);
 $dateTime[0] = explode('-', $dateTime[0]);
 $data = strtotime($dateTime[0][2]."-".$dateTime[0][1]."-".$dateTime[0][0]);
 
 $dataAtual = strtotime(date("d-m-Y"));
 $dataHistorico = ($dataAtual-$data)/86400;

 if($dataHistorico == "1") {
    $data = "ontem as";
 } else if($dataHistorico == "0 ") {
    $data = "hoje as";
 } else {
   $data = $dateTime[0][2]."/".$dateTime[0][1]."/".$dateTime[0][0]." as ";
 }


 if($conversa["fk_idUsuario"] == $idamigo && $conversa["idDestinatario"] == $idusuario ){ ?> 
  <div class="message w3layouts">
    <?php
    if (empty($dadosamigo["fotoPrincipal"])) {
      echo "<img class='img' src=".base_url('assets/personalizado/imagem/avatar.jpg').">";
    } else {
      echo "<img class='img' src='".base_url("assets/personalizado/foto_usuario/".$dadosamigo["fotoPrincipal"])."'>";
    }
    ?>
    <div class="bubble">
     <?php echo $conversa["chatMensagem"];?>
     <span><?php echo $dateTime[1];?></span>
   </div>
 </div>
<?php } ?> 

<?php if($conversa["fk_idUsuario"] == $idusuario && $conversa["idDestinatario"] == $idamigo){ ?> 
  <div class="message right agileits">
     <?php
    if (empty($dadosuser["fotoPrincipal"])) {
      echo "<img class='img' src=".base_url('assets/personalizado/imagem/avatar.jpg').">";
    } else {
      echo "<img class='img' src='".base_url("assets/personalizado/foto_usuario/".$dadosuser["fotoPrincipal"])."'>";
    }
    ?>
    <div class="bubble">
      <?php echo $conversa["chatMensagem"];?>
      <span><?php echo $dateTime[1];?></span>
    </div>
  </div>
<?php } ?>
<?php endforeach ?>
<label id="retorno"></label>