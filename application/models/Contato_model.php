<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato_Model extends CI_Model {

	function __construct() { 
		$this->load->database();
	}

	public function getContatoPorIdPetShop($idPetShop, $tipoContato = false) {
		$this->db->where("ativo",'1');

		if ($tipoContato) {
			$this->db->where("fk_tipoContato", $tipoContato);
		}

		$this->db->where("fk_idPetshop", $idPetShop);		
		return $this->db->get("contato")->row_array();
	}

	public function getContatoUsuario($idUsuario) {
		$this->db->where("ativo",'1');
		$this->db->where("fk_idUsuario",$idUsuario);		
		return $this->db->get("contato")->row_array();
	}

	public function insertContato($dados) {
		$this->db->insert("contato", $dados);
	}
	
	public function updateContatoPorIdPetShop($dados, $idPetShop) {
		$this->db->where("fk_idPetshop",$idPetShop);
		$this->db->update("contato", $dados);
	}

	public function updateContatoPorUsuario($dados, $idUsuario) {
		$this->db->where("fk_idUsuario",$idUsuario);
		$this->db->update("contato", $dados);	
	}
}