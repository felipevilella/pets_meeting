<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrinho_model extends CI_Model {

	function setCarrinho($dados) {
		$this->db->insert('carrinho', $dados);
	}

	function getCarrinhoPorIdUsuario($idUsuario) {
		$this->db->select("servico.*,animal.nome as nomeAnimal,carrinho.*,reserva_servico.*,petshop.nome as nomePetshop");
		$this->db->join("reserva_servico", "fk_idReserva = idReservaServico");
		$this->db->join("animal", "fk_idAnimal = idanimais");
		$this->db->join("servico", "fk_idServico = idServico");
		$this->db->join("petshop", "fk_idPetshop = idPetshop");
		$this->db->where("carrinho.fk_idUsuario", $idUsuario);

		return $this->db->get('carrinho')->result_array();
	}

	function getprodutoPorid($idCarrinho) {
		$this->db->where("idCarrinho", $idCarrinho);
		return $this->db->get('carrinho')->row_array();
	}

	function deletarProduto($idCarrinho) {
		$this->db->delete('carrinho', array('idCarrinho' => $idCarrinho));
	}

	function getValorPorcentagemPorProduto($valor, $idUsuario) {
		$this->db->select("round(((((servico.preco)*90/100)*100)/$valor),2) as valorPorcetagem");
		$this->db->join("servico", "fk_idServico = idServico");
		$this->db->where("fk_idUsuario", $idUsuario);
		$this->db->where("preReserva", '1');
		return $this->db->get('reserva_servico')->result_array();
	}
}
