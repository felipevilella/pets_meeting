<?php

class Banco_model extends CI_Model {

	function __construct(){
		$this->load->database();
	}

	public function getBancos() {
		$this->db->where("ativo", '1');
		$this->db->order_by('nome','asc');
		return $this->db->get("banco")->result_array();
	}

	public function buscarDadosBancarios($idPetshop) {
		$this->db->where('fk_idPetshop', $idPetshop);
		return $this->db->get("informacao_transferencia")->row_array();
	}

	public function setDadosBancario($dados) {
		$this->db->insert("informacao_transferencia", $dados);
	}

	public function geidRecebedorPorIdPetshop($idPetshop) {
		$this->db->select('idRecebedor');
		$this->db->where("fk_idPetshop", $idPetshop);
		$recebedor =  $this->db->get("informacao_transferencia")->row_array();

		return $recebedor['idRecebedor'];
	}

	public function updateDadosBancarios($dados, $idPetshop) {
		$this->db->where("fk_idPetshop", $idPetshop);
		$this->db->update("informacao_transferencia", $dados);
	}

	public function setCartao($dados) {
		$this->db->insert("cartao", $dados);
	}

	public function excluirCartao($codigoCartao) {
		$this->db->delete('cartao', array('idcartao' => $codigoCartao));
	}
	

	public function getCartaoPorIdUsuario($idUsuario) {
		$this->db->where("fk_idUsuario", $idUsuario);
		return $this->db->get("cartao")->row_array();
	}
}