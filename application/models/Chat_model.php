<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_model extends CI_Model{

	function __construct() { 
		$this->load->database();
	}

	public function enviar_mensagem($dados) {
		$this->db->insert("mensagem",$dados);
	}

	public function criarConversa($dados) {
		$this->db->insert("conversa",$dados);
	}
	public function criarNotificacao($dados) {
		$this->db->insert("notificacao",$dados);
	}
	public function buscarUltimaMensagem($mensagem,$id_destinatario){
		$this->db->where("chatMensagem",$mensagem);
		$this->db->where("id_destinatario",$id_destinatario);
		return $this->db->get("mensagem")->row_array();
	}
	public function buscarMensagem($fk_idUsuario,$id_destinatario){
		$this->db->select('idDestinatario, chatMensagem, data, fk_idUsuario');
		$this->db->where("idDestinatario", $id_destinatario);
		$this->db->where("fk_idUsuario", $fk_idUsuario);
		$this->db->or_where("idDestinatario", $fk_idUsuario);
		$this->db->where("fk_idUsuario", $id_destinatario);
		$this->db->where("ativo",1);
		$this->db->order_by("data","asc");
		return $this->db->get("mensagem")->result_array();
	}
	
	public function buscarConversa($id_destinatario){
		$this->db->where('id_destinatario', $id_destinatario);
		$this->db->order_by('data','desc');
		return $this->db->get("conversa")->result_array();
	}
	public function buscarUnicaConversa($id_destinatario, $fk_idUsuario){
		$this->db->where('id_destinatario', $id_destinatario);
		$this->db->where('fk_idUsuario',$fk_idUsuario);
		return $this->db->get("conversa")->row_array();
	}

	
	public function mostrarContadorDeMensagem($idusuario){
		$this->db->select_sum("ativo");
		$this->db->where("fk_idUsuario",$idusuario);
		return $this->db->get("notificacao")->row_array();
		
	}

	public function atualizarConversa($idConversa, $dados) {
		$this->db->where('id_conversa', $idConversa);
		$this->db->set('data', 'now()', false);
		$this->db->update('conversa');
	}


}
