<?php
class Notificacao_model extends CI_Model{

	function __construct(){
		$this->load->database();
	}

	public function buscarNotificacao($idusuario){
		$this->db->where("fk_idUsuario",$idusuario);
		$this->db->where("ativo",1);
		$this->db->group_by("fk_idUsuario");
		$this->db->order_by("idnotificacao","desc");
		return $this->db->get("notificacao")->result_array();
	}
	public function mostrarMensagemNotificacao($idusuario,$id_destinatario,$codigoTotal){
		$this->db->select("mensagem.chatMensagem,conversa.id_destinatario,conversa.fk_idUsuario,mensagem.codigofinal,mensagem.foto");
		$this->db->where("codigofinal",$codigoTotal);
		$this->db->join("mensagem","mensagem.idMensagem = conversa.Fk_idMensagem");
		$this->db->where("conversa.id_destinatario",$idusuario);
		$this->db->where("conversa.fk_idUsuario",$id_destinatario);
		$this->db->group_by("conversa.id_destinatario");
		$this->db->order_by("conversa.id_conversa","asc");
		return $this->db->get("conversa")->row_array();
	}
	public function atualizarNotificacao($dados, $idUsuario, $idDestinatario) {
		$this->db->where("fk_idUsuario",$idUsuario);
		$this->db->where("idDestinatario",$idDestinatario);
		$this->db->update("notificacao",$dados);	
	}

	public function getNotificacaoPorIdPetshop($idPetshop) {
		$this->db->where("fk_idPetshop", $idPetshop);
		$this->db->where("leitura", '1');
		return $this->db->get("notificacao_servicos")->result_array();
	}

	public function atualizarNotificacaoPorIdAgendamento($idAgendamento, $dados) {
		$this->db->where("fk_idAgendamento",$idAgendamento);
		$this->db->update("notificacao_servicos",$dados);	
	}
}