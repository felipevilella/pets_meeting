<?php

class Informacao_model extends CI_Model {

	function __construct(){
		$this->load->database();
	}

	public function getSobrePorIdUsuario($idUsuario) {
		$this->db->where("fk_idUsuarioPetshop", $idUsuario);
		return $this->db->get("petshop")->row_array();
	}

	public function setSobre($dados) {
		$this->db->insert("petshop", $dados);
	}

	public function updateSobrePorIdUsuario($dados, $idUsuario) {
		$this->db->where("fk_idUsuarioPetshop", $idUsuario);
		$this->db->update("petshop", $dados);
	}

	public function getPetShopPorCodigoCidade($idCidade, $idEstado = false) {
		$this->db->select("petshop.idpetshop as codigoPetshop, petshop.nome, petshop.descricao, petshop.horarioAbertura, petshop.horarioFechamento, petshop.endereco, petshop.cnpj,petshop.razaoSocial, localizacao.latitude, localizacao.longitude, cidade.nome as nomeCidade,
			estado.nome as nomeEstado, usuario.fotoPrincipal");
		$this->db->join("usuario", "fk_idUsuarioPetshop = idUsuario");
		$this->db->join("cidade", "usuario.fk_idCidade = idCidade");
		$this->db->join("estado", "usuario.fk_idEstado = idEstado");
		$this->db->join("localizacao","fk_idUsuario = idUsuario ");
		$this->db->where("petshop.ativo", "1");
		$this->db->where("usuario.fk_idCidade", $idCidade);
		
		if ($idEstado) {
			$this->db->where("usuario.fk_idEstado", $idEstado);
		}

		return $this->db->get("petshop")->result_array();
	}

	public function setVeterinario($dados) {
		$this->db->insert("veterinario", $dados);
	}

	public function getVeterinariosPoridPetshop($idPetShop) {
		$this->db->where("fk_idPetShop", $idPetShop);	
		$this->db->where("ativo", "1");
		return $this->db->get("veterinario")->result_array();
	}

	public function getVeterinarioPorId($idVeterinario) {
		$this->db->select("idveterinario as codigoVeterinario, nome, ra, fk_idPetshop as codigoPetshop");
		$this->db->where("idVeterinario", $idVeterinario);
		$this->Db->where("ativo", "1");
		return $this->db->get("veterinario")->row_array();	
	}

	public function deletarFuncionario($idVeterinario) {
		$this->db->delete("veterinario", array('idveterinario' => $idVeterinario));	
	}

	public function updateFuncionario($dados, $idVeterinario) {
		$this->db->where("idveterinario", $idVeterinario);
		$this->db->update("veterinario", $dados);
	}

	public function getPetShopPoridPetShop($idPetShop) {
		$this->db->select("petshop.idpetshop as codigoPetshop, petshop.nome, petshop.descricao, petshop.horarioAbertura, petshop.horarioFechamento, petshop.endereco, localizacao.latitude, localizacao.longitude, cidade.nome as nomeCidade,
			estado.nome as nomeEstado, usuario.fotoPrincipal, contato.descricao as contato");
		$this->db->join("usuario", "fk_idUsuarioPetshop = idUsuario");
		$this->db->join("cidade", "usuario.fk_idCidade = idCidade");
		$this->db->join("estado", "usuario.fk_idEstado = idEstado");
		$this->db->join("localizacao","fk_idUsuario = idUsuario ");
		$this->db->join("contato","idPetShop = fk_idPetshop ");
		$this->db->where("petshop.ativo", "1");
		$this->db->where("petshop.idPetShop", $idPetShop);
		$this->db->where("contato.fk_tipoContato", '1');

		return $this->db->get("petshop")->row_array();
	}

}