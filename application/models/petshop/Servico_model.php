<?php

class Servico_model extends CI_Model{

	function __construct(){
		$this->load->database();
	}

	function getTipoServico() {
		$this->db->where("ativo", 1);
		$this->db->order_by("nome", "asc");
		return $this->db->get("tipo_servico")->result_array();
	}

	function getServicoPetShopPorId($idPetshop, $vacina = false) {
		$this->db->select("servico.*, tipo_servico.nome as categoria");
		$this->db->join("tipo_servico","fk_idtipoServico = idtipoServico");
		$this->db->where("servico.ativo", 1);
		$this->db->where("fk_idPetshop", $idPetshop);
		if (!$vacina) {
			$this->db->where("fk_idTipoServico != 5");
		}
		$this->db->order_by("nome", "asc");
		$this->db->order_by('idServico', 'desc');
		return $this->db->get("servico")->result_array();
	}

	function getServicoPoridServico($idServico) {
		$this->db->where('idServico', $idServico);
		return $this->db->get('servico')->row_array();
	}


	function getServicoPetShopPorIdTipoServico($idtipoServico) {
		$this->db->select("servico.idServico, tipo_servico.nome as categoria, petshop.nome as nomePetshop, petshop.idpetshop as codigoPetshop,
			localizacao.latitude, localizacao.longitude, usuario.fotoPrincipal");
		$this->db->join("tipo_servico","fk_idtipoServico = idtipoServico");
		$this->db->join("petshop", "fk_idPetshop = idpetshop");
		$this->db->join("usuario", "fk_idUsuarioPetshop = idUsuario");
		$this->db->join("localizacao","fk_idUsuario = idUsuario ");
		$this->db->where("servico.ativo", 1);
		$this->db->where("fk_idTipoServico", $idtipoServico);
		$this->db->order_by("petshop.nome", "asc");
		$this->db->order_by('idServico', 'desc');
		$this->db->limit(5);
		return $this->db->get("servico")->result_array();
	}

	function getConsultaPetShopPorId($idServico) {
		$this->db->where("idServico", $idServico);
		return $this->db->get("servico")->row_array();
	}

	function getHorarioConsultaPorIdServico($idServico) {
		$this->db->select("horario_servico.*, horario.hora");
		$this->db->join("horario","fk_idHorario = idHorario");
		$this->db->where("fk_idServico", $idServico);
		return $this->db->get("horario_servico")->result_array();
	}

	function getServicoReservadoPorIdServico($idVeterinario, $data) {
		$this->db->select("reserva_servico.*, servico.tempo");
		$this->db->join("servico", "fk_idServico = idServico");
		$this->db->where("fk_idVeterinario", $idVeterinario);
		$this->db->where("data", $data);
		return $this->db->get("reserva_servico")->result_array();
	}

	function updateServicoReservado($dados, $idReservaServico) {
		$this->db->where("idReservaServico", $idReservaServico);
		$this->db->update("reserva_servico",$dados);
	}

	function getServicoIdReservaServico($idReservaServico) {
		$this->db->select("servico.nome, servico.fk_idPetshop as codigoPetshop, servico.preco, servico.descricao, reserva_servico.token_transacao");
		$this->db->join("servico","fk_idServico = idServico");
		$this->db->where("idReservaServico", $idReservaServico);
		return $this->db->get("reserva_servico")->row_array();
	}

	function getClienteServicoPorIdPetShop($idPetshop) {
		$this->db->select("usuario.*");
		$this->db->join("usuario", "fk_idUsuario = idUsuario");
		$this->db->where("fk_idPetshop", $idPetshop);
		return $this->db->get("cliente")->result_array();
	}

	function getServicosReservadoPorIdPetShop($idPetshop, $todos = true) {
		$this->db->select("servico.nome as nomeServico, servico.idServico as codigoServico, servico.fk_idTipoServico,
			servico.preco,tipo_servico.nome as categoria, reserva_servico.data, reserva_servico.situacao, usuario.nome as nomeUsuario, 
			usuario.idUsuario, usuario.fotoPrincipal, animal.idanimais as codigoAnimal, animal.nome as nomeAnimal,
			animal.fotoPrincipal as fotoPrincipalAnimal, reserva_servico.hora");

		$this->db->join("servico","fk_idServico = idServico");
		$this->db->join("tipo_servico","fk_idTipoServico = idtipoServico");
		$this->db->join("usuario","fk_idUsuario = idUsuario");
		$this->db->join("animal", "fk_idAnimal = idAnimais");
		
		if (!$todos) {
			$this->db->where("reserva_servico.preReserva", '0');
		}

		$this->db->where("servico.fk_idPetshop", $idPetshop);
		$this->db->order_by("data", 'desc');

		return $this->db->get("reserva_servico")->result_array();
	}

	function getServicosReservadoPorid($idReservaServico) {
		$this->db->select("servico.nome as nomeServico, servico.idServico as codigoServico, servico.fk_idTipoServico,
			servico.preco,tipo_servico.nome as categoria, reserva_servico.data, reserva_servico.situacao,reserva_servico.idReservaServico,
			usuario.nome as nomeUsuario, usuario.idUsuario, usuario.fotoPrincipal, animal.idanimais as codigoAnimal,
			animal.nome as nomeAnimal, animal.fotoPrincipal as fotoPrincipalAnimal, reserva_servico.hora");

		$this->db->join("servico","fk_idServico = idServico");
		$this->db->join("tipo_servico","fk_idTipoServico = idtipoServico");
		$this->db->join("usuario","fk_idUsuario = idUsuario");
		$this->db->join("animal", "fk_idAnimal = idAnimais");
		$this->db->where("idReservaServico", $idReservaServico);

		return $this->db->get("reserva_servico")->row_array();
	}

	function getServicosReservadoPorSituacaoData($idPetshop, $data, $situcao) {
		$dataAtual = date("Y/m/d");

		$this->db->select("servico.nome as nomeServico, servico.idServico as codigoServico, servico.fk_idTipoServico,
			servico.preco,tipo_servico.nome as categoria, reserva_servico.data, reserva_servico.idReservaServico,
			usuario.nome as nomeUsuario, 
			usuario.idUsuario, usuario.fotoPrincipal, animal.idanimais as codigoAnimal, animal.nome as nomeAnimal,
			animal.fotoPrincipal as fotoPrincipalAnimal, reserva_servico.hora");

		$this->db->join("servico","fk_idServico = idServico");
		$this->db->join("tipo_servico","fk_idTipoServico = idtipoServico");
		$this->db->join("usuario","fk_idUsuario = idUsuario");
		$this->db->join("animal", "fk_idAnimal = idAnimais");
		$this->db->where("servico.fk_idPetshop", $idPetshop);

		if ($data !="") {
			if ((strtotime($dataAtual) == (strtotime($data)))) {
				$this->db->where('data', $data);
			}  else {
				$this->db->where("data", $data);
			}
		} 

		if ($situcao >= 0 && $situcao!="") {
			$this->db->where('situacao', $situcao);
		}


		$this->db->order_by("data", 'asc');
		$this->db->order_by("hora", 'asc');
		return $this->db->get("reserva_servico")->result_array();
	}

	function insertNotificacaoServico($dados) {
		$this->db->insert("notificacao_servicos", $dados);
	}

	function setServicoReservado($dados) {
		$this->db->insert("reserva_servico", $dados);
		return $this->db->insert_id();
	}

	function deletarServicoReservado($idReservaServico) {
		$this->db->delete("reserva_servico", array('idReservaServico' => $idReservaServico));	
	}


	function updateReservaServico($idUsuario, $dados) {
		$this->db->where("fk_idUsuario", $idUsuario);
		$this->db->where("preReserva", '1');
		$this->db->update("reserva_servico", $dados);
	}

	public function getComentarioAvaliacaoServicoPorIdPetShop($idpetshop) {
		$this->db->select('comentario, nota, usuario.nome');
		$this->db->where('fk_idPetshop', $idpetshop);
		$this->db->where('comentario is not null');
		$this->db->where('comentario !=', '');
		$this->db->join("usuario", "fk_idUsuario = idUsuario");
		return $this->db->get("avaliacao_servico")->result_array();
	}

	public function getNotaAvaliacaoServicoPorIdPetShop($idpetshop) {
		$this->db->select('round(sum(nota) / count(nota),2) as total');
		$this->db->where('avaliacao_servico.fk_idPetshop', $idpetshop);
		$this->db->where('avaliacao_servico.nota is not null');
		return $this->db->get("avaliacao_servico")->row_array();
	}


	function getServidoReservadoPorIdAnimal($idAnimal, $situacao) {
		$this->db->select("reserva_servico.*, servico.nome as nomeServico, servico.preco,
			tipo_servico.nome as categoria, servico.fk_idTipoServico, petshop.nome as nomePetshop,
			petshop.idpetshop");
		$this->db->join("servico", "fk_idServico = idServico");
		$this->db->join("tipo_servico","servico.fk_idTipoServico = idtipoServico");
		$this->db->join("petshop","servico.fk_idPetshop = idPetshop");
		$this->db->where("fk_idAnimal", $idAnimal);
		$this->db->where_in("situacao", $situacao);
		$this->db->where_in("preReserva", '0');
		$this->db->order_by("data", 'desc');
		
		return $this->db->get("reserva_servico")->result_array();
	}

	function getTipoConsulta() {
		$this->db->where("ativo", 1);
		$this->db->order_by("nome", "asc");
		return $this->db->get("tipo_consulta")->result_array();
	}

	function salvarServicoPetshop($dados, $horarios = false) {
		$this->db->insert("servico", $dados);
	}

	function updateServicoPorId($dados, $idServico) {
		$this->db->where("idServico", $idServico);
		$this->db->update("servico", $dados);
	}

	function updateHorarioVeterinarioPorId($dados, $idVeterinario) {
		$this->db->where("fk_idVeterinario", $idVeterinario);
		$this->db->update("horario_servico", $dados);
	}

	function deletarHorarioServicosPorIdVeterinario($idVeterinario) {
		$this->db->where('fk_idVeterinario', $idVeterinario);
		$this->db->delete('horario_servico');
	}

	function inserirHorarioServico($horarios) {
		foreach ($horarios as $horario) {
			$this->db->insert("horario_servico", $horario);
		}
	
	}

}