<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Localizacao_model extends CI_Model {
	
	function __construct(){
		$this->load->database();
	}
	public function buscarEstado(){
		$this->db->where("ativo","1");
		return $this->db->get("estado")->result_array();
	}

	public function buscarCidade(){
		$this->db->where("ativo","1");
		return $this->db->get("cidade")->result_array();
	}

	public function getbuscarCidadePorEstado($idEstado){
		$this->db->where("fk_idestado",$idEstado);
		$this->db->where("ativo","1");
		return $this->db->get("cidade")->result_array();
	}

	public function getEnderecosPorIdUsuario($idUsuario, $naoPrincipal = false) {
		$this->db->where("fk_idUsuario",$idUsuario);
		$this->db->where("ativo","1");
		
		if($naoPrincipal) {
			$this->db->where("principal","0");
		}

		$this->db->order_by('principal', 'desc');
		return $this->db->get("endereco")->result_array();
	}


	public function getEnderecoPorIdUsuario($idUsuario) {
		$this->db->select('endereco.*, estado.sigla as sigla, cidade.nome as nomeCidade');
		$this->db->where("fk_idUsuario",$idUsuario);
		$this->db->where("endereco.ativo","1");
		$this->db->join('cidade','endereco.fk_idCidade = idCidade');
		$this->db->join('estado','endereco.fk_idEstado = idEstado');
		$this->db->order_by('principal', 'desc');
		return $this->db->get("endereco")->row_array();
	}

	public function getEnderecoPorId($idEndereco) {
		$this->db->where("idEndereco",$idEndereco);
		$this->db->where("ativo","1");
		return $this->db->get("endereco")->row_array();
	}

	public function deletarEndereco($idEndereco) {
		$this->db->delete('endereco', array('idEndereco' => $idEndereco));
	}
	
	public function atualizarEndereco($idEndereco, $dados) {
		$this->db->where('idEndereco', $idEndereco);
		$this->db->update('endereco', $dados);
	}

	public function atualizarEnderecoPorIdUsuario($idUsuario, $dados) {
		$this->db->where('fk_idUsuario', $idUsuario);
		$this->db->update('endereco', $dados);
	}

	public function setLocalizacaoAtual($dados) {
		$this->db->insert("localizacao",$dados);
	}

	public function setEndereco($dados) {
		$this->db->insert("endereco", $dados);	
	}

	public function deletarLocalizacao($idUsuario) {
		$this->db->delete('localizacao', array('fk_idUsuario' => $idUsuario));
	}
	
	public function atualizatStatusAtivacao($idUsuario, $dados) {
		$this->db->where('idUsuario', $idUsuario);
		$this->db->update('usuario', $dados);
	}

	public function getLocalizacaoPorIdUsuario($idusuario) {
		$this->db->where('fk_idUsuario', $idusuario);
		return $this->db->get('localizacao')->row_array();
	}
}