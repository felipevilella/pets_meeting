<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Desaparecimento_model extends CI_Model {
	function __construct() { 
		$this->load->database();
	}

	public function salvarDesaparecimento($dados) {
		$this->db->insert("anuncio_pets", $dados);
	}

	public function getDesaparecimento(){
		$this->db->limit(3);
		return $this->db->get("anuncio_pets")->result_array();
	}

	public function getDesaparecimentoPorID($idPet) {
		$this->db->where("fk_idPets", $idPet);
		return $this->db->get("anuncio_pets")->row_array();
	}

}