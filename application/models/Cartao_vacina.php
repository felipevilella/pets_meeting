<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cartao_vacina extends CI_Model {
	function __construct(){
		$this->load->database();
	}

	function setCartaoVacina($dados) {
		$this->db->insert('vacina', $dados);
	}

	function getCartaoVacinaPorIdPet($idPet) {
		$this->db->where("fk_idPet", $idPet);
		return $this->db->get('cartao_vacina')->row_array();
	}

	function getCartaoVacinaPorIdPetShop($nome) {
		$this->db->select("usuario.nome as nomeUsuario, usuario.fotoPrincipal as fotoUsuario, 
			animal.nome as nomeAnimal, animal.fotoPrincipal as fotoAnimal, cartao_vacina.idCartao as codigoCartao,
			cartao_vacina.situacao");
		$this->db->join("animal", "fk_idPet = idanimais");
		$this->db->join("usuario", "fk_idUsuario = idUsuario");
		$this->db->like('usuario.nome', $nome);
	
		return $this->db->get('cartao_vacina')->result_array();
	}



	function getCartaoVacinaPorId($idCartao) {
		$this->db->select("usuario.nome as nomeUsuario, usuario.fotoPrincipal as fotoUsuario, 
			animal.nome as nomeAnimal, animal.fotoPrincipal as fotoAnimal, animal.descricao,
			cartao_vacina.idCartao as codigoCartao, cartao_vacina.situacao, racas.nome as racaAnimal");
		$this->db->join("animal", "fk_idPet = idanimais");
		$this->db->join("racas","animal.fk_idraca = racas.idracas");
		$this->db->join("usuario", "fk_idUsuario = idUsuario");
		$this->db->where("idCartao", $idCartao);

		return $this->db->get('cartao_vacina')->row_array();
	}

	function getVacinas() {
		$this->db->select("idtipoVacina as codigoVacina, nome");
		$this->db->where("ativo", "1");
		return $this->db->get("tipo_vacina")->result_array();
	}

	function getAgendamentoVacinaPorDataIdVeterinario($idVeterinario, $data) {
		$this->db->select("vacina.*, horario.hora");
		$this->db->join("horario", 'fk_idHorario = idHorario');
		$this->db->where("fk_idVeterinario", $idVeterinario);
		$this->db->where("data", $data);
		return $this->db->get("vacina")->result_array();
	}

	function getAgendamentoPoridCartaoVacina($idCartaoVacina, $status = false) {
		$this->db->select("vacina.*, veterinario.nome, tipo_vacina.nome as nomeVacina, petshop.nome as nomePetshop");
		$this->db->join("veterinario", 'fk_idVeterinario = idVeterinario');
		$this->db->join("tipo_vacina", 'fk_idTipoVacina = idtipoVacina');
		$this->db->join("petshop","vacina.fk_idPetshop = idpetshop");
		$this->db->where("fk_idCartaoVacina", $idCartaoVacina);
		
		if ($status) {
			$this->db->where_in("status", $status);
		}

		return $this->db->get("vacina")->result_array();
	}

	function getConsultaVacinaPorId($idVacina) {
		$this->db->select("vacina.idVacina as codigoVacina, vacina.repetir, vacina.reforco, vacina.documento, vacina.data,
		 vacina.valor, horario.hora, veterinario.nome, tipo_vacina.nome as nomeVacina");
		$this->db->join("horario", 'fk_idHorario = idHorario');
		$this->db->join("veterinario", 'fk_idVeterinario = idVeterinario');
		$this->db->join("tipo_vacina", 'fk_idTipoVacina = idtipoVacina');
		$this->db->where("idVacina", $idVacina);

		return $this->db->get("vacina")->row_array();	
	}

	function updateCartaoVacina($dados, $idCartao) {
		$this->db->where("idCartao", $idCartao);
		$this->db->update("cartao_vacina", $dados);
	}
	
	function updateConsultaVacina($dados, $idVacina) {
		$this->db->where("idVacina", $idVacina);
		$this->db->update("vacina", $dados);
	}

	function ativarServico($dados) {
		$this->db->insert('cartao_vacina', $dados);
	}
}	