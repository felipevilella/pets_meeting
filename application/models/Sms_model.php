<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms_Model extends CI_Model {
	public function setSMS($dados) {
		$this->db->insert("sms", $dados);
	}

	public function getSMS($idUsuario) {
		$this->db->where("fk_idUsuario",$idUsuario);
		return $this->db->get("sms")->row_array();
	}

	public function deletarSMS($idUsuario) {
		$this->db->delete('sms', array('fk_idUsuario' => $idUsuario));
	}
}
?>