<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico_Model extends CI_Model {

	function __construct() { 
		$this->load->database();
	}

	public function getTipoServico() {
		$this->db->order_by('nome');
		$this->db->where('ativo','1');
		return $this->db->get('tipo_servico')->result_array();
	}

	public function getServicos($idTipoServico = false, $idEstado = false, $idCidade = false, $limit = false) {
		$this->db->select("servico.nome as nomeServico, servico.preco, servico.idServico as codigoServico, 
			servico.fk_idTipoServico as tipoServico, petshop.*,  localizacao.longitude, localizacao.latitude,
			cidade.nome as nomeCidade, estado.nome as nomeEstado, usuario.fotoPrincipal");
		$this->db->join('petshop','servico.fk_idPetshop = idpetshop');
		$this->db->join("usuario", "fk_idUsuarioPetshop = idUsuario");
		$this->db->join("cidade", "usuario.fk_idCidade = idCidade");
		$this->db->join("estado", "usuario.fk_idEstado = idEstado");
		$this->db->join("localizacao","fk_idUsuario = idUsuario ");
		$this->db->where("servico.ativo", "1");
		$this->db->where("petshop.ativo", "1");
		$this->db->order_by('nomeServico','asc');
		
		if ($idTipoServico) {
			$this->db->where("fk_idTipoServico", $idTipoServico);
		}
		if ($idEstado) {
			$this->db->where("usuario.fk_idEstado", $idEstado);
		}

		if ($idCidade) {
			$this->db->where("usuario.fk_idCidade", $idCidade);	
		}


		$this->db->where("fk_idTipoServico != 5");
		return $this->db->get("servico")->result_array();
	}

	public function getAvaliacaoServicoPorIdUsuario($idUsuario, $idReservaServico = false) {
		$this->db->select('avaliacao_servico.*, servico.nome');
		$this->db->where('avaliacao_servico.fk_idUsuario', $idUsuario);
		
		if($idReservaServico) {
			$this->db->where('avaliacao_servico.fk_idReservaServico', $idReservaServico);
		}
		
		$this->db->join('reserva_servico', 'avaliacao_servico.fk_idReservaServico = idReservaServico');
		$this->db->join('servico', 'reserva_servico.fk_idServico = idServico');
		return $this->db->get("avaliacao_servico")->result_array();
	}

	public function setAvaliacao($dados) {
		$this->db->insert("avaliacao_servico", $dados);
	}

	public function updateServicosNaoFinalizados($idUsuario) {
		$this->db->where("data BETWEEN '2020/01/01' AND '". date('Y/m/d', strtotime('-1 day'))."'");
		$this->db->where("fk_idUsuario", $idUsuario);
		$this->db->where("situacao", "0");
		$this->db->update("reserva_servico", array("situacao" => "3"));
	}

	public function getServicoRealizadosPorIdUsuario($idUsuario) {
		$this->db->select("reserva_servico.*, petshop.nome as nomePetshop, petshop.idpetshop, servico.nome as nomeServico, servico.preco, servico.idServico as codigoServico, animal.nome as nomeAnimal");
		$this->db->join('servico','reserva_servico.fk_idServico = idServico');
		$this->db->join('animal','reserva_servico.fk_idAnimal = idanimais');
		$this->db->join('petshop','servico.fk_idPetshop = idpetshop');	
		$this->db->where('reserva_servico.fk_idUsuario', $idUsuario);
		$this->db->where('situacao', '1');
		return $this->db->get("reserva_servico")->result_array();
	}

	public function getServicoReservadoPorIdUsuario($idUsuario) {
		$this->db->where('fk_idUsuario', $idUsuario);
		return $this->db->get("reserva_servico")->result_array();
	}
}