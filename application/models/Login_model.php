<?php
class Login_model extends CI_Model{

	function __construct(){
		$this->load->database();
	}
	
	public function validacao($usuario,$senha){
		$this->db->where("email",$usuario);
		$this->db->where("senha",$senha); 
		return $this->db->get("usuario")->row_array();
	}
	
	public function session_usuario($dados) {
		$this->load->library('session');
		$this->session->set_userdata('usuario_session',$dados["email"]);
		$this->session->set_userdata('senha_session',$dados["senha"]);
		$this->session->set_userdata('nome_session',$dados["nome"]);
		$this->session->set_userdata('idusuario_session',$dados["idUsuario"]);
		$this->session->set_userdata('comercial_session',$dados["comercial"]);
		$this->session->set_userdata('estado_session', $dados["estado"]);
		$this->session->set_userdata('cidade_session', $dados["cidade"]);

		if (array_key_exists("longitude", $dados) && array_key_exists("latitude", $dados)) {
			$this->session->set_userdata('longitude_session',$dados["longitude"]);
			$this->session->set_userdata('latitude_session',$dados["latitude"]);
		}
	}
	
	public function log_acesso($dados) {
		$this->db->insert("log_acesso", $dados);
	}
}




