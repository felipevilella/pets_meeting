<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil_model extends CI_Model{

	function __construct() { 
		$this->load->database();
	}

	public function getPerfilPorQrCode($codigo) {
		$this->db->select("animal.*,racas.nome as nomeraca");
		$this->db->where("codigo", $codigo);
		$this->db->join("animal", "fk_idanimal = idanimais");
		$this->db->join("racas","fk_idraca = idracas");
		return $this->db->get("qrcode_identificacao")->row_array();
	}

}