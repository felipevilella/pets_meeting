<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Paginainicial/paginaIndex';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route["perfil/(:num)"] = "Sistema_interno/Perfil/perfil/$1"; 
$route["perfilpets/(:num)"] = "Sistema_interno/Perfilpets/verPerfil/$1"; 
$route["selecionarperfilpets"] = "Sistema_interno/Selecionarperfil";
$route["login"] = "Paginainicial/login";
$route["acessoAplicativo"] = "Paginainicial/inicioDispositivo";
$route["cadastro"] = "Paginainicial/cadastro";
$route['inicio'] = 'Paginainicial';
$route["pesquisa"] = "Sistema_interno/Pesquisa/listarServico";
$route["pesquisa/(:num)"] = "Sistema_interno/Pesquisa/listarServico/$1";
$route["cadastrarPets"] = "Sistema_interno/Selecionarperfil/cadastrarPets";
$route["enviarMensagem"] = "Sistema_interno/ChatCompleto_controller/salvar_conversa";
$route["mensagem"] = "Sistema_interno/ChatCompleto_controller/buscar_conversa";
$route["buscarCidade"] = "Paginainicial/buscarCidadePorEstado";
$route["salvarUsuario"] = "Paginainicial/cadastroUsuarios";
$route["salvarFotopets"] = "Sistema_interno/Perfilpets/salvarFotoPetsPerfil";
$route["salvarDescricao"] ="Sistema_interno/Perfilpets/salvarSobre";
$route["inserirfotoPets"] = "Sistema_interno/Perfilpets/inserirFotoPetsPerfil";
$route["alterarPets"] = "Sistema_interno/Perfilpets/alterarPets";
$route["alterarFotoUsuario"] = "Sistema_interno/Perfil/salvarFotoUsuario";
$route["validarUsuario"] = "Paginainicial/validaEmail";
$route["verperfilpets/(:num)/(:num)"] = "Sistema_interno/Perfilpets/perfilPetsAmigo/$1/$2";
$route["sair"] = "Sistema_interno/Perfil/deslogarUsuario";
$route["solicitarSenha"] = "Paginainicial/enviarEmail";
$route["modificarsenha/(:any)"] = "Paginainicial/modificarSenha/$1";
$route["alterarsenha"] = "Paginainicial/alterarSenha";
$route["dadosPesquisaPets"] = "Sistema_interno/Pesquisa/dadosPesquisaPets";
$route["notificacao"] = "Sistema_interno/Notificacao/notificacaoUsuario";
$route["acessoInicial"] = "Paginainicial/paginaIndex";
$route["noticias"] = "Sistema_interno/donoPets/Home";
$route["perfil"] = "Sistema_interno/donoPets/Perfil";
$route["informacao_pessoais"] = "Sistema_interno/Perfil/informacaoPessoais";
$route["carrinho"] = "Sistema_interno/donoPets/Carrinho";
$route['removerIntem'] = "Sistema_interno/donoPets/Carrinho/removerIntem";
$route['adicionarPagamento'] = "Sistema_interno/donoPets/Carrinho/adicionarPagamento";
$route['salvarCartao'] = "Sistema_interno/donoPets/Pagamento/salvarCartao";
$route['excluirCartao'] = "Sistema_interno/donoPets/Pagamento/excluirCartao";
$route['confirmarReserva'] = "Sistema_interno/donoPets/Pagamento/confirmarReserva";

$route['verificar_endereco'] = "Sistema_interno/Endereco/verificarEndereco";
$route['adicionar_endereco'] = "Sistema_interno/Endereco";
$route['salvarEndereco'] = "Sistema_interno/Endereco/salvarEndereco";
$route['deletarEndereco'] = "Sistema_interno/Endereco/excluirEndereco";
$route["selecionarEndereco"] = "Sistema_interno/Endereco/selecionarEndereco";

$route["estabelecimento/(:num)"] = "Sistema_interno/donoPets/Petshop/visualizarEstabelecimento/$1";
$route["reservarServico/(:num)"] = "Sistema_interno/donoPets/Petshop/servico/$1";
$route["cadastrar_estabelecimento"] = "Paginainicial/cadastroEstabelecimento";

# Mapa 
$route["localizacao"] = "Sistema_interno/Mapa/mapaNovaLocalizacao";
$route["salvarLocalizacao"] = "Sistema_interno/Mapa/salvarLocalizacao";

# Qrcode
$route["estimacao/(:num)"] = "Qrcode/perfil/$1";
$route["qrcodeLeitura"] = "qrcode/leitura";
$route["detalharPerfil/(:num)"] = "qrcode/detalhePerfil/$1";

# Petshop
$route["financeiro"] = "Sistema_interno/petshop/Financeiro";
$route["servicos"] = "Sistema_interno/petshop/Servico";
$route["atendimentos"] = "Sistema_interno/petshop/Atendimento";
$route["vacinas"] = "Sistema_interno/petshop/vacina";
$route["detalharCartaoVacina/(:num)"] = "Sistema_interno/petshop/Vacina/detalharCartaoVacina/$1";
$route['verificarFoto'] = "Sistema_interno/Perfil/verificarFoto";
$route['verificarVeterinario'] = "Sistema_interno/petshop/servico/verificarFuncionarios";
$route["entrar"] = "Paginainicial/loginParceiro";
$route['salvarDadosBancario'] = "Sistema_interno/petshop/Pagamento/salvarDadosBancario";
$route['gerarTokenSMS'] = 'Sistema_interno/petshop/Pagamento/gerarTokenSMS';
$route['verificarPagamento'] = "Sistema_interno/petshop/Pagamento/VerificarDadosPagamento";

# Rotas JS - PetShop
$route["modalSobre"] = "Sistema_interno/petshop/Servico/modalSobre";
$route["tipoConsulta"] = "Sistema_interno/petshop/Servico/getTipoConsulta";
$route["salvarServico"]  = "Sistema_interno/petshop/Servico/salvarServico";
$route["salvarSobre"] = "Sistema_interno/petshop/Servico/salvarSobre";
$route["obterServico"] = "Sistema_interno/petshop/Servico/obterServico";
$route["alterarServico"] = "Sistema_interno/petshop/Servico/alterarServico";
$route["deletarServico"] = "Sistema_interno/petshop/Servico/deletarServico";
$route["buscarAtendimento"] = "Sistema_interno/petshop/atendimento/buscarAtendimento";
$route["detalharAtendimento"] = "Sistema_interno/petshop/atendimento/detalharAtendimento";
$route["atualizarAtendimeto"] = "Sistema_interno/petshop/atendimento/atualizarAtendimeto";
$route["ativarServicoVacina"] = "Sistema_interno/petshop/Vacina/ativarServicoVacina";
$route["verificarHorarioDisponivel"] = "Sistema_interno/petshop/Vacina/verificarHorarioDisponivel";
$route["buscarCartaoVacina"] = "Sistema_interno/petshop/Vacina/buscarCartaoVacina";
$route["agendarVacina"] = "Sistema_interno/petshop/Vacina/agendarVacina";
$route["registrarVeterinario"] = "Sistema_interno/petshop/servico/registrarVeterinario";
$route["funcionarios"] = "Sistema_interno/petshop/servico/funcionarios";
$route["atualizarFuncionario"] = "Sistema_interno/petshop/servico/atualizarFuncionario";
$route["editarFuncionario"] = "Sistema_interno/petshop/servico/editarFuncionario";
$route["deletarVeterinario"] = "Sistema_interno/petshop/servico/deletarVeterinario";
$route["atualizarConsultaVacina"] = "Sistema_interno/petshop/Vacina/atualizarConsultaVacina";
$route["listarVacina"] = "Sistema_interno/petshop/Vacina/listarVacina";
$route["notificarPetshop"] = "Sistema_interno/notificacao/notificarPetshop";
$route["analisaServicoAtivo"] = "Sistema_interno/petshop/Vacina/analisaServicoAtivo";
$route['carregarFotoPerfil'] = "Sistema_interno/Perfil/obterFotoUsuario";
$route['carregarFotoPets'] = "Sistema_interno/Perfil/obterFotoPets";

# Rotas JS - Donos de animais
$route["sobreModal"] = "Sistema_interno/Perfil/modalSobre";
$route["horarioServicos"] = "Sistema_interno/donoPets/petshop/horarioServicos";
$route["agendarServico"] = "Sistema_interno/donoPets/petshop/agendarServico";
$route["detalharServico"] = "Sistema_interno/donoPets/petshop/detalharServico";
$route["obterConsultas"] = "Sistema_interno/Perfilpets/obterConsultas";
$route["solicitarCancelamento"] = "Sistema_interno/donoPets/petshop/solicitarCancelamentoReservaServico";
$route["obterPetshop"] = "Sistema_interno/donoPets/Vacina/obterPetshop";
$route["ativarServico"] = "Sistema_interno/donoPets/Vacina/ativarServico";
$route["verificarServicoVacina"] = "Sistema_interno/donoPets/Vacina/verificarServicoVacina";
$route["buscarPetShop"] = "Sistema_interno/Pesquisa/buscarPetshop";
$route["salvarSobrePerfil"] = "Sistema_interno/Perfil/atualizarSobre";
$route['obterRacasPets'] = "Sistema_interno/Selecionarperfil/obterRacaAnimal";
$route['verificarAvaliacaoServico'] = "Sistema_interno/donoPets/petshop/verificaAvaliacaoServico";
$route['salvarAvaliacao'] = "Sistema_interno/donoPets/petshop/salvarAvaliacao";

## controller de funções ##
$route["validacaousuario"] = "funcoes_sistema/Login/validacao_login";
