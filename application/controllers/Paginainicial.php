<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class paginainicial extends CI_Controller  {
	
	public function index() {	
		$this->load->helper("url");
		$this->load->view("estrutura/comingsoon");
	}

	public function login() {
		$this->load->helper("url");
		
		$dados["dispositivos"] = reconhecerDispositivo();
		$dados['scripts'] = array('assets\personalizado\js\sistema.js');

		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/login");
		$this->load->view("dashbord/template/footer");
	}

	public function inicioDispositivo() {
		$this->load->helper("url");
		
		$dados["dispositivos"] = reconhecerDispositivo();
		$dados['scripts'] = array('assets\personalizado\js\sistema.js');

		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/paginaInicialApp");
	}

	public function loginParceiro() {
		$this->load->helper("url");
		
		$dados["dispositivos"] = reconhecerDispositivo();
		$dados['scripts'] = array('assets\personalizado\js\sistema.js');

		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/petshop/login");
		$this->load->view("dashbord/template/footer");
	}

	public function cadastro() {
		$this->load->helper("url");
		$this->load->model("localizacao_model");
		
		$dados["dispositivos"] = reconhecerDispositivo();
		$dados['scripts'] = array('assets\personalizado\js\sistema.js');
		$dados["estados"] = $this->localizacao_model->buscarEstado();

		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/cadastro");
		$this->load->view("dashbord/template/footer");	
	}

	public function paginaIndex() {
		$this->load->helper("url");

		$this->load->view("estrutura/template/header");
		$this->load->view("estrutura_interna/telainicial");
		$this->load->view("estrutura/template/footer");	
	}

	public function cadastroEstabelecimento() {
		$this->load->helper("url");
		$this->load->helper('funcoes_helper');
		$this->load->model("localizacao_model");

		$dados["estados"] = $this->localizacao_model->buscarEstado();
		$dados["dispositivo"] = reconhecerDispositivo();

		$this->load->view("estrutura/template/header", $dados);
		$this->load->view("estrutura/cadastroEstabelecimento");
		$this->load->view("estrutura/template/footer");	
	}
	
	public function validaEmail() {
		$this->load->helper("url");
		$this->load->library('form_validation');
		$this->load->model("usuario_model");

		$email = $this->input->post("email");
		$emailValido = $this->usuario_model->buscarEmailExistente($email);

		if(!empty($emailValido)){
			log_message('error', 'Foi realizado uma tentativa de cadastro com o email existente '.$email);
			echo json_encode(array("mensagem" => "E-mail indisponivel"));
		} else{
			echo json_encode(array("mensagem" => ""));
		}
	}


	public function cadastroUsuarios() {
		$this->load->helper("url");
		$this->load->library('form_validation');
		$this->load->model("usuario_model");

		$this->form_validation->set_rules("nome","nome","required|min_length[3]|max_length[45]");
		$this->form_validation->set_rules("estado","estado","required");
		$this->form_validation->set_rules("cidade","cidade","required");
		$this->form_validation->set_rules("email","email","required|valid_email|max_length[45]");
		$this->form_validation->set_rules("senha","senha","required|min_length[8]|max_length[45]");
		$this->form_validation->set_rules("comercial", "comercial", "required");

		if($this->form_validation->run() == false) {
			echo json_encode($this->form_validation->error_array());
		}
		else {
			
			$dados = array(
				'nome'=> $this->input->post("nome"),
				'email' => $this->input->post("email"),
				'senha' =>  md5($this->input->post("senha")),
				'fk_idCidade' => $this->input->post("cidade"),
				'fk_idEstado' => $this->input->post("estado"),
				'comercial' => $this->input->post("comercial")
			);

			$this->usuario_model->cadastroUsuario($dados);
			echo json_encode(array("mensagem" => "Conta cadastrada com sucesso!"));
		}
	}
	public function buscarCidadePorEstado() {
		$this->load->helper("url");
		$this->load->model("localizacao_model");

		$idEstado = $this->input->post("idEstado");
		$dadosCidades = $this->localizacao_model->getbuscarCidadePorEstado($idEstado);
		echo json_encode($dadosCidades);
	}
	
	/**
	* Função responsavel por encaminhar e-mail com o codigo de alteração da senha
	* @author Felipe Vilella
	*
	**/
	public function enviarEmail() {
		$this->load->helper("url");
		$this->load->library('email');
		$this->load->model('usuario_model');

		$email = $this->input->post("email");
		$codigo = strtoupper(substr(bin2hex(random_bytes(4)), 1));
		if (empty($email)) {
			echo json_encode(array(
				"tipo" => "alert",
				"mensagem" => "E-mail incorreto ou não cadastrado, digite novamente!"
			));
		} else {

			$dadosUsuario =  $this->usuario_model->buscarDadosUsuario($email);
			if (empty($dadosUsuario)) {
				echo json_encode(array(
					"tipo" => "alert",
					"mensagem" => "E-mail incorreto ou não cadastrado, digite novamente!"
				));
			} else {

				$nome = $dadosUsuario['nome'];

				$dados = array(
					"fk_idUsuario" => $dadosUsuario["idUsuario"],
					"codigo" => $codigo
				);
				$this->usuario_model->solicitacao_senha($dados);

				$url = "https://petsmeeting.com.br/modificarsenha/".$codigo;

				$foto = "<img class='img' src=".base_url("../../assets/personalizado/fotos_pets/logo1.png").">";

				$config['protocol'] = 'smtp';
				$config["smtp_user"] = "petsmeetingoficial@gmail.com";
				$config["smtp_pass"] = "pets@100";
				$config['charset'] = 'UTF-8';
				$config["smtp_host"] = "smtp.umbler.com";
				$config['smtp_port'] = '587';

				$config['wordwrap'] = TRUE; // define se haverá quebra de palavra no texto
        		$config['validate'] = TRUE; // define se haverá validação dos endereços de email

        		$this->email->initialize($config);
        		$this->email->set_mailtype("html");
        		$this->email->from("contato@petsmeeting.com.br","Pets meeting");
        		$this->email->to($email);
        		$this->email->subject("$nome, este e o link para redefinir a sua senha");
        		$this->email->message("
        			<h4>Olá $nome</h4>

        			Redefina sua senha para começar novamente.<br>
        			Para alterar sua senha do pets meeting, clique no link abaixo:<br><br>
        			<a href = '$url'> Redefinir minha senha </a><br><br>
        			O link é válido por 12 horas, portanto, utilize-o imediatamente.<br><br>
        			Obrigado por utilizar o pets meeting!<br>	
        			Equipe do pets meeting<br>");

        		if($this->email->send()) {
        			echo json_encode(array(
        				"tipo" => "success",
        				"mensagem" => "Solicitação enviada para o e-mail ".$email
        			));
        		} else {
        			echo $this->email->print_debugger();
        		}
        	}

        }
    }

/**
* Função responsavel por verificar se o codigo está dentro do horario previsto para a alteração da senha
* @author Felipe Vilella
* @param [String] Codigo
*/
function modificarSenha ($codigo) {

	$this->load->model("usuario_model");
	date_default_timezone_set('America/Sao_Paulo');
	$dataatual = date("Y-m-d H:i:s");
		# Obtendo os dados da solicitação de senha
	$dadosSolicitacao = $this->usuario_model->verificarCodigoSenha($codigo);

	if(empty($dadosSolicitacao)) {
		$this->load->helper("url");
		$this->load->view("estrutura/headerinicial");
		$this->load->view("estrutura/navbar_inicial");
		$this->load->view("estrutura_interna/errotoken");
		$this->load->view("estrutura/footerinicial");
	} else {
		$dadosUsuario = $this->usuario_model->buscarUsuario($dadosSolicitacao["fk_idUsuario"]);

		$dados["nome"] = $dadosUsuario["nome"];
		$dados["idUsuario"] = $dadosUsuario["idUsuario"];
			# Verificando os dados
		$dataSolicitacao = explode(" ", $dadosSolicitacao["data"]);
		$dataVerificacao = explode(" ", $dataatual);

		$diaSolicitacao = explode("-", $dataSolicitacao[0]);
		$diaverificacao = explode("-", $dataVerificacao[0]);

		$horarioSolicitacao = explode(":", $dataSolicitacao[1]);
		$horarioVerificacao = explode(":", $dataVerificacao[1]); 

		$horas = abs($horarioSolicitacao[0] - $horarioVerificacao[0]);
		$minutos = abs($horarioSolicitacao[1] - $horarioVerificacao[1]);
		$segundos = abs($horarioSolicitacao[2] - $horarioVerificacao[2]);


		if (($diaSolicitacao[2] == $diaverificacao[2]) && ($diaSolicitacao[1] == $diaverificacao[1]) && 
			($diaSolicitacao[0] == $diaverificacao[0]) && $horas < 13) {
			$modificacao = true;
	} else if (($diaSolicitacao[2] - $diaverificacao[2] == 1) && ($diaSolicitacao[1] - $diaverificacao[1] == 0) 
		($diaSolicitacao[0] - $diaverificacao[0] == 0)  &&  $tempoSolicitacao >= 12) {
		$modificacao = true;
	} else {
		$modificacao = false;
	}

	$this->load->helper("url");
	$this->load->view("estrutura/headerinicial");
	$this->load->view("estrutura/navbar_inicial");

	if ($modificacao == true) {
		$this->load->view("estrutura_interna/recuperarsenha",$dados);
	} else {
		$this->load->view("estrutura_interna/errotoken");
	}

	$this->load->view("estrutura/footerinicial");
}

}

function alterarSenha () {
	$this->load->model("usuario_model");
	$idUsuario = $this->input->post("idUsuario");
	$dados = array(
		"senha" => md5($this->input->post("senha"))
	);
	$dadosSolicitacao = array(
		"ativo" => "false"
	);

	$this->usuario_model->alterarPerfilusuario($dados,$idUsuario);
	$this->usuario_model->atualizarSolicitacao($dadosSolicitacao,$idUsuario); 

	echo json_encode(array(
		"tipo" => "success",
		"mensagem" => "senha alterada com sucesso"
	));

}

}   