<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qrcode extends CI_Controller  {
	public function index() {

	} 

	public function perfil($codigoQrcode) {
		$this->load->helper("url");
		$this->load->helper("funcoes");
		$this->load->model("qrcode/perfil_model");
		$this->load->model("usuario_model");

		$dados["perfilPet"] = $this->perfil_model->getPerfilPorQrCode($codigoQrcode);
		
		if ($dados["perfilPet"]["sexo"] == "f") {
			$dados["perfilPet"]["mensagem"] = "Olá, eu sou a";
		} else {
			$dados["perfilPet"]["mensagem"] = "Olá, eu sou o";
		}

		$dados["perfilPet"]["fotoPrincipal"] = verificarFotoAnimal($dados["perfilPet"]["fotoPrincipal"]);
		$dados["url"] = base_url("detalharPerfil/".$codigoQrcode);

		$dados["usuario"] = $this->usuario_model->buscarUsuario($dados["perfilPet"]["fk_idUsuario"]);
		$dados["usuario"]["fotoPrincipal"] = verificarFotoUsuario($dados["usuario"]["fotoPrincipal"]);
		$dados["scripts"] = false;
		

		$dispositivo = reconhecerDispositivo();

		if ($dispositivo["plataforma"] == "iOS" || $dispositivo["plataforma"] == "Android") {
			$this->load->view("dashbord/template/header", $dados);
			$this->load->view("estrutura_interna/qrcode/capa");
			$this->load->view("estrutura/qrcode/footer");	
		} else {
			redirect(base_url('inicio'));
		}
			
	}

	public function detalhePerfil($codigoQrcode) {
		$this->load->helper("url");
		$this->load->helper("funcoes");
		$this->load->helper("form");

		$this->load->model("qrcode/perfil_model");
		$this->load->model("usuario_model");
		$this->load->model("petshop/servico_model");
		$this->load->model('cartao_vacina');

		$dados["perfilPet"] = $this->perfil_model->getPerfilPorQrCode($codigoQrcode);
		
		# Obter consultas
		$consultas = $this->servico_model->getServidoReservadoPorIdAnimal($dados["perfilPet"]["idanimais"], array(0,1));
		foreach ($consultas as $key => $consulta) {
			$consultas[$key]["data"] = converterData($consulta["data"]);
			$consultas[$key]["hora"] = padronizaHorario($consulta["hora"]);
		}

		$cartaoVacina = $this->cartao_vacina->getCartaoVacinaPorIdPet($dados["perfilPet"]["idanimais"]);
		$historicoVacinas = $this->cartao_vacina->getAgendamentoPoridCartaoVacina($cartaoVacina["idCartao"], array(2,1)); 
		foreach ($historicoVacinas as $key => $vacina) {
			$historicoVacinas[$key]["hora"] = padronizaHorario($vacina["hora"]);
			$historicoVacinas[$key]["data"] = converterData($vacina["data"]);
		}

		$dados["historicoVacinas"] = $historicoVacinas;
		$dados["consultas"] = $consultas;
		$dados["perfilPet"]["fotoPrincipal"] = verificarFotoAnimal($dados["perfilPet"]["fotoPrincipal"]);

		$dados["usuario"] = $this->usuario_model->buscarUsuario($dados["perfilPet"]["fk_idUsuario"]);
		$dados["usuario"]["fotoPrincipal"] = verificarFotoUsuario($dados["usuario"]["fotoPrincipal"]);
		$dados["scripts"] = false;

		$dispositivo = reconhecerDispositivo();

		if ($dispositivo["plataforma"] == "iOS" || $dispositivo["plataforma"] == "Android") {
			$this->load->view("estrutura/qrcode/header",$dados);
			$this->load->view("estrutura_interna/qrcode/perfil");
			$this->load->view("estrutura/qrcode/footer");
		} else {
			redirect(base_url('inicio'));	
		}		
	}

	public function leitura() {
		$this->load->helper("url");
		$this->load->helper("funcoes_helper");

		$dispositivo = reconhecerDispositivo();

		if ($dispositivo["plataforma"] == "iOS" || $dispositivo["plataforma"] == "Android") {
			$data["scripts"] = array(base_url('assets/personalizado/qrcode/js/qrcode.js'));
			$data["tipo"] = "success";
			$data["mensagem"] = "Dispositivo compativel";
			
		} else {
			$data["scripts"] = array(base_url('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js'));
			$data["tipo"] = "alert";
			$data["mensagem"] = "Leitura Qrcode está somente disponivel em dispositivos moveis.";
		}

		$this->load->view("estrutura/qrcode/header", $data);
		$this->load->view("estrutura_interna/qrcode/leitura");
		$this->load->view("estrutura/qrcode/footer");
	}


}