<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index() {
		$this->load->library('session');
		$this->load->helper('url');

		$this->load->model("usuario_model");
		$this->load->model("localizacao_model");
		
		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}


		$data["idusuario"] = $this->session->idusuario_session;
		$data["nome"] = $this->session->nome_session;
		$data["estados"] = $this->localizacao_model->buscarEstado();

		$data["idusuario"] = $this->session->idusuario_session;
		$data["nome"] = $this->session->nome_session;
		$data["estados"] = $this->localizacao_model->buscarEstado();

		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$data['dispositivos'] = reconhecerDispositivo();

		if ($data["dispositivos"]["plataforma"] != "Android" && $data["dispositivos"]["plataforma"] != "iOS") {
			redirect(base_url('pesquisa'));	
		}

		$data["scripts"] = array("assets/personalizado/js/cliente/donosPet.js","assets/personalizado/js/maps_busca.js");

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/home");
		$this->load->view("dashbord/template/footer");
	}

}