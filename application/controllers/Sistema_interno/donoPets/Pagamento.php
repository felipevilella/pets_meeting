<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagamento extends CI_Controller {
	
	public function index() {

	}

	public function salvarCartao() {
		$this->load->library("form_validation");
		$this->load->library("MY_Form_validation");
		$this->load->library('encryption');
		$this->load->library('session');

		$this->load->model("banco_model");
		
		$this->form_validation->set_rules("numero", "numero cartao", "required|valida_cartao");
		$this->form_validation->set_rules("dataExpiracao", "validade", "required|valid_date_cartao");
		$this->form_validation->set_rules("nome", "nome", "required");
		$this->form_validation->set_rules("cpf", "cpf", "required|valid_cpf");
		$this->form_validation->set_rules("cvv", "cvv", "required|min_length[2]");

		if(!$this->form_validation->run()) {
			die(json_encode(array("tipo" => "alert", "campos" => $this->form_validation->error_array())));
		}

		$dados = array(
			'numero' => trim($this->encryption->encrypt(removerEspacoCartao($this->input->post("numero")))),
			'dataExpiracao' => $this->encryption->encrypt($this->input->post("dataExpiracao")),
			'nome' => $this->encryption->encrypt($this->input->post("nome")),
			'cpf' => $this->encryption->encrypt($this->input->post("cpf")),
			'cvv' => $this->encryption->encrypt($this->input->post("cvv")),
			'fk_idUsuario' => $this->session->idusuario_session
		);

		$this->banco_model->setCartao($dados);
		echo json_encode(array("tipo" => "success", "mensagem" => "Dados salvo com sucesso"));
	}

	public function excluirCartao() {
		$this->load->model("banco_model");

		$codigoCartao = $this->input->post("codigoCartao");
		$this->banco_model->excluirCartao($codigoCartao);
		echo json_encode(array("tipo" => "success", "mensagem" => "cartão excluido com sucesso"));
	}


	public function confirmarReserva() {
		$this->load->helper('url');

		$this->load->library('encryption');
		$this->load->library('session');

		$this->load->model("petshop/servico_model");
		$this->load->model("banco_model");
		$this->load->model("carrinho_model");
		$this->load->model("contato_model");
		$this->load->model("localizacao_model");

		$this->load->helper('pagamento/pagar_me');

		$servicosReservados = $this->carrinho_model->getCarrinhoPorIdUsuario($this->session->idusuario_session);
		$contato = $this->contato_model->getContatoUsuario($this->session->idusuario_session);
		$dadosCartao = $this->banco_model->getCartaoPorIdUsuario($this->session->idusuario_session);
		$enderecos = $this->localizacao_model->getEnderecoPorIdUsuario($this->session->idusuario_session);

		$data["total"] = 0;

		foreach ($servicosReservados as $key => $servicoReservado) {
			$servicosReservados[$key]["hora"] = padronizaHorario($servicoReservado["hora"]);
			$servicosReservados[$key]["data"] = converterData($servicoReservado["data"]);
			$servicosReservados[$key]["idRecebedor"] = $this->banco_model->geidRecebedorPorIdPetshop($servicoReservado["fk_idPetshop"]);
			$data["total"] = doubleval($data['total']) + doubleval($servicoReservado["preco"]);
		}

		# tirando os 10% do valor para obter a portecetagem de cada serviço
		$porcetagens = $this->carrinho_model->getValorPorcentagemPorProduto($data['total'], $this->session->idusuario_session);
		
		# Ajustando as porcetagens
		foreach ($porcetagens as $key => $portecetagem) {
			$porcetagens[$key]['valorPorcetagem'] = $portecetagem['valorPorcetagem']; 
		}

		# juntando  informação do recebedor
		foreach ($servicosReservados as $key => $servicoReservado) {
			$recebedores[$key]['idRecebedor'] = $servicoReservado['idRecebedor'];
			$recebedores[$key]['valorPorcetagem'] = redondarValor($porcetagens[$key]['valorPorcetagem']);
		}

		# descriptograr dados cartoes

		$data['numeroCartao'] = $this->encryption->decrypt($dadosCartao['numero']);
		$data['codigoCVV'] = $this->encryption->decrypt($dadosCartao['cvv']);
		$data['expiracaoData'] = removerMascara($this->encryption->decrypt($dadosCartao['dataExpiracao']));
		$data['nomePortador'] = $this->encryption->decrypt($dadosCartao['nome']);
		$data['cpf'] = removerMascara($this->encryption->decrypt($dadosCartao['cpf']));
		$data['telefone'] = "+55".removerMascara($contato['descricao']);
		$data["nomeCliente"] = $this->session->nome_session;
		$data['email'] = $this->session->usuario_session;
		$data['idCliente'] = $this->session->idusuario_session;
		$data["total"] = formatarMonetario($data["total"]);


		# endereco
		$data['numero'] = $enderecos['numero'];
		$data['rua'] = $enderecos['rua'];
		$data['cep'] = removerMascara($enderecos['cep']);
		$data['bairro'] = $enderecos['bairro'];
		$data['cidade'] = $enderecos['nomeCidade'];
		$data['estado'] = $enderecos['sigla'];

		$data['recebedores'] = $recebedores;
		$data['produtos'] = $servicosReservados;

		$retornoPagamento = cartaoCredito($data);
		
		if ($retornoPagamento['tipo'] == 'success') {
			$this->servico_model->updateReservaServico($this->session->idusuario_session, array('preReserva' => 0, 'token_transacao' => $retornoPagamento['token']));

			foreach ($servicosReservados as $reserva) {
				$this->carrinho_model->deletarProduto($reserva['idCarrinho']);
			}

			echo json_encode(array("tipo" => "success", "mensagem" => $retornoPagamento['mensagem']));
		} else {
			echo json_encode(array("tipo" => "alert", "mensagem" => $retornoPagamento['mensagem']));
		}
	}
}