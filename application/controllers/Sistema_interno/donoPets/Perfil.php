<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {
	
	public function index() {
		$this->load->library('session');
		$this->load->helper('url');

		$this->load->model("servico_model");
		$this->load->model("usuario_model");
		$this->load->model("localizacao_model");
		$this->load->model("pets_model");
		
		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

		$data["idusuario"] = $this->session->idusuario_session;
		$data["nome"] = $this->session->nome_session;
		$data["estados"] = $this->localizacao_model->buscarEstado();

		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$data["informacaoUsuario"]["fotoPrincipal"] = verificarFotoUsuario($data["informacaoUsuario"]["fotoPrincipal"]);
		$data['dispositivos'] = reconhecerDispositivo();

		# contadores de reserva de serviço, perfil e avaliacao;
		$pets = $this->pets_model->buscarPets($this->session->idusuario_session);
		$servicos = $this->servico_model->getServicoReservadoPorIdUsuario($this->session->idusuario_session);
		$avaliacao = $this->servico_model->getAvaliacaoServicoPorIdUsuario($this->session->idusuario_session);
		
		$data['totalPets'] = count($pets);
		$data['totalReservas'] = count($servicos);
		$data['totalAvaliacao'] = count($avaliacao);


		$data["scripts"] = array(
			"assets/personalizado/js/cliente/donosPet.js",
			"assets/personalizado/js/maps_busca.js",
			"assets/personalizado/js/sistema.js"
		);

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/perfil");
		$this->load->view("dashbord/template/footer");
	}

}