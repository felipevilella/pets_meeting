<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petshop extends CI_Controller {
	
	public function index() {

	}

	public function visualizarEstabelecimento($codigoPetshop) {
		$this->load->helper("url");
		$this->load->helper("funcoes");
		$this->load->helper("form");
		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model("localizacao_model");
		$this->load->model("petshop/informacao_model");
		$this->load->model("petshop/servico_model");

		if (empty($this->session->usuario_session)) {
			redirect(base_url('inicio'));
		}

		$dados["idusuario"] = $this->session->idusuario_session;
		$dados["nome"] = $this->session->nome_session;
		
		$usuario = $this->usuario_model->buscarUsuario($dados["idusuario"]);

		if ($usuario["localizacao"] == 1) {
			redirect(base_url("localizacao"));
		} 

		$petshop = $this->informacao_model->getPetShopPoridPetShop($codigoPetshop);
		$servico = $this->servico_model->getServicosReservadoPorIdPetShop($codigoPetshop, false);

		$dados["dadosusuario"] = $usuario;
		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($dados["idusuario"]);

		#Dados estabelecimento
		$dados["veterinarios"] = $this->informacao_model->getVeterinariosPoridPetshop($codigoPetshop);
		$petshop["fotoPrincipal"] = verificarFotoPetshop($petshop["fotoPrincipal"]);
		$dados["petshop"] = $petshop;

		# obtendo as avaliações sobre os serviços oferecidos
		$dados['comentarios'] = $this->servico_model->getComentarioAvaliacaoServicoPorIdPetShop($codigoPetshop);
		$dados['nota'] = $this->servico_model->getNotaAvaliacaoServicoPorIdPetShop($codigoPetshop);
		$dados['nota'] = $dados['nota']['total'];
		$dados['totalComentario'] = count($dados['comentarios']);
		$dados['totalVenda'] = count($servico);

	
		$dados["scripts"] = array("assets/personalizado/js/cliente/donosPet.js","assets/personalizado/js/maps_busca.js");
		$dados["scriptMaps"] = array("https://maps.googleapis.com/maps/api/js?key=AIzaSyCAAbe8Z0i7XexLH7hooViwxV-bBAYo-Mw&callback=initMap");
		$dados['dispositivos'] = reconhecerDispositivo();

		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/estabelecimento");
		$this->load->view("dashbord/template/footer");
	}

	public function horarioServicos() {
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->helper('funcoes');

		$this->load->library('session');
		$this->load->model("petshop/servico_model");
		$this->load->model("petshop/informacao_model");
		
		$idusuario = $this->session->idusuario_session;
		$codigoServico = $this->input->post("codigoServico");
		$codigoPetshop = $this->input->post("codigoPetshop");

		$data = $this->input->post("data");

		$servico = $this->servico_model->getServicoPoridServico($codigoServico);
		$petshop = $this->informacao_model->getPetShopPoridPetShop($codigoPetshop);
		$veterinarios = $this->informacao_model->getVeterinariosPoridPetshop($codigoPetshop);


		if (strtotime($data) < strtotime(date('Y-m-d'))) {
			$html = '<p align="center"><img src="'.base_url("assets/personalizado/imagem/artes/sem_horario.jpg").'" class="imagem-fundo-agendamento"></p>
			<p align="center"><font color="black">Não é possivel agendar<br> horario em datas anteriores!</font></p>';

			die(json_encode(array('tipo' => 'alert', 'mensagem' => $html)));
		}

		# Montar o horario de acordo com os serviços
		if ($data == date('Y-m-d') && (strtotime(date('H:i:s').' + 4 hours') >= strtotime("08:00"))) {
			$horarios = montarHorario(horarioServidor(), $petshop['horarioFechamento'], $servico['tempo']);	
		} else {
			$horarios = montarHorario($petshop['horarioAbertura'], $petshop['horarioFechamento'],  $servico['tempo']);
		}
		
		$totalFuncionario = count($veterinarios);
		$funcionarioOcupado = 1;

		# Horario Reservados dos veterinarios
		foreach ($veterinarios as $keyVet => $veterinario) {
			$horarioReservados = $this->servico_model->getServicoReservadoPorIdServico($veterinario['idveterinario'], $data);

			if ($horarioReservados) {
				foreach ($horarioReservados as $horarioReservado) {
					foreach ($horarios as $key => $horario) {
						if (strtotime($servico['tempo']) != strtotime($horarioReservado['tempo'])) {
							if ($horario == $horarioReservado['hora']) {
								
								$intervaloTempo = calcularHorario($horario, $servico['tempo']);
								
								foreach ($horarios as $key => $horario) {
									if (strtotime($horario) == strtotime($intervaloTempo)) {
										$funcionarioOcupado++;
										# Remover o horario todos os funcionarios estiver com o horario ocupado
										if ($funcionarioOcupado == $totalFuncionario) {
											unset($horarios[$key]);	
										}
									}
								}
							}	
						} else {

							if (strtotime($horario) == strtotime($horarioReservado['hora'])) {
								$funcionarioOcupado++;
								# Remover o horario todos os funcionarios estiver com o horario ocupado							
								if ($funcionarioOcupado == $totalFuncionario) {
									unset($horarios[$key]);	
								}
							}
						}
					}
				}	
			}	
		}		

		$totalVeterinario = count($veterinarios);

		# Retorno de hoararios
		$dados["horarios"] = $horarios;
		# Escolher aleatoriamente o veterinario para o atendimento;
		$posicaoVeterinario = rand(0, ($totalVeterinario - 1));
		$dados["veterinario"]['codigoVeterinario'] = $veterinarios[$posicaoVeterinario]['idveterinario'];
		
		die(json_encode(array("tipo" => "success", "html" => $this->load->view("dashbord/template/agendar_modal", $dados, true))));

	}

	public function servico($codigoServico) {
		$this->load->helper("url");
		$this->load->helper("funcoes");
		$this->load->helper("form");
		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model("localizacao_model");
		$this->load->model("petshop/informacao_model");
		$this->load->model("petshop/servico_model");
		$this->load->model("pets_model");

		
		$dados["idusuario"] = $this->session->idusuario_session;
		$dados["nome"] = $this->session->nome_session;
		
		$usuario = $this->usuario_model->buscarUsuario($dados["idusuario"]);

		if ($usuario["localizacao"] == 1) {
			redirect(base_url("localizacao"));
		} 

		$servico = $this->servico_model->getServicoPoridServico($codigoServico);
		$servico["imagem"] = verificarFotoServico($servico["fk_idTipoServico"]);
		$servico["preco"] = formatarMonetario($servico["preco"]);
		$servico["tempo"] = padronizaHorario($servico['tempo']);
		
		$petshop = $this->informacao_model->getPetShopPoridPetShop($servico['fk_idPetshop']);

		$dados["dadosusuario"] = $usuario;
		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($dados["idusuario"]);
		$dados["veterinarios"] = $this->informacao_model->getVeterinariosPoridPetshop($servico['fk_idPetshop']);

		$petshop["fotoPrincipal"] = verificarFotoPetshop($petshop["fotoPrincipal"]);
		
		$pets = $this->pets_model->buscarPets($dados["idusuario"]);
		foreach ($pets as $key => $pet) {
			$pets[$key]["fotoPrincipal"] = verificarFotoAnimal($pet["fotoPrincipal"]);	
		}
		$dados["urlPetShop"] = base_url("estabelecimento/".$petshop['codigoPetshop']);
		$dados["servico"] = $servico;
		$dados["petshop"] = $petshop;
		$dados["pets"] = $pets;

		if (empty($this->session->usuario_session)) {
			redirect(base_url('inicio'));
		}

		$dados["scripts"] = array("assets/personalizado/js/cliente/donosPet.js","assets/personalizado/js/maps_busca.js");	
		$dados['dispositivos'] = reconhecerDispositivo();

		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/servico");
		$this->load->view("dashbord/template/footer");
	}

	public function agendarServico() {
		$this->load->helper("url");
		$this->load->library("form_validation");
		$this->load->library('session');
		$this->load->model("petshop/servico_model");
		$this->load->model("carrinho_model");

		$this->form_validation->set_rules("codigoPet", "animal", "required");
		$this->form_validation->set_rules("data", "data", "required");
		$this->form_validation->set_rules("horario", "horario", "required");

		if ($this->form_validation->run() == false) {
			echo json_encode(array("tipo" => "alert", "campos"=> $this->form_validation->error_array()));
		} else {
			$dados = array(
				"data"=> $this->input->post("data"),
				"hora" => $this->input->post("horario"),
				"fk_idServico" => $this->input->post("codigoServico"),
				"fk_idAnimal" => $this->input->post("codigoPet"),
				"fk_idUsuario" => $this->session->idusuario_session,
				"fk_idVeterinario" => $this->input->post("codigoFuncionario"),
				"preReserva" => '1'
			);

			$codigoReserva = $this->servico_model->setServicoReservado($dados);

			$dadosCarrinho = array(
				'fk_idUsuario' => $this->session->idusuario_session,
				'fk_idReserva' => $codigoReserva
			);

			$this->carrinho_model->setCarrinho($dadosCarrinho);
			echo json_encode(array("tipo" => "success", "mensagem" => "Servicos agendado com sucesso!"));
		}	
	}

	public function detalharServico() {
		$this->load->model("petshop/servico_model");
		$idServico = $this->input->post("codigoServico");
		$servico = $this->servico_model->getConsultaPetShopPorId($idServico);

		echo json_encode(array("tipo"=>"success", "descricao" => $servico["descricao"]));
	}

	public function verificaAvaliacaoServico() {
		$this->load->library('session');
		$this->load->model('servico_model');

		$servicos = $this->servico_model->getServicoRealizadosPorIdUsuario($this->session->idusuario_session);

		foreach ($servicos as $servico) {
			$avaliacao = $this->servico_model->getAvaliacaoServicoPorIdUsuario($servico['fk_idUsuario'], $servico['idReservaServico']);

			if (!$avaliacao) {
				$texto = "Dê de 1 até 5 estrelas para o servico ". $servico['nomeServico'] ." realizado pelo estabelecimento ".
				 $servico['nomePetshop'] .", no valor de R$ ". formatarMonetario($servico['preco'])." em ".$servico['nomeAnimal'].".";

				die(json_encode(array(
					'tipo' => 'alert',
					'codigoPetshop' => $servico['idpetshop'],
					'codigoReservaServico' => $servico['idReservaServico'],
					'texto' => $texto,
					'avaliado' => false
				)));
			}
		}
	}

	public function salvarAvaliacao() {
		$this->load->library('session');
		$this->load->model('servico_model');

		$dados = array(
			'comentario' => $this->input->post('comentario'),
			'nota' => $this->input->post('nota'),
			'fk_idReservaServico' => $this->input->post('codigoReservaServico'),
			'fk_idPetshop' => $this->input->post('codigoPetshop'),
			'fk_idUsuario' =>  $this->session->idusuario_session
		);

		$this->servico_model->setAvaliacao($dados);

		echo json_encode(array('tipo' => 'success', 'mensagem'=> 'avaliacao salva com sucesso'));
	}

	public function solicitarCancelamentoReservaServico() {
		$this->load->model("petshop/servico_model");
		$this->load->library('session');
		$idReserva = $this->input->post("codigoReserva");
		$servico = $this->servico_model->getServicoIdReservaServico($idReserva);
		
		$dados = array(
			"situacao" => '2'
		);

		$notificacao = array(
			"mensagem" => "Você possui uma solicitação de cancelamento de serviço",
			"fk_idUsuario"=> $this->session->idusuario_session,
			"fk_idPetshop" => $servico["codigoPetshop"],
			"fk_idAgendamento" => $idReserva
		);

		$this->servico_model->updateServicoReservado($dados, $idReserva);
		$this->servico_model->insertNotificacaoServico($notificacao);
		echo json_encode(array("tipo" => "success", "mensagem" => "Cancelamento solicitado com sucesso!"));
	}
}