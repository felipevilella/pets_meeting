<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacina extends CI_Controller {
	
	public function verificarServicoVacina() {
		$this->load->library('session');
		$this->load->model('cartao_vacina');
		if (empty($this->session->usuario_session)) {
			redirect(base_url('inicio'));
		}
		$idPets = $this->input->post("codigoAnimal");
		$cartaoVacina = $this->cartao_vacina->getCartaoVacinaPorIdPet($idPets);

		if ($cartaoVacina) {
			echo json_encode((array('tipo' => 'success', 'mensagem' => 'cartão vacina  está ativado')));
			
		} else {
			echo json_encode((array('tipo' => 'alert', 'mensagem' => 'cartão vacina não está ativado')));
		}
	}

	function obterPetshop() {
		$this->load->helper('url');
		$this->load->helper("form");
		$this->load->helper("funcoes");		
		
		$this->load->library('session');
		$this->load->model('petshop/servico_model');

		$petshops = $this->servico_model->getServicoPetShopPorIdTipoServico('5');
		$latitude = $this->session->latitude_session;
		$longitude = $this->session->longitude_session;

		foreach ($petshops as $key => $petshop) {
			$petshops[$key]["fotoPrincipal"] = base_url("assets/personalizado/imagem/petshop1.jpg");
			$petshops[$key]["url"] = base_url("petshop/".$petshop["codigoPetshop"]);
			$petshops[$key]["localizacao"] = calcularDistancia($latitude, $longitude, $petshop["latitude"], $petshop["longitude"]);
		}

		$data['petshops'] = $petshops;
		$html = $this->load->view('dashbord/template/listagem_petshop_carta_vacina', $data, true);

		die(json_encode(array("tipo" => 'success', 'html'=> $html)));
	}

	function ativarServico() {
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('cartao_vacina');

		$idPets = $this->input->post("codigoPet");

		$dados = array(
			'fk_idPet' => $idPets
		);

		$this->cartao_vacina->ativarServico($dados);
		die(json_encode(array("tipo"=> 'success', 'mensagem'=> 'serviço ativador com  sucesso!')));
	}
}