<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrinho extends CI_Controller {
	
	public function index() {
		$this->load->library('session');
		$this->load->library('encryption');
		
		$this->load->helper('url');
		$this->load->model("usuario_model");
		$this->load->model("localizacao_model");
		$this->load->model("carrinho_model");
		$this->load->model("banco_model");
		
		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

		$data["idusuario"] = $this->session->idusuario_session;
		$data["nome"] = $this->session->nome_session;
		$data["estados"] = $this->localizacao_model->buscarEstado();

		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$data["informacaoUsuario"]["fotoPrincipal"] = verificarFotoUsuario($data["informacaoUsuario"]["fotoPrincipal"]);
		$data['dispositivos'] = reconhecerDispositivo();
		$data["total"] = 0;

		$dadosCartao = $this->banco_model->getCartaoPorIdUsuario($this->session->idusuario_session);

		if($dadosCartao) {
			$numeroCartao = $this->encryption->decrypt($dadosCartao['numero']);
			$data['numeroCartao'] = substr($numeroCartao,0, 4)." **** **** ".substr($numeroCartao, 12,14);
			$data['codigoCartao'] = $dadosCartao['idcartao'];
		}
		

		$servicosReservados = $this->carrinho_model->getCarrinhoPorIdUsuario($this->session->idusuario_session);

		foreach ($servicosReservados as $key => $servicoReservado) {
			$servicosReservados[$key]["fotoPrincipal"] = verificarFotoServico($servicoReservado["fk_idTipoServico"]);
			$servicosReservados[$key]["hora"] = padronizaHorario($servicoReservado["hora"]);
			$servicosReservados[$key]["data"] = converterData($servicoReservado["data"]);
			$data["total"] = formatarMonetario(doubleval($data['total']) + doubleval($servicoReservado["preco"]));
		}

		$data['servicosReservados'] = $servicosReservados;

		$data["scripts"] = array(
			"assets/personalizado/js/cliente/donosPet.js",
			"assets/personalizado/js/sistema.js"
		);

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/carrinho");
		$this->load->view("dashbord/template/footer");
	}

	public function removerIntem() {
		$this->load->model("petshop/servico_model");
		$this->load->model("carrinho_model");

		$codigo = $this->input->post('codigoReserva');

		$produtoCarrinho = $this->carrinho_model->getprodutoPorid($codigo);
		$this->carrinho_model->deletarProduto($codigo);
		$this->servico_model->deletarServicoReservado($produtoCarrinho['fk_idReserva']);

		echo json_encode(array('tipo' => 'success', 'mensagem' => 'produto excluido com sucesso.'));
	}

	public function adicionarPagamento() {
		$this->load->library('session');
		$this->load->helper('url');

		$this->load->model("usuario_model");
		$this->load->model("localizacao_model");
		$this->load->model("carrinho_model");
		
		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

		$data["idusuario"] = $this->session->idusuario_session;
		$data["nome"] = $this->session->nome_session;
		$data["estados"] = $this->localizacao_model->buscarEstado();

		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$data["informacaoUsuario"]["fotoPrincipal"] = verificarFotoUsuario($data["informacaoUsuario"]["fotoPrincipal"]);
		$data['dispositivos'] = reconhecerDispositivo();


		$data["scripts"] = array(
			"assets/personalizado/js/cliente/donosPet.js",
			"assets/personalizado/js/sistema.js"
		);

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/formaPagamento");
		$this->load->view("dashbord/template/footer");
	}

}