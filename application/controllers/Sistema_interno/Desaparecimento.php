<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desaparecimento extends CI_Controller {

	function index() {

	}

	function obterDesaparecimento() {
		$this->load->helper("funcoes_helper");
		$this->load->library('session');

		$lat = $this->session->latitude_session;
		$lng = $this->session->longitude_session;

		$listaDesparecimento =  montarAnuncioDesaparecimento($lat, $lng);
		echo json_encode($listaDesparecimento);

	}


}