<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapa extends CI_Controller {

	function index() {

	}
	function mapaNovaLocalizacao () {
		$this->load->helper("url");
		$this->load->library('session');
		$this->load->model('usuario_model');

		if (empty($this->session->usuario_session)) {
			redirect(base_url('inicio'));
		}
		$usuario = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$dados['tipoConta'] = $usuario['comercial'];
		$dados["nome"] = $this->session->nome_session;
		$dados["idusuario"] = $this->session->idusuario_session;
		
		$this->load->view("estrutura/header_mapa",$dados);
		$this->load->view("estrutura_interna/mapa");
		$this->load->view("estrutura_interna/maps/footer_geocoder");	
	}
	
}