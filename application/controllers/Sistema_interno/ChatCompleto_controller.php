<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class chatCompleto_controller extends CI_Controller {
	
	public function index() {
		$this->load->helper("url");
		$this->load->view("estrutura/header");
		$this->load->view("estrutura/navbar_usuario");
		$this->load->view("estrutura_interna/chatCompleto");
		$this->load->view("estrutura/footer");
	}

	public function salvar_conversa() {
		$this->load->helper('url');
		$this->load->helper('pusher/pusher');
		$this->load->model("chat_model");
		$this->load->model("usuario_model");
		$this->load->library('session');

		$idusuario =  $this->session->idusuario_session;
		$dadosusuario = $this->usuario_model->buscarUsuario($idusuario);
		$idamigo = $this->input->post("idamigo");
		$mensagem = $this->input->post("mensagem");

		$codigoPrimeiro = $idusuario.$idamigo;
		$codigoSegundo = $idamigo.$idusuario;
		$soma = $codigoPrimeiro+$codigoSegundo;

		$dadosMensagem = array(
			'chatMensagem' => $mensagem,
			'idDestinatario' => $idamigo,
			'fk_idUsuario' => $idusuario
		);

 		# enviar mensagem
		$this->chat_model->enviar_mensagem($dadosMensagem);

 	    # Verificar se possui uma conversa com o usuario
		$conversa = $this->chat_model->buscarUnicaConversa($idamigo,$idusuario);

		if(empty($conversa)) {
			$dadosConversa = array(
				'fk_idUsuario' => $idusuario,
				'id_destinatario' => $idamigo
			); 
 			# salvar conversa
			$this->chat_model->criarConversa($dadosConversa);
			# Obter o id da conversa 
			$conversa = $this->chat_model->buscarUnicaConversa($idamigo,$idusuario);
		} else {
			$dados = array(
				"data" =>  "now()"
			);

			$this->chat_model->atualizarConversa($conversa["id_conversa"],$dados);
		}
		
 		# criar notificação
		$notificacao = array(
			'fk_idUsuario' => $idamigo,
			'fk_idTipoNotificacao' => '1',
			'fk_idConversa'=> $conversa["id_conversa"],
			'idDestinatario' => $idusuario,
		);
		
		$this->chat_model->criarNotificacao($notificacao);

		# ativando o envio do pusher da mensagem
		canalPusher('chat','enviar-mensagem');

	} 

	public function solicitacao_encontro() {
		$this->load->helper('url'); 
		$this->load->model("chat_model");
		$this->load->model("usuario_model");
		$this->load->model("pets_model");
		$this->load->library('session');

		$idusuario =  $this->session->idusuario_session;
		$dadosusuario = $this->usuario_model->buscarUsuario($idusuario);
		$idamigo = $this->input->post("idamigo");
		$mensagem = $this->input->post("mensagem");
		# ID pet do usuario que executou a solicitação
		$idPet = $this->input->post("idPets");
		$idPetamigo = $this->input->post("idPetsAmigo");

		$dadosMensagem = array(
			'chatMensagem' => $mensagem,
			'idDestinatario' => $idamigo,
			'fk_idUsuario' => $idusuario
		);
 		# enviar mensagem
		$this->chat_model->enviar_mensagem($dadosMensagem);


 	    # Verificar se possui uma conversa com o usuario
		$conversa = $this->chat_model->buscarUnicaConversa($idamigo, $idusuario);

		if(empty($conversa)) {
			$dadosConversa = array(
				'fk_idUsuario' => $idusuario,
				'id_destinatario' => $idamigo
			); 
 			# salvar conversa
			$this->chat_model->criarConversa($dadosConversa);
			# Obter o id da conversa 
			$conversa = $this->chat_model->buscarUnicaConversa($idamigo,$idusuario);
		}
		

 		# criar notificação
		$notificacao = array(
			'fk_idUsuario' => $idamigo,
			'fk_idTipoNotificacao' => '2',
			'fk_idConversa'=> $conversa["id_conversa"],
			'idDestinatario' => $idusuario,
		);
		$this->chat_model->criarNotificacao($notificacao);

		#Inserir o id do pet nas lista dos solicitados para que não apareça novamente
		$dadosSolicitacao = array(
			"idPetDestinatario"=> $idPetamigo,
			"fk_idPet" => $idPet
		);
		$this->pets_model->setSolicitacaoPets($dadosSolicitacao);

	}
	public function buscar_conversa(){
		$this->load->helper('url');
		$this->load->model("chat_model");
		$this->load->model("usuario_model");
		$this->load->model("notificacao_model");
		$this->load->library('session');
		

		$dados["idusuario"] =  $this->session->idusuario_session;
		$dados["idamigo"] = $this->input->post("idamigo");
		$dados["dadosuser"] = $this->usuario_model->buscarUsuario($dados["idusuario"]);
		$dados["dadosamigo"] = $this->usuario_model->buscarUsuario($dados["idamigo"]);

		# obtendo as mensagens entre os usuarios 
		$dados["dadosConversa"] = $this->chat_model->buscarMensagem($dados["idusuario"],$dados["idamigo"]);
		$dadosNotificacao = array(
				"ativo" => 0
		);
		
		$this->notificacao_model->atualizarNotificacao($dadosNotificacao, $this->session->idusuario_session , $dados["idamigo"]);

		$html = $this->load->view("templates/chat_conversa",$dados,true);
		echo json_encode(array (
			'permisao' => 'true' ,
			'html' => $html 
		));
	}


}   