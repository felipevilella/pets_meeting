<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends CI_Controller {

	public function __construct() {
		parent ::__construct();

		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->library("session");
		$this->load->library('form_validation');
		$this->load->model("usuario_model");
		$this->load->model('petshop/servico_model');
		$this->load->model('petshop/informacao_model');
		$this->load->model('contato_model');
	}

	public function index() {

		if (!($this->session->usuario_session)) {
			redirect(base_url('inicio'));
		}

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$servicos = $this->servico_model->getServicoPetShopPorId($petshop["idpetshop"]);

		if ($petshop) {
			$data["sobre"] = 1;
		}  else {
			$data["sobre"] = 0;
		}

		foreach ($servicos as $key => $servico) {
			$servicos[$key]["imagem"] = verificarFotoServico($servico["fk_idTipoServico"]);
		}

		$data["servicos"] = $servicos;

		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$data["tipoServicos"] = $this->servico_model->getTipoServico();
		$data["nome"] = $this->session->nome_session;
		$data["idusuario"] = $this->session->idusuario_session;

		$data["scripts"] = array("assets/personalizado/js/cliente/petshop.js");
		$data['paginaLoja'] = base_url('petshop/'.$petshop['idpetshop']);

		$data['dispositivos'] = reconhecerDispositivo();


		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/petshop/servico");
		$this->load->view("dashbord/template/footer");
	}
	
	public function modalSobre() {
		$data["nome"] = $this->session->nome_session;
		$idUsuario = $this->session->idusuario_session;

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$contato = $this->contato_model->getContatoPorIdPetShop($petshop['idpetshop']);
		$contatoCelular = $this->contato_model->getContatoPorIdPetShop($petshop['idpetshop'], '3');

		$data["sobre"] = $this->informacao_model->getSobrePorIdUsuario($idUsuario);
		$data["sobre"]["contato"] = $contato["descricao"];
		$data['sobre']['celular'] = $contatoCelular['descricao'];

		$html = $this->load->view("dashbord/template/sobre_pet_shop_template", $data, true);
		echo json_encode(array("tipo" => "success", "html" => $html));
	}

	public function verificarFuncionarios() {
		$idUsuario = $this->session->idusuario_session;

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$servicos = $this->servico_model->getServicoPetShopPorId($petshop["idpetshop"]);
		$veterinarios = $this->informacao_model->getVeterinariosPoridPetshop($petshop["idpetshop"]);

		if (!$veterinarios) {
			die(json_encode(array('tipo' => 'alert', 'veterinarios' => false)));
		}

		echo json_encode(array('tipo' => 'success', 'veterinarios' => true));
	}

	public function getTipoConsulta() {
		$tipoConsultas = $this->servico_model->getTipoConsulta();
		echo json_encode(array("tipo" => "success", "consultas" => $tipoConsultas));
	}

	public function obterServico() {
		$idServico = $this->input->post("codigoServico");
		$servico = $this->servico_model->getConsultaPetShopPorId($idServico);
		
		echo json_encode(array("tipo" => "success", "servico" => $servico));
	}

	/**
	* Cadastrar os serviços oferecidos pelos petshop
	* @author Felipe Vilella
	* @var int tipoServico
	* @var int nome
	* @var double valor
	* @var String descricao
	* @var array horarios
	**/
	public function salvarServico() {
		$this->form_validation->set_rules("tipoServico", "servico", 'required');
		$this->form_validation->set_rules("nome","nome","required|min_length[3]|max_length[45]");
		$this->form_validation->set_rules("preco","preco","required|max_length[12]");
		$this->form_validation->set_rules("descricao", "descricao", "required|max_length[800]");
		$this->form_validation->set_rules("tempo", "tempo", "required");
		

		if ($this->form_validation->run() == false) {
			echo json_encode(array("tipo" => "alert", "retorno" => $this->form_validation->error_array()));
		} else {
			$tipoServico = $this->input->post("tipoServico");
			$nome = $this->input->post("nome");
			$preco = $this->input->post("preco");
			$descricao = $this->input->post("descricao");
			$tempo = $this->input->post("tempo");

			# Obter dados do petshop
			$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

			$dados = array(
				'descricao' => $descricao,
				'nome'=> $nome,
				'preco' => $preco,
				'fk_idPetshop' => $petshop["idpetshop"],
				'fk_idTipoServico' => $tipoServico,
				'tempo' => $tempo
			);

			$this->servico_model->salvarServicoPetshop($dados);

			echo json_encode(array("tipo"=>"success", "retorno" => "Serviço cadastrado com sucesso."));
		}
	}

	public function alterarServico() {
		$this->form_validation->set_rules("preco","preco","required|max_length[12]");
		$this->form_validation->set_rules("descricao", "descricao", "required|max_length[800]");	

		if ($this->form_validation->run() == false) {
			echo json_encode(array("tipo" => "alert", "retorno" => $this->form_validation->error_array()));
		} else {

			$idServico = $this->input->post("codigoServico");
			$preco = $this->input->post("preco");
			$descricao = $this->input->post("descricao");
			$horarios = $this->input->post("horarios");

			$dados = array('descricao' => $descricao, 'preco' => $preco);

			$this->servico_model->updateServicoPorId($dados, $idServico);

			echo json_encode(array("tipo"=>"success", "retorno" => "Serviço alterado com sucesso."));
		}
	}

	public function salvarSobre() {
		$this->load->library('MY_Form_validation');

		$this->form_validation->set_rules("cnpj", "cnpj", "required|validar_cnpj");
		$this->form_validation->set_rules("razaoSocial", "razão social", "required|min_length[3]|max_length[100]");
		$this->form_validation->set_rules("nome", "nome", "required|min_length[3]|max_length[100]");
		$this->form_validation->set_rules("abertura", "horario de abertura", "required");
		$this->form_validation->set_rules("fechamento", "horario de fechamento", "required");
		$this->form_validation->set_rules("descricao", "descricao", "required|min_length[3]|max_length[300]");
		$this->form_validation->set_rules("telefone", "telefone", "required");
		$this->form_validation->set_rules("celular", "celular", "required|valid_phone");

		if ($this->form_validation->run() == false) {
			echo json_encode(array("tipo" => "alert", "retorno" => $this->form_validation->error_array()));
		} else {
			$idUsuario = $this->session->idusuario_session;

			$dados = array(
				"cnpj"=> $this->input->post("cnpj"),
				"razaoSocial" => $this->input->post('razaoSocial'), 
				"nome" => $this->input->post("nome"),
				"descricao" => $this->input->post("descricao"),
				"horarioAbertura" => $this->input->post("abertura"),
				"horarioFechamento" => $this->input->post("fechamento"),
			);
			
			$dadosContato = array(
				'descricao' => $this->input->post("telefone"),
				'fk_tipoContato' => '1'
			);


			$sobre = $this->informacao_model->getSobrePorIdUsuario($idUsuario);

			if (!$sobre) {
				$dados["fk_idUsuarioPetshop"] = $idUsuario;
				$this->informacao_model->setSobre($dados);
				
				$petshop = $this->informacao_model->getSobrePorIdUsuario($idusuario);
				$dadosContato["fk_idPetshop"] = $petshop['idpetshop'];
				$this->contato_model->insertContato($dadosContato);

				# Salvar celular
				$dadosContato = array(
					'descricao' => $this->input->post("celular"),
					'fk_tipoContato' => '3'
				);

				$dadosContato["fk_idPetshop"] = $petshop['idpetshop'];
				$this->contato_model->insertContato($dadosContato);

				# Salvar celular
				$dadosContato = array(
					'descricao' => $this->input->post("celular"),
					'fk_tipoContato' => '3'
				);

				$dadosContato["fk_idPetshop"] = $petshop['idpetshop'];
				$this->contato_model->insertContato($dadosContato);

			} else {
				$this->informacao_model->updateSobrePorIdUsuario($dados, $idUsuario);
				$petshop = $this->informacao_model->getSobrePorIdUsuario($idUsuario);

				$contato = array('ativo' => '0');
				$this->contato_model->updateContatoPorIdPetShop($contato, $petshop['idpetshop']);

				$dadosContato["fk_idPetshop"] = $petshop['idpetshop'];
				$this->contato_model->insertContato($dadosContato);
			}

			echo json_encode(array("tipo"=> "success", "mensagem" => "Sobre atualizado com sucesso"));
		}	
	}

	public function registrarVeterinario() {
		$this->form_validation->set_rules("nome", "nome", "required|min_length[3]|max_length[45]");

		if (!$this->form_validation->run()) {
			echo json_encode(array("tipo" => 'alert', 'campos' => $this->form_validation->error_array()));	
		} else {
			$idUsuario = $this->session->idusuario_session;
			$petshop = $this->informacao_model->getSobrePorIdUsuario($idUsuario);

			if ($petshop) {
				$dados = array(
					'nome' => $this->input->post("nome"),
					'fk_idPetShop' => $petshop['idpetshop']
				);

				$this->informacao_model->setVeterinario($dados);
				echo json_encode(array("tipo" => "success", "mensagem" => "Cadastro realizado com sucesso!"));
			}
		}
	}

	public function funcionarios() {
		$idUsuario = $this->session->idusuario_session;
		$petshop = $this->informacao_model->getSobrePorIdUsuario($idUsuario);

		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$data["nome"] = $this->session->nome_session;
		$data["idusuario"] = $this->session->idusuario_session;

		$veterinarios = $this->informacao_model->getVeterinariosPoridPetshop($petshop["idpetshop"]);
		$data["veterinarios"] = $veterinarios;
		$data["scripts"] = array("assets/personalizado/js/cliente/petshop.js");
		$data['dispositivos'] = reconhecerDispositivo();

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/petshop/funcionario");
		$this->load->view("dashbord/template/footer");
	}

	public function deletarVeterinario() {
		$idVeterinario = $this->input->post("codigoVeterinario");
		$dados = array("ativo" => 0);

		$this->informacao_model->updateFuncionario($dados, $idVeterinario);
		echo json_encode(array("tipo"=>"success", "retorno" => "Veterinario excluido com sucesso."));
	}

	public function editarFuncionario() {
		$idVeterinario = $this->input->post("codigoVeterinario");
		$veterinario = $this->informacao_model->getVeterinarioPorId($idVeterinario);
		echo json_encode(array("tipo" => "success", "veterinario" => $veterinario));
	}

	public function atualizarFuncionario() {
		$this->form_validation->set_rules("nome", "nome", "required|min_length[3]|max_length[45]");
		$this->form_validation->set_rules("horarios[]", "horario", "required");

		if (!$this->form_validation->run()) {
			echo json_encode(array("tipo" => 'alert', 'campos' => $this->form_validation->error_array()));	
		} else {
			$idUsuario = $this->session->idusuario_session;
			$petshop = $this->informacao_model->getSobrePorIdUsuario($idUsuario);
			$horarios = $this->input->post("horarios");
			$idVeterinario = $this->input->post("codigoVeterinario");


			if ($petshop) {
				$dados = array(
					'nome' => $this->input->post("nome"),
					'ra' => $this->input->post('ra')
				);

				$horariosVeterinario = array();
				foreach ($horarios as $key => $horario) {
					$horariosVeterinario[$key]["fk_idVeterinario"] = $idVeterinario;
					$horariosVeterinario[$key]["fk_idHorario"] = $horario;
				}

				$this->informacao_model->updateVeterinario($dados, $idVeterinario);
				$this->servico_model->deletarHorarioServicosPorIdVeterinario($idVeterinario);
				$this->servico_model->inserirHorarioServico($horariosVeterinario);

				echo json_encode(array("tipo" => "success", "mensagem" => "Atualizado veterinario com sucesso!"));
			}
		}
	}

	public function deletarServico() {
		$idServico = $this->input->post("codigoServico");
		$dados = array("ativo" => 0);

		$this->servico_model->updateServicoPorId($dados, $idServico);
		
		echo json_encode(array("tipo"=>"success", "retorno" => "Serviço excluido com sucesso."));
	}
}