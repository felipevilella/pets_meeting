<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacina extends CI_Controller {

	public function index() {
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model('petshop/servico_model');
		$this->load->model('petshop/informacao_model');
		$this->load->model('cartao_vacina');

		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$dados["nome"] = $this->session->nome_session;
		$dados["idusuario"] = $this->session->idusuario_session;
		$dados['dispositivos'] = reconhecerDispositivo();

		# Obter dados do petshop
		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$servicos = $this->servico_model->getServicoPetShopPorId($petshop["idpetshop"]);

		if ($petshop) {
			# obter clientes dos cartões de vacina 
			$cartoesVacina = $this->cartao_vacina->getCartaoVacinaPorIdPetShop($petshop['idpetshop'], '1');

			# Verificar o seviços de pethop
			$servicos = $this->servico_model->getServicoPetShopPorId($petshop["idpetshop"], true);
			$dados["perfilPropio"] = true;
			$dados["cartoesVacina"] = $this->padronizarCartaoVacina($cartoesVacina);
			$dados['dispositivos'] =  reconhecerDispositivo();

			$dados["estruturaCartaoVacina"] = $this->load->view("dashbord/template/listagem_cartao_vacina", $dados, true);
			$dados['paginaLoja'] = base_url('petshop/'.$petshop['idpetshop']);

			foreach ($servicos as $servico) {
				if ($servico['fk_idTipoServico'] == '5') {
					$dados["possuiVacina"] = true;
				}
			}
		} 

		$dados["scripts"] = array("assets/personalizado/js/cliente/petshop.js");
		$dados['dispositivos'] = reconhecerDispositivo();


		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/petshop/vacina");
		$this->load->view("dashbord/template/footer");
	} 

	/**
	* @author Felipe Vilella
	* 
	**/
	public function analisaServicoAtivo() {
		$this->load->library('session');
		$this->load->model('petshop/servico_model');
		$this->load->model('petshop/informacao_model');

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$servicos = $this->servico_model->getServicoPetShopPorId($petshop["idpetshop"], true);

		$possueVacina = false;

		foreach ($servicos as $servico) {
			if ($servico['fk_idTipoServico'] == '5') {
				$possueVacina = true;
			}
		}

		if ($possueVacina) {
			echo json_encode(array('tipo' => 'success' , 'mensagem' => 'Serviço de vacina ativa'));
		} else {
			echo json_encode(array('tipo' => 'alert' , 'mensagem' => 'Serviço de vacina não está ativo'));
		}
	}
	
	/**
	* @author Felipe Vilella 
	**/	
	public function ativarServicoVacina() {
		$this->load->library('session');
		$this->load->model('petshop/servico_model');
		$this->load->model('petshop/informacao_model');

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		
		$dados = array(
			'fk_idpetshop' => $petshop['idpetshop'],
			'fk_idTipoServico' => '5',
			'nome' => 'Cartão de vacina virtual',
			'descricao' => 'Reserver o seu cartão de vacina do seu animal de estimação neste petshop',
			'preco'  => '0'
		);

		$this->servico_model->salvarServicoPetshop($dados);
		echo json_encode(array("tipo" => "success", 'mensagem' => 'Vacina ativada com sucesso'));
	}

	/**
	* @author Felipe Vilella
	* 
	**/
	public function buscarCartaoVacina() {
		$this->load->helper('url');
		$this->load->library('session');

		$this->load->model('cartao_vacina');
		$this->load->model('petshop/servico_model');
		$this->load->model('petshop/informacao_model');

		$nome = $this->input->post("nome");
		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

		$cartoesVacina = $this->cartao_vacina->getCartaoVacinaPorIdPetShop($nome);

		$dados["cartoesVacina"] = $this->padronizarCartaoVacina($cartoesVacina);
		$dados['dispositivos'] =  reconhecerDispositivo();

		$html = $this->load->view("dashbord/template/listagem_cartao_vacina", $dados, true);

		echo json_encode(array("tipo" => "success", 'html' => $html));
	}

	public function detalharCartaoVacina($idCartaoVacina) {
		$this->load->library('session');

		$this->load->model("usuario_model");
		$this->load->model('cartao_vacina');
		$this->load->model('petshop/informacao_model');

		$this->load->helper('url');
		$this->load->helper('funcoes');
		$this->load->helper("form");

		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$dados["nome"] = $this->session->nome_session;
		$dados["idusuario"] = $this->session->idusuario_session;
		$dados["codigoCartao"] = $idCartaoVacina;
		$dados['codigoPetshop'] = $petshop["idpetshop"];

		$cartaoVacina = $this->cartao_vacina->getCartaoVacinaPorId($idCartaoVacina);
		if ($cartaoVacina["situacao"] == '1') {
			$this->cartao_vacina->updateCartaoVacina(array("situacao" => '2'), $idCartaoVacina);
		}

		$cartaoVacina["fotoUsuario"] = verificarFotoUsuario($cartaoVacina["fotoUsuario"]);
		$cartaoVacina["fotoAnimal"] = verificarFotoAnimal($cartaoVacina["fotoAnimal"]);

		$historicoVacinas = $this->cartao_vacina->getAgendamentoPoridCartaoVacina($idCartaoVacina, 1);
		
		# Formatar horarios 
		foreach ($historicoVacinas as $key => $vacina) {
			$historicoVacinas[$key]["hora"] = padronizaHorario($vacina["hora"]);
			$historicoVacinas[$key]["data"] = converterData($vacina["data"]);
		}

		$historicoVacinas["vacinas"] = $historicoVacinas;
		$historicoVacinas['codigoPetshop'] = $dados['codigoPetshop'];
		$historicoVacinas['dispositivos'] =  reconhecerDispositivo();

		$dados["cartaoVacina"] = $cartaoVacina;
		$dados["vacinas"] = $this->cartao_vacina->getVacinas();
		$dados["scripts"] = array("assets/personalizado/js/cliente/petshop.js");
		$dados['dispositivos'] = reconhecerDispositivo();
		
		if($petshop) {
			$dados['paginaLoja'] = base_url('petshop/'.$petshop['idpetshop']);
			$dados["veterinarios"] = $this->informacao_model->getVeterinariosPoridPetshop($petshop["idpetshop"]);
			$dados["historicoVacinas"] = $this->load->view("dashbord/template/listagem_consultas_vacinas_petshop", $historicoVacinas, true);
		} else {
			$dados["historicoVacinas"] = $this->load->view("dashbord/template/listagem_consultas_vacinas_donosAnimais",$historicoVacinas, true);
		}

		

		$this->load->view("dashbord/template/header", $dados);
		$this->load->view("dashbord/template/navbar");
		
		if ($dados["informacaoUsuario"]['comercial'] == '1') {
			$this->load->view("dashbord/petshop/detalhar_vacina");
		} else {
			$this->load->view("dashbord/cartaoVirtual");
		}
		
		$this->load->view("dashbord/template/footer");
	}

	public function listarVacina() {
		$this->load->helper('url');
		$this->load->library('session');
		
		$this->load->model('cartao_vacina');
		$this->load->model('petshop/informacao_model');

		$this->load->helper('funcoes');
		$this->load->helper("form");
		
		$codigoCartao = $this->input->post("codigoCartao");
		$situacao = $this->input->post('situacao');

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

		$historicoVacinas = $this->cartao_vacina->getAgendamentoPoridCartaoVacina($codigoCartao, $situacao);
		# Formatar horarios 
		foreach ($historicoVacinas as $key => $vacina) {
			$historicoVacinas[$key]["hora"] = padronizaHorario($vacina["hora"]);
			$historicoVacinas[$key]["data"] = converterData($vacina["data"]);
		}

		$historicoVacinas["vacinas"] = $historicoVacinas;
		
		if($petshop) {
			$historicoVacinas['codigoPetshop'] =  $petshop["idpetshop"];
		}
		
		$historicoVacinas['dispositivos'] =  reconhecerDispositivo();
		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		
		if($petshop) {
			$html = $this->load->view("dashbord/template/listagem_consultas_vacinas_petshop",$historicoVacinas, true);;
		} else {
			$html = $this->load->view("dashbord/template/listagem_consultas_vacinas_donosAnimais",$historicoVacinas, true);
		}
		

		echo json_encode(array("tipo" => 'success', 'html' => $html));
	}

	public function detalharConsultaVacina() {
		$this->load->helper('funcoes');
		$this->load->helper("form");

		$this->load->model('cartao_vacina');
	
		$codigoVacina = $this->input->post("codigoVacina");
		
		$historicoVacina = $this->cartao_vacina->getConsultaVacinaPorId($codigoVacina);
		$historicoVacina["hora"] = padronizaHorario($historicoVacina["hora"]);
		$historicoVacina["data"] = converterData($historicoVacina["data"]);
		
		$dados["consulta"] = $historicoVacina;
		$html = $this->load->view("dashbord/template/detalheConsultaVacina", $dados, true);	
		echo json_encode(array("tipo" => "success", "html" => $html));
	}

	public function atualizarConsultaVacina() {
		$this->load->model('cartao_vacina');

		$vacina = array("status" => $this->input->post("situacao"));
		$codigoVacina = $this->input->post("codigoVacina");

		$this->cartao_vacina->updateConsultaVacina($vacina, $codigoVacina);
		echo json_encode(array("tipo"=>"success", "mensagem" => "Vacina atualizada com sucesso"));
	}

	public function verificarHorarioDisponivel() {
		$this->load->library("form_validation");

		$this->load->helper("form");
		$this->load->helper('funcoes');

		$this->load->model("petshop/informacao_model");
		$this->load->model("petshop/servico_model");
		$this->load->model("cartao_vacina");

		$data = $this->input->post("data");
		$codigoPetshop = $this->input->post('codigoPetshop');

		$petshop = $this->informacao_model->getPetShopPoridPetShop($codigoPetshop);

		if (strtotime($data) < strtotime(date('Y-m-d'))) {
			die(json_encode(array('tipo' => 'alert', 'mensagem' => 'Não é possivel agendar horario em datas anteriores.')));
		}

		$servico['tempo'] = "00:20:00";

		# Montar o horario de acordo com os serviços
 		if ($data == date('Y-m-d') && (strtotime(date('H:i:s').' + 4 hours') >= strtotime("08:00"))) {
			$horarios = montarHorario(horarioServidor(), $petshop['horarioFechamento'], $servico['tempo']);	
		} else {
			$horarios = montarHorario($petshop['horarioAbertura'], $petshop['horarioFechamento'],  $servico['tempo']);
		}
		
		$veterinarios = $this->informacao_model->getVeterinariosPoridPetshop($codigoPetshop);		
		$totalFuncionario = count($veterinarios);
		$funcionarioOcupado = 1;

		# Horario Reservados dos veterinarios
		foreach ($veterinarios as $keyVet => $veterinario) {
			$horarioReservados = $this->servico_model->getServicoReservadoPorIdServico($veterinario['idveterinario'], $data);

			if ($horarioReservados) {
				foreach ($horarioReservados as $horarioReservado) {
					foreach ($horarios as $key => $horario) {
						if (strtotime($servico['tempo']) != strtotime($horarioReservado['tempo'])) {
							if ($horario == $horarioReservado['hora']) {
								
								$intervaloTempo = calcularHorario($horario, $servico['tempo']);
								
								foreach ($horarios as $key => $horario) {
									if (strtotime($horario) == strtotime($intervaloTempo)) {
										$funcionarioOcupado++;
										# Remover o horario todos os funcionarios estiver com o horario ocupado
										if ($funcionarioOcupado == $totalFuncionario) {
											unset($horarios[$key]);	
										}
									}
								}
							}	
						} else {

							if (strtotime($horario) == strtotime($horarioReservado['hora'])) {
								$funcionarioOcupado++;
								# Remover o horario todos os funcionarios estiver com o horario ocupado							
								if ($funcionarioOcupado == $totalFuncionario) {
									unset($horarios[$key]);	
								}
							}
						}
					}
				}	
			}	
		}		

		$totalVeterinario = count($veterinarios);

		# Retorno de hoararios
		if ($horarios) {
			$dados["horarios"] = $horarios;
			# Escolher aleatoriamente o veterinario para o atendimento;
			$posicaoVeterinario = rand(0, ($totalVeterinario - 1));
			$dados["veterinario"]['codigoVeterinario'] = $veterinarios[$posicaoVeterinario]['idveterinario'];
			
			die(json_encode(array("tipo" => "success", "horarios" => $this->load->view("dashbord/template/agendar_modal", $dados, true))));
		} 
		
		die(json_encode(array("tipo" => "alert", "mensagem" => "Desculpe, o dia selecionado não possui reservas disponiveis")));

	}

	public function agendarVacina(){
		$this->load->library("form_validation");
		$this->load->library('session');

		$this->load->model("petshop/informacao_model");
		$this->load->model("cartao_vacina");

		$this->form_validation->set_rules("vacina", "vacina", "required");
		$this->form_validation->set_rules("codigoFuncionario", "funcionario", "required");
		$this->form_validation->set_rules("data", "data", "required");
		$this->form_validation->set_rules("horario", "horario", "required");
		$this->form_validation->set_rules("valor", "valor", "required|min_length[1]|max_length[12]");

		if (!$this->form_validation->run()) {
			die(json_encode(array("tipo"=> "alert", "campos" => $this->form_validation->error_array())));
		}

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

		$agendamento = array(
			"fk_idPetshop" => $petshop["idpetshop"],
			"fk_idVeterinario" => $this->input->post('codigoFuncionario'),
			"fk_idTipoVacina" => $this->input->post("vacina"),
			"hora" => $this->input->post("horario"),
			"fk_idCartaoVacina" => $this->input->post("codigoCartao"),
			"data" => $this->input->post("data"),
			"valor" => $this->input->post("valor")
		);
		
		$this->cartao_vacina->setCartaoVacina($agendamento);
		echo json_encode(array("tipo" => "success", "mensagem" => "serviço agendado com sucesso!"));
	}


	/**
	* @author Felipe Vilella
	* 
	**/
	private function padronizarCartaoVacina($cartoesVacina) {
		$this->load->helper('funcoes_helper');

		if ($cartoesVacina) {
			foreach ($cartoesVacina as $key => $cartaoVacina) {		
				$cartoesVacina[$key]["fotoUsuario"] = verificarFotoUsuario($cartaoVacina["fotoUsuario"]);
				$cartoesVacina[$key]["fotoAnimal"] = verificarFotoAnimal($cartaoVacina["fotoAnimal"]);
				$cartoesVacina[$key]["url"] = 'detalharCartaoVacina/'.$cartaoVacina["codigoCartao"];
			}
		} 
		return $cartoesVacina;
	}
}