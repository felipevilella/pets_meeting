<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atendimento extends CI_Controller {

	public function index() {
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model('petshop/informacao_model');
		
		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}
		
		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$dados["nome"] = $this->session->nome_session;
		$dados["idusuario"] = $this->session->idusuario_session;
		
		$dados["scripts"] = array(
			"assets/personalizado/js/cliente/petshop.js"
		);

		$dados['paginaLoja'] = base_url('petshop/'.$petshop['idpetshop']);
		$dados['dispositivos'] = reconhecerDispositivo();

		$semana = filtrarDiaSemanaExtenso(date("w"));
		$dias = array();

		foreach ($semana as $key => $dia) {
			if($key == 0) {
				array_push($dias, array('data' => date('Y/m/d'), 'dia' => "Hoje" ));
			} else {
				array_push($dias, array('data' =>  date('Y/m/d', strtotime("+$key days", strtotime(date('Y-m-d')))), 'dia' => $dia ));
			}
		}

		$dados['dias'] = $dias;

		$this->load->view("dashbord/template/header",$dados);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/petshop/atendimentos");
		$this->load->view("dashbord/template/footer");
	} 

	public function buscarAtendimento() {
		$this->load->library('session');
		$this->load->model('petshop/servico_model');
		$this->load->model('petshop/informacao_model');

		$this->load->helper('url');
		$this->load->helper("form");
		$this->load->helper("funcoes");

		$data = $this->input->post('data');
		$situacao = $this->input->post("situacao");

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$atendimentos = $this->servico_model->getServicosReservadoPorSituacaoData($petshop["idpetshop"], $data, $situacao);

		if ($atendimentos) {
			$dataAtual = date("Y/m/d");

			foreach ($atendimentos as $key => $atendimento) {
				if (strtotime($dataAtual) == strtotime($atendimento["data"])) {
					$atendimentos[$key]["data"] = "Hoje";
				} else {
					$atendimentos[$key]["data"] = conveterData($atendimento["data"]);
				}
				
				$atendimentos[$key]["hora"] = padronizaHorario($atendimento["hora"]);
				$atendimentos[$key]["fotoPrincipal"] = verificarFotoUsuario($atendimento["fotoPrincipal"]);
				$atendimentos[$key]["fotoPrincipalAnimal"] = verificarFotoAnimal($atendimento["fotoPrincipalAnimal"]);

				if ($atendimento["fk_idTipoServico"] == 3) {
					$atendimentos[$key]["imagem"] = base_url("assets/personalizado/imagem/veterinario.jpg");
				} else if ($atendimento["fk_idTipoServico"] == 2) {
					$atendimentos[$key]["imagem"] = base_url("assets/personalizado/imagem/banhoTosa.jpg");
				} else {
					$atendimentos[$key]["imagem"] = base_url("assets/personalizado/imagem/banho.jpg");
				}
			}
		}

		$dados["atendimentos"] = $atendimentos;
		$dados['dispositivos'] =  reconhecerDispositivo();

		$html = $this->load->view("dashbord/template/atendimento_template", $dados, true);
		echo json_encode(array("tipo" => "success", "html" => $html));
	}

	public function detalharAtendimento() {
		$this->load->model('petshop/servico_model');
		$this->load->model('notificacao_model');
		$this->load->helper('filtro');
		$this->load->helper('url');
		$this->load->helper("form");
		$this->load->helper("funcoes");

		$idReservaServico = $this->input->post("codigoAtendimento");

		$dados = array(
			"leitura" => '0'
		);
		$this->notificacao_model->atualizarNotificacaoPorIdAgendamento($idReservaServico, $dados);

		$atendimento = $this->servico_model->getServicosReservadoPorid($idReservaServico);
		$dataAtual = date("Y/m/d");

		if (strtotime($dataAtual) == strtotime($atendimento["data"])) {
			$atendimento["data"] = "Hoje";
		} else {
			$atendimento["data"] = conveterData($atendimento["data"]);
		}

		$atendimento["hora"] = padronizaHorario($atendimento["hora"]);
		$atendimento["fotoPrincipal"] = base_url("assets/personalizado/foto_usuario/".$atendimento["fotoPrincipal"]);
		$atendimento["fotoPrincipalAnimal"] = base_url("assets/personalizado/fotos_pets/".$atendimento["fotoPrincipalAnimal"]);

		if ($atendimento["fk_idTipoServico"] == 3) {
			$atendimentos["imagem"] = base_url("assets/personalizado/imagem/veterinario.jpg");
		} else if ($atendimento["fk_idTipoServico"] == 2) {
			$atendimento["imagem"] = base_url("assets/personalizado/imagem/banhoTosa.jpg");
		} else {
			$atendimento["imagem"] = base_url("assets/personalizado/imagem/banho.jpg");
		}

		$atendimento["status"] = filtroStatusServico($atendimento["situacao"]);

		$data['atendimento'] = $atendimento; 
		$html = $this->load->view("dashbord/template/detalheAtendimento", $data, true);

		echo json_encode(array(
			"tipo" => "success", 
			"html" => $html, 
			"codigoAtendimento" => $idReservaServico, 
			"situacao" => $atendimento['situacao'],
			"titulo" => $atendimento['nomeServico']." - ".$atendimento['categoria'] 
		));
	}

	public function atualizarAtendimeto() {
		$this->load->model("petshop/servico_model");
		$this->load->helper("pagamento/pagar_me_helper");

		$idReservaServico = $this->input->post("codigoAtendimento");
		$status = $this->input->post("status");

		$dados["situacao"] = $status;

		if ($status == 3) {
			$dados["ativo"] = 0;
			$servico = $this->servico_model->getServicoIdReservaServico($idReservaServico);
			estorno($servico['token_transacao']);
		}

		$this->servico_model->updateServicoReservado($dados, $idReservaServico);
		echo json_encode(array("tipo" => "success", "mensagem" => "atendimento atualizado com sucesso!"));

	}

}