<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagamento extends CI_Controller {
	public function index() {

	}

	public function VerificarDadosPagamento() {
		$this->load->model("petshop/informacao_model");
		$this->load->model('banco_model');
		$this->load->library('session');

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$dadosBancarios = $this->banco_model->buscarDadosBancarios($petshop['idpetshop']);

		

		if(!$dadosBancarios)  {
			die(json_encode(array("tipo" => "alert", "pagamento" => false)));
		} 

		echo json_encode(array("tipo" => "alert", "pagamento" => true));
	}

	public function gerarTokenSMS() {
		$this->load->library('session');
		$this->load->helper('sms/nexmo_sms');
		$this->load->model('sms_model');
		$this->load->model('contato_model');
		$this->load->model('petshop/informacao_model');

		$codigo = rand(1000,9999);
		$nomeUsuario = $this->session->nome_session;
		$mensagem = "Ola+$nomeUsuario,+o+seu+codigo+de+autenticacao+e:+$codigo";
		$idUsuario = $this->session->idusuario_session;

		$petshop = $this->informacao_model->getSobrePorIdUsuario($idUsuario);
		$contato = $this->contato_model->getContatoPorIdPetShop($petshop['idpetshop']);
		$telefone = "55".removerCaracterNaoNumericos($contato['descricao']);

		$dados = array(
			'codigo' => $codigo,
			'fk_idUsuario' => $idUsuario
		);
		//$this->sms_model->setSMS($dados);
		//enviar_sms($nomeUsuario, $mensagem, $telefone);

		echo json_encode(array('tipo' => 'success', 'mensagem'=> 'codigo gerado com sucesso!'));
	}
	
	public function salvarDadosBancario() {
		$this->load->helper('url');
		$this->load->helper('pagamento/pagar_me');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model('petshop/informacao_model');
		$this->load->model('banco_model');
		$this->load->model('sms_model');

		
		$this->form_validation->set_rules('agencia','agencia','required|max_length[5]|numeric');
		$this->form_validation->set_rules('digitoAgencia','digito agência', 'required|max_length[4]|numeric');
		$this->form_validation->set_rules('banco', 'banco','required');
		$this->form_validation->set_rules('numeroConta', 'numero conta', 'required|max_length[9]|numeric');
		$this->form_validation->set_rules('digitoConta', 'digito conta', 'required|min_length[1]|max_length[3]|numeric');
		$this->form_validation->set_rules('tipoConta', 'tipo conta', 'required');

		if (!$this->form_validation->run()) {
			die(json_encode(array('tipo' => 'alert', 'campos' => $this->form_validation->error_array())));
		}

		$codigo = $this->input->post('codigo');
		$sms = $this->sms_model->getSMS($this->session->idusuario_session);

		if($sms['codigo']!= $codigo) {
			die(json_encode(array('tipo' => 'alert', 'mensagem' => 'O codigo informado está invalido!')));
		} else {
			$this->sms_model->deletarSMS($this->session->idusuario_session);
			$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
			$dadosBancarios = $this->banco_model->buscarDadosBancarios($petshop['idpetshop']);

			$dados = array(
				'agencia'=> $this->input->post('agencia'),
				'digitoAgencia' => $this->input->post('digitoAgencia'),
				'codigoBanco' => $this->input->post('banco'),
				'conta' => $this->input->post('numeroConta'),
				'digitoConta' => $this->input->post('digitoConta'),
				'tipoConta' => $this->input->post('tipoConta'),
				'fk_idPetshop' => $petshop['idpetshop'],
				'CNPJ' => $petshop['cnpj'],
				'nomeEmpresa' => $petshop['razaoSocial']
			);

			if ($dadosBancarios) {
				# Cadastrar dados bancarios na API pagar.me
				$dadosBancariosPagarme = cadastrarContaBancaria($dados);

				# Atualizar dados bancarios pagar.me
				$retornoDadosBancarios = atualizarRecebedor($dadosBancariosPagarme['idDadosBancario'], $dadosBancarios['idRecebedor']);
				
				if($retornoDadosBancarios['tipo'] == "success") {
					unset($dados['CNPJ']);
					unset($dados['nomeEmpresa']);

					$dados['idInformacaoBancaria'] = $dadosBancariosPagarme['idDadosBancario'];
					$this->banco_model->updateDadosBancarios($dados, $petshop['idpetshop']);
					
					die(json_encode(array('tipo' => 'success', 'mensagem' => 'Dados atualizado com sucesso!')));
				} else {
					die(json_encode(array('tipo' => 'alert', 'mensagem' => $retornoDadosBancarios['mensagem'])));
				}
			} else {
				# Cadastrar dados bancarios na API pagar.me
				$dadosBancariosPagarme = cadastrarContaBancaria($dados);

				if ($dadosBancariosPagarme['tipo'] == 'success') {
					$dados['idInformacaoBancaria'] = $dadosBancariosPagarme['idDadosBancario'];

					# Cadastrar fornecedor bancarios na API pagar.me
					$dadosRecebedorPagarme = cadastrarRecebedor($dadosBancariosPagarme['idDadosBancario'], '5');
					
					if ($dadosRecebedorPagarme['tipo'] == 'success') {
						$dados['idRecebedor'] = $dadosRecebedorPagarme['idRecebedor'];
						$dados['tipoConta'] = filtrarTipoconta($dados['tipoConta']);
						
						unset($dados['CNPJ']);
						unset($dados['nomeEmpresa']);

						$this->banco_model->setDadosBancario($dados);
						die(json_encode(array('tipo' => 'success', 'mensagem' => 'Dados cadastrado com sucesso!')));
					} else {
						die(json_encode(array('tipo' => 'alert', 'mensagem' => 'Ops, não foi possivel cadastrar a conta bancaria, tente mais tarde')));
					}
				} else {
					die(json_encode(array('tipo' => 'alert', 'mensagem' => 'Erro ao cadastrar a conta bancaria')));
				}
			}
		}
	}
}
?>