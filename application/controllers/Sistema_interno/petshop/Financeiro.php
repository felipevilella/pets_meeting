<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro extends CI_Controller {

	public function index() {
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->helper("funcoes");
		$this->load->helper('pagamento/pagar_me');

		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model("petshop/servico_model");
		$this->load->model("notificacao_model");
		$this->load->model("petshop/informacao_model");
		$this->load->model("localizacao_model");
		$this->load->model('banco_model');

		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

		$usuario = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

		if ($petshop) {
			$data["sobre"] = 1;
		}  else {
			$data["sobre"] = 0;
		}


		$data["fluxoCaixa"] = 0;
		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$data["nome"] = $this->session->nome_session;
		$data["idusuario"] = $this->session->idusuario_session;
		$dataAtual = date("d-m-Y");

		$petshop = $this->informacao_model->getSobrePorIdUsuario($data["idusuario"]);
		
		if ($petshop) {
			$agendamentos = $this->servico_model->getServicosReservadoPorIdPetShop($petshop['idpetshop']);

			foreach ($agendamentos as $key => $agendamento) {
				if($agendamento["situacao"] != 0) {
					if ($agendamento["situacao"] == 1) {
						$data["fluxoCaixa"] += $agendamento["preco"];
					} 
					// Remove atendimente que não estão pendentes
					unset($agendamentos[$key]);
				} else {

					if (strtotime($dataAtual) == strtotime($agendamento["data"])) {
						$agendamentos[$key]["data"] = "Hoje";
					} else {
						$agendamentos[$key]["data"] = conveterData($agendamento["data"]);
					}

					$agendamentos[$key]["hora"] = padronizaHorario($agendamento["hora"]);
					$agendamentos[$key]["fotoPrincipal"] = verificarFotoUsuario($agendamento["fotoPrincipal"]);
					$agendamentos[$key]["fotoPrincipalAnimal"] = verificarFotoAnimal($agendamento["fotoPrincipalAnimal"]);
					
					if ($agendamento["fk_idTipoServico"] == 3) {
						$agendamentos[$key]["imagem"] = base_url("assets/personalizado/imagem/veterinario.jpg");
					} else if ($agendamento["fk_idTipoServico"] == 2) {
						$agendamentos[$key]["imagem"] = base_url("assets/personalizado/imagem/banhoTosa.jpg");
					} else {
						$agendamentos[$key]["imagem"] = base_url("assets/personalizado/imagem/banho.jpg");
					}	
				}
			}

			$clientes = $this->servico_model->getClienteServicoPorIdPetShop($petshop['idpetshop']);
			foreach ($clientes as $key => $cliente) {
				$clientes[$key]["fotoPrincipal"] = base_url("assets/personalizado/foto_usuario/".$cliente["fotoPrincipal"]);
			}


			$data["fluxoCaixa"] = formatarMonetario($data["fluxoCaixa"]);
			$data["agendamentos"] = $agendamentos; 
			$data["clientes"] = $clientes;
			$data["TotalAgendamento"] = count($agendamentos);
			$data['paginaLoja'] = base_url('petshop/'.$petshop['idpetshop']);
		} else {
			$data["agendamentos"] = ""; 
			$data["clientes"] = ""; 
			$data["TotalAgendamento"] = ""; 
			$data['paginaLoja'] = "";

		}

		$data['bancos'] = $this->banco_model->getBancos();
		$data['dadosBancarios'] = $this->banco_model->buscarDadosBancarios($petshop['idpetshop']);
		$data["enderecos"] = $this->localizacao_model->getEnderecosPorIdUsuario($this->session->idusuario_session);
		$data['valorPagamento'] = verificarSaldo($data['dadosBancarios']['idRecebedor']);
		$veterinarios = $this->informacao_model->getVeterinariosPoridPetshop($petshop["idpetshop"]);
		$data["veterinarios"] = $veterinarios;


		$data["scripts"] = array(
			"assets/personalizado/js/cliente/petshop.js"
		);

		$data['dispositivos'] = reconhecerDispositivo();

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/petshop/gestao");
		$this->load->view("dashbord/template/footer");
	}
}