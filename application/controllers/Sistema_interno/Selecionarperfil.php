<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class selecionarperfil extends CI_Controller {
	
	public function index() {
		$this->load->helper("url");
		$this->load->library('session');
		$this->load->model("pets_model");
		$this->load->model("usuario_model");
		$this->load->model("chat_model");

		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}
		$dadosUsuario = $this->usuario_model->buscarUsuario($this->session->idusuario_session);

		$dados["cidade"] = $dadosUsuario["nomeCidade"];
		$dados["estado"] = $dadosUsuario["nomeEstado"];
		$dados["nome"] = $this->session->nome_session;
		$dados["idusuario"] = $this->session->idusuario_session;
		$dados["dadosPets"] = $this->pets_model->buscarPets($dados["idusuario"]);
		$dados["racaPets"] = $this->pets_model->racaoPets();
		$dados['tipoAnimais'] = $this->pets_model->getTipoAnimal();

		$dados["contadornotificacao"] = $this->chat_model->mostrarContadorDeMensagem($dados["idusuario"]);
		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($dados["idusuario"]);

		# Obtendo a notificação de mensagem
		$conversaChat = $this->chat_model->buscarConversa($this->session->idusuario_session);
		$contador = 0;
		$conversaUsuario = array();

		foreach ($conversaChat as $conversa) {
			# Obtendo dados do destinatario 
			$dadosDestinatario = $this->usuario_model->buscarUsuario($conversa["fk_idUsuario"]);
			# Inserindo os dados na conversa
			$conversaUsuario[$contador]["foto"] = $dadosDestinatario["fotoPrincipal"];
			$conversaUsuario[$contador]["nome"] = $dadosDestinatario["nome"];
			$conversaUsuario[$contador]["id_conversa"] = $conversa["id_conversa"];
			$conversaUsuario[$contador]["id_destinatario"] = $conversa["id_destinatario"];
			$conversaUsuario[$contador]["fk_idUsuario"] = $conversa["fk_idUsuario"];

			$contador++;
		}

		if($conversaUsuario){
			$dados["listaMensagem"] = $conversaUsuario;
		}

		$dados["scripts"] = array("assets/personalizado/js/cliente/donosPet.js");
		$dados['dispositivos'] = reconhecerDispositivo();
		
		$this->load->view("dashbord/template/header",$dados);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/selecionarPerfilPets");
		$this->load->view("dashbord/template/footer");
	}

	public function obterRacaAnimal() {
		$this->load->model("pets_model");

		$codigoTipoAnimal = $this->input->post('codigoTipoAnimal');
		$racaPets = $this->pets_model->racaoPets($codigoTipoAnimal);

		echo json_encode(array('tipo' => 'success', 'racas' => $racaPets));
	}

	public function cadastrarPets() {
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model("pets_model");
		$this->load->library('session');

		$this->form_validation->set_rules("nome","nome","required|min_length[3]|max_length[45]");
		$this->form_validation->set_rules("ano","ano","required");
		$this->form_validation->set_rules("sexo","sexo","required");
		$this->form_validation->set_rules("raca","raca","required");
	
		if ($this->form_validation->run() == false ) {
			echo json_encode(array("tipo" => "alert", "campos"=> $this->form_validation->error_array()));
		} else {
			$dadosPets = array(
				'nome' => $this->input->post("nome") ,
				'ativo' => '1',
				'ano_nascimento'=> $this->input->post("ano"),
				'sexo'=> $this->input->post("sexo"),
				'fk_idUsuario'=> $this->session->idusuario_session,
				'fk_idTipoAnimal'=> $this->input->post('tipoAnimal'),
				'fk_idraca' => $this->input->post("raca")
			);
			$this->pets_model->cadastro_pets($dadosPets);
			echo json_encode(array("tipo" => "success", "mensagem"=>"Pets cadastrado com sucesso"));
		}
	}
}   