<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	public function index() {

	}

	public function informacaoPessoais() {
		$this->load->helper('url');
		$this->load->model("usuario_model");
		$this->load->model('petshop/informacao_model');
		$this->load->model("contato_model");
		$this->load->library('session');
		
		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

	
		$data["idusuario"] = $this->session->idusuario_session;
		$data['contato'] = $this->contato_model->getContatoUsuario($data["idusuario"]);
		$data["nome"] = $this->session->nome_session;
		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$data['dispositivos'] = reconhecerDispositivo();
	
		$data["scripts"] = array(
			"assets/personalizado/js/cliente/donosPet.js",
			"assets/personalizado/js/sistema.js",
			"assets/personalizado/js/cliente/petshop.js"
		);

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/informacaoPessoais");
		$this->load->view("dashbord/template/footer");
	}

	public function salvarFotoUsuario() {

		$this->load->helper("url");
		$this->load->helper("pusher/pusher");
		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model("pets_model");

		$url = $_SERVER['HTTP_REFERER'];

		if (preg_match("/perfilpets/", $url)) {
			$url = $_SERVER['HTTP_REFERER'];
			$urlSeparada = explode("/", $url);

			$idpets = end($urlSeparada);
			$dadosPets = $this->pets_model->buscarPetsPorIdPets($idpets);
		}
		$dados["nome"] = $this->session->nome_session;
		$dados["idusuario"] = $this->session->idusuario_session;


		if (empty($this->session->usuario_session)) {
			redirect(base_url('inicio'));
		}

		if (preg_match("/perfilpets/", $url)) {
			$config['upload_path'] = 'assets/personalizado/fotos_pets';
		} else {
			$config['upload_path'] = 'assets/personalizado/foto_usuario';
		}

		
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 200000;
        $config['max_width'] = 2024;
        $config['max_height']  = 1768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('fotousuarioperfil')){

			$nome = $_FILES['fotousuarioperfil'];
			$dadosFoto = array('fotoPrincipal' => $nome["name"]);

			if (preg_match("/perfilpets/", $url)) {
				$config['source_image'] = "./assets/personalizado/fotos_pets/".$dadosFoto["fotoPrincipal"].'.jpg';
			} else {
				$config['source_image'] = "./assets/personalizado/foto_usuario/".$dadosFoto["fotoPrincipal"].'.jpg';
			}

			$config['create_thumb'] = TRUE;
			$cofig['maintain_ratio'] = TRUE;
			$config['width'] = 800;
			$config['height']  = 800;

			$this->upload->initialize($config);

			$error = array('error' => $this->upload->display_errors());

			if (preg_match("/perfilpets/", $url)) {
				$this->pets_model->alterarPerfilPets($dadosFoto, $idpets);
			} else {
				$this->usuario_model->alterarPerfilusuario($dadosFoto, $dados["idusuario"]);
			}
			
			canalPusher('pets-meeting-'.$dados["idusuario"], "atualizar-foto-perfil", "Foto atualizada com sucesso");
			die(json_encode(array("tipo" => 'success', 'mensagem' => 'Foto atualizada com sucesso!')));
		 
		} else{
			$mensagem = $this->upload->display_errors();
			if(preg_match("/O tipo de arquivo que você está tentando fazer upload não é permitido/", $mensagem)) {
				$mensagem = "O formato da imagem não é permitido, insira uma nova foto no formato JPG ou PNG";
			}
			
			die(json_encode(array("tipo" => 'alert', 'mensagem' => $mensagem)));
		}

	}

	public function obterFotoUsuario() {
		$this->load->helper("url");
		$this->load->model("usuario_model");
		$this->load->library('session');
		$usuario = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$usuario["fotoPrincipal"] = verificarFotoUsuario($usuario["fotoPrincipal"]);

		echo json_encode(array('tipo' => 'success', 'foto' => $usuario['fotoPrincipal']));
	}

	public function verificarFoto() {
		$this->load->helper("url");
		$this->load->model("usuario_model");
		$this->load->library('session');
		$usuario = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		
		if ($usuario["fotoPrincipal"]) {
			die(json_encode(array('tipo' => 'success', 'foto' => true)));
		} 

		echo json_encode(array('tipo' => 'alert', 'foto' => false));
	}


	public function obterFotoPets() {
		$this->load->helper("url");
		$this->load->model("usuario_model");
		$this->load->model("pets_model");

		$url = $_SERVER['HTTP_REFERER'];
		$urlSeparada = explode("/", $url);

		$idpets = end($urlSeparada);
		$dadosPets = $this->pets_model->buscarPetsPorIdPets($idpets);
		$dadosPets["fotoPrincipal"] = verificarFotoAnimal($dadosPets["fotoPrincipal"]);

		echo json_encode(array('tipo' => 'success', 'foto' => $dadosPets['fotoPrincipal']));
	}

	public function modalSobre() {
		$this->load->library('session');
		$this->load->helper("form");
		$this->load->model("usuario_model");
		$this->load->model("contato_model");

		$data["sobre"]["nome"] = $this->session->nome_session;
		$idUsuario = $this->session->idusuario_session;

		$contato = $this->contato_model->getContatoUsuario($idUsuario);
		$data["sobre"]["contato"] = $contato["descricao"];

		$html = $this->load->view("dashbord/template/sobre_usuario", $data, true);
		echo json_encode(array("tipo" => "success", "html" => $html));
	}

	public  function atualizarSobre() {
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model("usuario_model");
		$this->load->model("contato_model");
		
		$telefone = $this->input->post("telefone");
		$this->form_validation->set_rules("nome", 'nome', 'required|max_length[45]');

		if (!$this->form_validation->run()) {
			echo json_encode(array("tipo"=>"alert", "campos" => $this->form_validation->error_array()));
		}

		$dados = array("nome" => $this->input->post("nome"));

		# Atualizar o perfil
		$this->usuario_model->alterarPerfilusuario($dados, $this->session->idusuario_session);

		if ($telefone) {
			$contato = $this->contato_model->getContatoUsuario($this->session->idusuario_session);
			
			if ($contato) {
				$this->contato_model->updateContatoPorUsuario(array("ativo" => 0), $this->session->idusuario_session);
			}

			$dadosContato = array(
				"fk_idUsuario" => $this->session->idusuario_session,
				"descricao" => $telefone,
				"fk_tipoContato" => '1'
			);


			$this->contato_model->insertContato($dadosContato);
			echo json_encode(array("tipo"=>"success", "campos" => "Atualizado com sucesso"));
		}
	}

	public function deslogarUsuario(){
		$this->load->helper("url");
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url("login"));
	}

}   