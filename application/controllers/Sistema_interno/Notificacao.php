<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacao extends CI_Controller {

	function notificacaoUsuario() {
		$this->load->helper("url");
		$this->load->library('session');
		$this->load->model("pets_model");
		$this->load->model("Notificacao_model");
	
		# Verifica se existe a sessão
		if (empty($this->session->usuario_session)) {
			redirect('login');
		}

		$notificacao = $this->Notificacao_model->buscarNotificacao($this->session->idusuario_session);
		$dados["notificacao"] = $notificacao;
		
		echo json_encode($dados);
	}

	public function notificarPetshop() {
		$this->load->helper("url");
		$this->load->library('session');
		$this->load->model("notificacao_model");
		$this->load->model("petshop/informacao_model");

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);
		$notificacoes = $this->notificacao_model->getNotificacaoPorIdPetshop($petshop["idpetshop"]);
		$totalCancelamento = 0;

		foreach ($notificacoes as $notificacao) {
			if(preg_match("/cancelamento de serviço/", $notificacao["mensagem"])) {
				$totalCancelamento +=1;
			}
		}

		if ($totalCancelamento !=0) {
			if ($totalCancelamento == 1) {
				$mensagem = "Você possui ".$totalCancelamento." solicitação de cancelamento.";
			} else {
				$mensagem = "Você possui ".$totalCancelamento." solicitações de cancelamento.";
			}
			
		}

		echo json_encode(array("tipo" => "success", "mensagem" => $mensagem));
	}
}