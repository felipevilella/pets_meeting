<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Endereco extends CI_Controller {

	function __construct() {
	    parent::__construct();
		$this->load->model("localizacao_model");
	    $this->load->library('session');
	 }

	public function index() {
		$this->load->helper('url');
		$this->load->model("usuario_model");
		$this->load->model('petshop/informacao_model');
		
		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}

		$data["idusuario"] = $this->session->idusuario_session;
		$data["nome"] = $this->session->nome_session;
		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$data['dispositivos'] = reconhecerDispositivo();
		$data["enderecos"] = $this->localizacao_model->getEnderecosPorIdUsuario($this->session->idusuario_session);

		$petshop = $this->informacao_model->getSobrePorIdUsuario($this->session->idusuario_session);

		if ($petshop) {
			$data['paginaLoja'] = base_url('petshop/'.$petshop['idpetshop']);
		}
		

		$data["scripts"] = array("assets/personalizado/js/cliente/donosPet.js", "assets/personalizado/js/sistema.js");

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/endereco");
		$this->load->view("dashbord/template/footer");
	}

	public function verificarEndereco() {
		$endereco = $this->localizacao_model->getEnderecosPorIdUsuario($this->session->idusuario_session);
			
		if (!$endereco) {
			$enderecoAtivo = false; 
		} else {
			$enderecoAtivo = true;
		}

		echo json_encode(array('tipo' => 'success', 'enderecoAtivo' => $enderecoAtivo));
	}

	public function excluirEndereco() {
		if ($this->input->post('codigoEndereco')) {
			$this->localizacao_model->deletarEndereco($this->input->post('codigoEndereco'));
			die(json_encode(array("tipo" => 'success', 'message' => 'endereço excluido com sucesso')));
		}

		echo json_encode(array("tipo" => 'success', 'message' => 'Não foi possivel excluir o endereço.'));
	}

	public function selecionarEndereco() {
		if ($this->input->post('codigoEndereco')) {
			$this->localizacao_model->atualizarEnderecoPorIdUsuario($this->session->idusuario_session, array('principal' => '0'));
			$this->localizacao_model->atualizarEndereco($this->input->post('codigoEndereco'), array('principal' => '1'));

			$endereço = $this->localizacao_model->getEnderecoPorId($this->input->post('codigoEndereco'));
			$enderecoMaps = tirarAcentos($endereço['rua']).", ".$endereço['numero']." , ".tirarAcentos($endereço['bairro']).", ".removerCaracterNaoNumericos($endereço['cep']);
			
			$localizacao = obterLocalizacaoPorEndereco($enderecoMaps);
			salvarLocalizacao($localizacao->lat, $localizacao->lng);

			die(json_encode(array("tipo" => 'success', 'message' => 'endereço selecionado com sucesso')));
		}

		echo json_encode(array("tipo" => 'success', 'message' => 'Não foi possivel selecionar o endereço.'));
	}

	public function salvarEndereco() {
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('cep', 'cep', 'required|min_length[7]');
		$this->form_validation->set_rules('bairro','bairro','required');
		$this->form_validation->set_rules('rua','rua', 'required');

		if (!$this->form_validation->run()) {
			die(json_encode(array("tipo" => 'alert', "campos" =>  $this->form_validation->error_array())));
		}

		$dados = array(
			'cep' => $this->input->post('cep'),
			'bairro' => $this->input->post('bairro'),
			'rua' => $this->input->post('rua'),
			'fk_idUsuario'=> $this->session->idusuario_session,
			'fk_idCidade' => filtrarCidade($this->input->post('cidade')),
			'fk_idEstado' => filtrarEstado($this->input->post('estado'))
		);

		if ($this->input->post('numero')) {
			$dados['numero'] = $this->input->post('numero');
			$enderecoMaps = tirarAcentos($dados['rua']).", ".$dados['numero']." , ".tirarAcentos($dados['bairro']).", ".removerCaracterNaoNumericos($dados['cep']);
		} else {
			$enderecoMaps = tirarAcentos($dados['rua']).", ".tirarAcentos($dados['bairro']).", ".removerCaracterNaoNumericos($dados['cep']);
		}

		$localizacao = obterLocalizacaoPorEndereco($enderecoMaps);
		salvarLocalizacao($localizacao->lat, $localizacao->lng);

		$enderecos =  $this->localizacao_model->getEnderecosPorIdUsuario($this->session->idusuario_session);

		if ($enderecos) {
			$this->localizacao_model->atualizarEnderecoPorIdUsuario($this->session->idusuario_session, array('principal' => '0'));
		}

		if(count($enderecos) < 4) {
			if ($this->input->post('codigoEndereco')) {
				$this->localizacao_model->atualizarEndereco($this->input->post('codigoEndereco'),$dados);
			} else {
				$this->localizacao_model->setEndereco($dados);
			}
			
			echo json_encode(array("tipo" => 'success', 'message' => 'endereço salvo com sucesso'));
		} else {
			echo json_encode(array("tipo" => 'alert', "limite" => 'Limite de endereço cadastrado foi atigindo!'));
		}

		
	}
}

?>