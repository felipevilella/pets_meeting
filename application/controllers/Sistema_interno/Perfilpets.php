<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfilpets extends CI_Controller {

	public function index(){

	}
	
	public function verPerfil($idpets) {
		$this->load->helper("url");
		$this->load->helper("funcoes_helper");

		$this->load->library('session');

		$this->load->model("pets_model");
		$this->load->model("usuario_model");
		$this->load->model("chat_model");
		$this->load->model("cartao_vacina");
		$this->load->model("petshop/servico_model");

		if (empty($this->session->usuario_session)) {
			redirect(base_url('inicio'));
		}

		$dados["idpets"] = $idpets;
		$dados["nome"] = $this->session->nome_session;
		$dados["idusuario"] = $this->session->idusuario_session;
		$dadosPets = $this->pets_model->buscarPetsPorIdPets($idpets);

		if ($dadosPets["fotoPrincipal"]) {
			$dadosPets["fotoPrincipal"] = base_url("assets/personalizado/fotos_pets/".$dadosPets["fotoPrincipal"]);
		} else {
			$dadosPets["fotoPrincipal"] = base_url("assets/personalizado/imagem/addFoto.jpg");
		}	
	
		$dados["dadosPets"] = $dadosPets;
		$dados["dadosusuario"] = $this->usuario_model->buscarUsuario($dados["idusuario"]);
		$dados["buscarFotosPets"] = $this->pets_model->buscaFotosPets($idpets);
		$dados["contadornotificacao"] = $this->chat_model->mostrarContadorDeMensagem($dados["idusuario"]);
		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($dados["idusuario"]);
		$dados["racaPets"] = $this->pets_model->racaoPets($dadosPets['fk_idTipoAnimal']);
		$dados["informacaoUsuario"] = $this->usuario_model->buscarUsuario($dados["idusuario"]);
		
		$cartaoVacina = $this->cartao_vacina->getCartaoVacinaPorIdPet($idpets);

		if ($cartaoVacina) {
			$dados["urlCartao"] = base_url('detalharCartaoVacina/'.$cartaoVacina["idCartao"]);
		}

		$dados["scripts"] = array(
			"assets/personalizado/js/cliente/donosPet.js",
		 	"assets/personalizado/js/sistema.js"
		);

		$consultas = $this->servico_model->getServidoReservadoPorIdAnimal($idpets, array(0,2));

		foreach ($consultas as $key => $consulta) {
			$consultas[$key]["imagem"] = verificarFotoServico($consulta["fk_idTipoServico"]);
			$consultas[$key]["hora"] = padronizaHorario($consulta["hora"]);
			$consultas[$key]["dataMes"] = converterDataMes($consulta["data"]);
			$consultas[$key]["data"] = converterData($consulta["data"]);
			$consultas[$key]["dataAtual"] = date("d-m-Y");
			$consultas[$key]['diaSemana'] = filtrarDiaSemana(date("w", strtotime($consulta["data"])));
		}

		$data["consultas"] = $consultas;
		$data["dadosPets"] = $dadosPets;
		$dados["consultas"] = $this->load->view("dashbord/template/listagem_consultas_templates", $data, true);
		$dados['dispositivos'] = reconhecerDispositivo();

		$this->load->view("dashbord/template/header",$dados);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/perfilPets");
		$this->load->view("dashbord/template/footer");
	}



	public function alterarPets() {
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model("pets_model");
		$this->load->model("pets_model");
		$this->load->library('session');

		if (empty($this->session->usuario_session)) {
			redirect('inicio');
		}

		$this->form_validation->set_rules("nome","nome","required|min_length[3]|max_length[45]");
		$this->form_validation->set_rules("ano","ano","required");

		if($this->form_validation->run() == false ){
			die(json_encode($this->form_validation->error_array()));
		}
		
		$idpets = $this->input->post("codigoPet");

		$dadosPets = array(
			'nome' => $this->input->post("nome"),
			'ano_nascimento'=> $this->input->post("ano"),
			'fk_idraca' => $this->input->post('raca'),
			'sexo' => $this->input->post("sexo"),
			'descricao' => $this->input->post("descricao")
		);

		$this->pets_model->alterarPerfilPets($dadosPets, $idpets);
		echo json_encode(array("mensagem"=>"Pets cadastrado com sucesso"));
	}

	public function obterConsultas() {
		$this->load->model("petshop/servico_model");
		$this->load->model("pets_model");
		$this->load->helper("form");
		$this->load->helper("funcoes");

		$this->load->helper("url");

		$idAnimal = $this->input->post("codigoAnimal");
		$situacao = $this->input->post("situacao");
		$consultas = $this->servico_model->getServidoReservadoPorIdAnimal($idAnimal, $situacao);

		foreach ($consultas as $key => $consulta) {
			$consultas[$key]["imagem"] = verificarFotoServico($consulta["fk_idTipoServico"]);
			$consultas[$key]["hora"] = padronizaHorario($consulta["hora"]);
			$consultas[$key]["dataMes"] = converterDataMes($consulta["data"]);
			$consultas[$key]["data"] = converterData($consulta["data"]);
			$consultas[$key]["dataAtual"] = date("d-m-Y");
			$consultas[$key]['diaSemana'] = filtrarDiaSemana(date("w", strtotime($consulta["data"])));
		}
		
		$data["consultas"] = $consultas;
		$data["dadosPets"] = $this->pets_model->buscarPetsPorIdPets($idAnimal);
		$html = $this->load->view("dashbord/template/listagem_consultas_templates", $data, true);
		echo json_encode(array("tipo"=>"success", "html" => $html));
		
	}

	public function salvarDesaparecimento() {
		$this->load->model("desaparecimento_model");
		$this->load->model("pets_model");
		
		$data = $this->input->post("data");
		$texto = $this->input->post("texto");
		$ultimaLocalizacao = $this->input->post("ultimaLocalizacao");
		$idpet = $this->input->post("idpet");

		$dadosDesaparecimento = array(
			"texto" => $texto,
			"data" => $data,
			"fk_idPets"=> $idpet,
			"ultimoLugar"=>$ultimaLocalizacao 
		); 

		$this->desaparecimento_model->salvarDesaparecimento($dadosDesaparecimento);
	}
}   