<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesquisa extends CI_Controller {
	
	public function index() {

	}

	public function listarServico($codigoServico = false) {
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->helper("funcoes");
		
		$this->load->library('session');

		$this->load->model("pets_model");
		$this->load->model("usuario_model");
		$this->load->model("localizacao_model");
		$this->load->model("petshop/informacao_model");
		$this->load->model("chat_model");
		$this->load->model("servico_model");

		if(empty($this->session->usuario_session)){
			redirect(base_url('inicio'));
		}
		$data["idusuario"] = $this->session->idusuario_session;
		$data["nome"] = $this->session->nome_session;
		$data["estados"] = $this->localizacao_model->buscarEstado();

		$usuario = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$latitude = $this->session->latitude_session;
		$longitude = $this->session->longitude_session;

		$data["dadosusuario"] = $usuario;
		$data["estado"] = $this->localizacao_model->buscarEstado();

		$data["contadornotificacao"] = $this->chat_model->mostrarContadorDeMensagem($data["idusuario"]);
		$data["informacaoUsuario"] = $this->usuario_model->buscarUsuario($data["idusuario"]);
		$data["endereco"] = $this->localizacao_model->getEnderecoPorIdUsuario($data["idusuario"]);

		$enderecoMaps = tirarAcentos($data["endereco"]['rua']).", ".$data["endereco"]['numero']." , ".tirarAcentos($data["endereco"]['bairro']).", ".removerCaracterNaoNumericos($data["endereco"]['cep']);
		$enderecoMaps = obterLocalizacaoPorEndereco($enderecoMaps);

		$data["enderecos"] = $this->localizacao_model->getEnderecosPorIdUsuario($data["idusuario"], true);

		if($codigoServico) {
			$servicos = $this->servico_model->getServicos($codigoServico, $data["endereco"]['fk_idEstado'],  $data["endereco"]['fk_idCidade']);

			if(!$servicos) {
				$servicos = $this->servico_model->getServicos($codigoServico, $data["endereco"]['fk_idEstado'], false);	
			}

		} else {
			$servicos = $this->servico_model->getServicos(false, $data["endereco"]['fk_idEstado'],  $data["endereco"]['fk_idCidade']);

			if(!$servicos) {
				$servicos = $this->servico_model->getServicos(false, $data["endereco"]['fk_idEstado'], false);	
			}
		}
		

		foreach ($servicos as $key => $servico) {
			$servicos[$key]["fotoPrincipal"] = verificarFotoServico($servico["tipoServico"]);
			$servicos[$key]["horarioAbertura"] = padronizaHorario($servico["horarioAbertura"]);
			$servicos[$key]["horarioFechamento"] = padronizaHorario($servico["horarioFechamento"]);

			
			$servicos[$key]["localizacao"] = calcularDistancia($enderecoMaps->lat, $enderecoMaps->lng, $servico["latitude"], $servico["longitude"]);

			$servicos[$key]["url"] = base_url("reservarServico/".$servico["codigoServico"]);

			if($servicos[$key]['localizacao'] > 10) {
				unset($servicos[$key]);
			}
		}


		$dados["servicos"] = $servicos;
		$data["petshops"] = $this->load->view("dashbord/template/listar_estabelecimento_petshop", $dados, true);
		$data["servicos"] = $this->servico_model->getTipoServico();

		$data["scripts"] = array("assets/personalizado/js/cliente/donosPet.js");

		$data['dispositivos'] = reconhecerDispositivo();

		$this->load->view("dashbord/template/header",$data);
		$this->load->view("dashbord/template/navbar");
		$this->load->view("dashbord/pesquisa");
		$this->load->view("dashbord/template/footer");
	} 

	public function buscarPetshop() {
		$this->load->helper('url');
		$this->load->helper("form");
		$this->load->helper("funcoes");

		$this->load->library('session');
		$this->load->model("usuario_model");
		$this->load->model("petshop/informacao_model");
		$this->load->model("localizacao_model");
		$this->load->model("servico_model");

		$usuario  = $this->usuario_model->buscarUsuario($this->session->idusuario_session);
		$latitude = $this->session->latitude_session;
		$longitude = $this->session->longitude_session;

		$endereco = $this->localizacao_model->getEnderecoPorIdUsuario($this->session->idusuario_session);
		$enderecoMaps = tirarAcentos($endereco['rua']).", ".$endereco['numero']." , ".tirarAcentos($endereco['bairro']).", ".removerCaracterNaoNumericos($endereco['cep']);

		$enderecoMaps = obterLocalizacaoPorEndereco($enderecoMaps);
		$codigoServico = $this->input->post("codigoServico");
		$servicos = $this->servico_model->getServicos($codigoServico, $endereco['fk_idEstado'],$endereco['fk_idCidade']);

		if(!$servicos) {
			$servicos = $this->servico_model->getServicos($codigoServico, $endereco['fk_idEstado'], false);	
		}

		foreach ($servicos as $key => $servico) {
			$servicos[$key]["fotoPrincipal"] = verificarFotoServico($servico["tipoServico"]);
			$servicos[$key]["horarioAbertura"] = padronizaHorario($servico["horarioAbertura"]);
			$servicos[$key]["horarioFechamento"] = padronizaHorario($servico["horarioFechamento"]);

			
			$servicos[$key]["localizacao"] = calcularDistancia($enderecoMaps->lat, $enderecoMaps->lng, $servico["latitude"], $servico["longitude"]);

			$servicos[$key]["url"] = base_url("reservarServico/".$servico["codigoServico"]);

			if($servicos[$key]['localizacao'] > 10) {
				unset($servicos[$key]);
			}
		}

		$dados["servicos"] = $servicos;

		$html = $this->load->view("dashbord/template/listar_estabelecimento_petshop", $dados, true);	
		echo json_encode(array("tipo"=> "success", "html" => $html));

	}
}  