<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

	public function index(){

	}

	public function validacao_login(){
		$this->load->helper("url");
		$this->load->model("login_model");
		$this->load->model("localizacao_model");
		$this->load->model("pets_model");
		$this->load->model("servico_model");

		$usuario = $this->input->post("usuario");
		$senha = md5($this->input->post("senha"));
		$autenticacao = $this->login_model->validacao($usuario,$senha);
		
		if($autenticacao) {

			$dispositivos = reconhecerDispositivo();
			
			if (($dispositivos["plataforma"] != "Android" && $dispositivos["plataforma"] != "iOS") && $autenticacao['comercial'] == 0) {
				die(json_encode(array(
				"mensagem"=>"Baixe o nosso aplicativo na play Store ",
				"confirmado"=>false)));
			}
			$localizacao = $this->localizacao_model->getLocalizacaoPorIdUsuario($autenticacao["idUsuario"]);

			if ($localizacao) {

				$sessaoUsuario = array(
					"email" => $autenticacao["email"],
					"senha" => $autenticacao["senha"],
					"idUsuario" => $autenticacao["idUsuario"],
					"nome"=> $autenticacao["nome"],
					"comercial" => $autenticacao["comercial"],
					"longitude" => $localizacao["longitude"],
					"latitude" => $localizacao["latitude"],
					'estado' => $autenticacao['fk_idEstado'],
					'cidade' => $autenticacao['fk_idCidade']
				);

				$this->login_model->session_usuario($sessaoUsuario);
			} else {

				$sessaoUsuario = array(
					"email" => $autenticacao["email"],
					"senha" => $autenticacao["senha"],
					"idUsuario" => $autenticacao["idUsuario"],
					"nome"=> $autenticacao["nome"],
					"comercial" => $autenticacao["comercial"],
					'estado' => $autenticacao['fk_idEstado'],
					'cidade' => $autenticacao['fk_idCidade']
				);
				
				$this->login_model->session_usuario($sessaoUsuario);
			}

			/*$localizacao = obterLocalizaçãoPorIP();
			salvarLocalizacao($localizacao['latitude'], $localizacao['longitude']);*/
			
			date_default_timezone_set('America/Sao_Paulo'); 
			$data = date("Y-m-d H:i:s");

			# Inserindo o log de acesso
			$ip = $_SERVER["REMOTE_ADDR"];
			$dadosLogAcesso = array(
				"ip_usuario" => $ip,
				"data" => $data,
				"fk_idusuario" => $autenticacao["idUsuario"]
			);

			$this->login_model->log_acesso($dadosLogAcesso);
			$this->servico_model->updateServicosNaoFinalizados($autenticacao["idUsuario"]);
		
			if ($autenticacao["comercial"] == "0") {
				$pets = $this->pets_model->buscarPets($autenticacao["idUsuario"]);

				if (!$pets) {
					$url = base_url('selecionarperfilpets');
				} else {
					$url = base_url("noticias");	
				}
				
			} else {
				$url = base_url("financeiro");
			}

			
			echo json_encode(array(
				"confirmado"=>"ok",
				"url" => $url,
				'autenticacao' => $autenticacao['comercial']
			));

		} else{
			echo json_encode(array(
				"mensagem"=>"Email ou senha invalida, tente novamente!",
				"confirmado"=>false));
		}
	}
}





?>