<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagamento extends CI_Controller {
	public function index() {

	}

	public function gerarBoleto_() {
		$this->load->library("form_validation");
		$this->load->helper("pagamento/pagar_me");

		$key = $this->input->post('key');

		$dados = array(
			'nome' =>  $this->input->post('nome'),
			'cpf' => $this->input->post('cpf'),
			'valor' => $this->input->post('valor')
		);

		print_r(gerarBoleto($dados));

	}

	public function cartaoCredito_teste() {
		$this->load->library("form_validation");
		$this->load->helper("pagamento/pagar_me");

		$key = $this->input->post('key');
		
		if($key != 'a061f769d1966cc238a5897ff1f76ea1') {
			die(json_encode(array('tipo' => 'alert', 'mensagem' => "autenticacao invalida")));
		}

		$dados = array(
			'nomeCliente' =>  $this->input->post('nome'),
			'nomePortador' => $this->input->post('nomePortador'),
			'cpf' => $this->input->post('cpf'),
			'valor' => $this->input->post('valor'),
			'numeroCartao' => $this->input->post('numeroCartao'),
			'codigoCVV' => $this->input->post('codigoCVV'),
			'expiracaoData'=> $this->input->post('expiracaoData'),
			'dataAniversario' => $this->input->post('dataAniversario'),
			'telefone' =>  $this->input->post('telefone'),
			'estado' =>  $this->input->post('estado'),
			'cidade' =>  $this->input->post('cidade'),
			'bairro' =>  $this->input->post('bairro'),
			'rua' =>  $this->input->post('rua'),
			'numero' =>  $this->input->post('numero'),
			'cep' =>  $this->input->post('cep'),
			'codigoProduto' =>  $this->input->post('codigoProduto'),
			'nomeProduto' =>  $this->input->post('nomeProduto'),
			'valor' =>  $this->input->post('valor'),
			'idCliente' => $this->input->post('idCliente'),
			'email' => $this->input->post('email'),
			'idRecebedor' => $this->input->post('idRecebedor')
		);

		print_r(cartaoCredito($dados));

	}

	public function cadastrarContaBancaria_teste() {
		$this->load->library("form_validation");
		$this->load->helper("pagamento/pagar_me");

		$key = $this->input->post('key');
		
		if($key != 'a061f769d1966cc238a5897ff1f76ea1') {
			die(json_encode(array('tipo' => 'alert', 'mensagem' => "autenticacao invalida")));
		}


		$dadosBancario = array(
			"agencia" => $this->input->post('agencia'), 
		    "digitoAgencia" => $this->input->post('digitoAgencia'), 
		    "codigoBanco" => $this->input->post('codigoBanco'), 
		    "conta" => $this->input->post('conta'), 
		    "digitoConta" => $this->input->post('digitoConta'), 
		    "CNPJ" => $this->input->post('CNPJ'), 
		    "nomeEmpresa" => $this->input->post('nomeEmpresa'), 
		    "contaCorrente" => $this->input->post('contaCorrente')
		);

		print_r(cadastrarContaBancaria($dadosBancario));
	}

	public function cadastrarFornecedor_teste() {
		$this->load->library("form_validation");
		$this->load->helper("pagamento/pagar_me");

		$key = $this->input->post('key');
		
		if($key != 'a061f769d1966cc238a5897ff1f76ea1') {
			die(json_encode(array('tipo' => 'alert', 'mensagem' => "autenticacao invalida")));
		}

		print_r(cadastrarRecebedor($this->input->post('idBancario'), '5'));

	}

	public function saque_teste() {
		$this->load->library("form_validation");
		$this->load->helper("pagamento/pagar_me");

		$key = $this->input->post('key');
		
		if($key != 'a061f769d1966cc238a5897ff1f76ea1') {
			die(json_encode(array('tipo' => 'alert', 'mensagem' => "autenticacao invalida")));
		}

		print_r(realizarSaque($this->input->post('idRecebedor'), $this->input->post('valor')));
	}

	public function verificarSaldo() {
		$this->load->library("form_validation");
		$this->load->helper("pagamento/pagar_me");

		$key = $this->input->get('key');
		
		if($key != 'a061f769d1966cc238a5897ff1f76ea1') {
			die(json_encode(array('tipo' => 'alert', 'mensagem' => "autenticacao invalida")));
		}

		print_r(verificarSaldo($this->input->get('idRecebedor')));
	}
}
?>